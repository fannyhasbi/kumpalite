generate-proto:
	@sh ./proto-generator.sh

# build process for containerization
docker: build
	@docker-compose build
	@echo "\033[0;32m>>> SUCCESSFULLY BUILD DOCKER CONTAINER <<<\033[0m"
	@go run ./env-generator.go development
	@docker-compose up

build: generate-prod
	@sh ./build.sh
	@echo "\033[0;32m>>> DONE BUILDING <<<\033[0m"

generate-prod:
	@echo "Generating production environment..."
	@go run ./env-generator.go production

# author server
run-author-grpc-server:
	@cd author && go run server/grpc/main.go

test-author:
	@cd author && go test ./... -v -coverprofile=author_coverage.out 
	@cd author && go tool cover -html=author_coverage.out

# graphql server
run-graphql-server:
	@cd graphql && go run ./main.go

test-graphql:
	@cd graphql && go test ./... -v -coverprofile=graphql_coverage.out 
	@cd graphql && go tool cover -html=graphql_coverage.out

# article server
run-article-grpc-server:
	@cd article && go run server/grpc/main.go

test-article:
	@cd article && go test ./... -v -coverprofile=article_coverage.out 
	@cd article && go tool cover -html=article_coverage.out

# comment server
run-comment-grpc-server:
	@cd comment && go run server/grpc/main.go

test-comment:
	@cd comment && go test ./... -v -coverprofile=comment_coverage.out 
	@cd comment && go tool cover -html=comment_coverage.out

# category server
run-category-grpc-server:
	@cd category && go run server/grpc/main.go

test-category:
	@cd category && go test ./... -v -coverprofile=category_coverage.out 
	@cd category && go tool cover -html=category_coverage.out