POSITION=$(pwd)

function printLoading() {
  echo "\033[0;33m=> Building $1 service \033[0m"
}

function build_service() {
  cd ./$1/server/grpc/
  printLoading $1
  GOOS=linux CGO_ENABLED=0 go build -a -installsuffix cgo -o ./$1-build
  cd $POSITION
}

build_service article
build_service author
build_service category
build_service comment

# build graphql server
printLoading graphql
cd graphql && GOOS=linux CGO_ENABLED=0 go build -a -installsuffix cgo -o graphql-build && cd $POSITION
