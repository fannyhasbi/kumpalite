package services

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/fannyhasbi/kumpalite/author/config"
	"gitlab.com/fannyhasbi/kumpalite/author/domain"
	"gitlab.com/fannyhasbi/kumpalite/author/helper"
	"gitlab.com/fannyhasbi/kumpalite/author/repository"
	"gitlab.com/fannyhasbi/kumpalite/author/repository/cockroach"
	"gitlab.com/fannyhasbi/kumpalite/author/repository/inmemory"
	"gitlab.com/fannyhasbi/kumpalite/author/storage"
)

// AuthorServices wrap author services
type AuthorServices struct {
	Query repository.AuthorQuery
	Repo  repository.AuthorRepository
}

// NewAuthorService create new author service instance
func NewAuthorService() *AuthorServices {
	var authorQuery repository.AuthorQuery
	var authorRepo repository.AuthorRepository

	driver := "cockroach"

	switch driver {
	case "cockroach":
		db := config.InitCockroachDB()
		authorRepo = cockroach.NewAuthorRepositoryCockroachDB(db)
		authorQuery = cockroach.NewAuthorQueryCockroachDB(db)
	default:
		db := storage.NewAuthorStorage()
		authorQuery = inmemory.NewAuthorQueryInMemory(db)
		authorRepo = inmemory.NewAuthorRepositoryInMemory(db)
	}

	return &AuthorServices{
		Query: authorQuery,
		Repo:  authorRepo,
	}
}

// CreateAuthor create an author for registration
func (as AuthorServices) CreateAuthor(username, fullname, email, password string) (domain.Author, error) {
	authorID := uuid.NewV4()

	pass, _ := helper.HashPassword(password)

	author := domain.Author{
		ID:       authorID,
		Name:     fullname,
		Email:    email,
		Username: username,
		Password: pass,
	}

	err := as.Repo.Save(&author)
	if err != nil {
		return domain.Author{}, err
	}

	return author, nil
}

// FindAuthorByUsernamePass implement FindAuthorByUsernamePass interface
func (as AuthorServices) FindAuthorByUsernamePass(username, password string) (domain.Author, error) {
	result := as.Query.FindAuthorByUsernamePass(username, password)

	if result.Error != nil {
		return domain.Author{}, result.Error
	}

	return result.Result.(domain.Author), nil
}

// FindAuthorByUsername implements FindAuthorByUsername interface
func (as AuthorServices) FindAuthorByUsername(username string) (domain.Author, error) {
	result := as.Query.FindAuthorByUsername(username)

	if result.Error != nil {
		return domain.Author{}, result.Error
	}

	return result.Result.(domain.Author), nil
}

// FindAuthorByID implements FindAuthorByID interface
func (as AuthorServices) FindAuthorByID(id string) (domain.Author, error) {
	result := as.Query.FindAuthorByID(id)

	if result.Error != nil {
		return domain.Author{}, result.Error
	}

	return result.Result.(domain.Author), nil
}
