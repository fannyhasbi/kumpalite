package helper

import (
	"log"

	"golang.org/x/crypto/bcrypt"
)

// HashPassword hash a string
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		log.Println("[HashPassword][ERR]", err)
	}
	return string(bytes), err
}

// CheckPasswordHash check password validity
func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
