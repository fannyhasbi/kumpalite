package main

import (
	"log"
	"net"

	_ "github.com/lib/pq"
	"gitlab.com/fannyhasbi/kumpalite/author/config"
	pb "gitlab.com/fannyhasbi/kumpalite/author/proto"
	"gitlab.com/fannyhasbi/kumpalite/author/server/grpc/handler"
	"gitlab.com/fannyhasbi/kumpalite/author/services"
	"google.golang.org/grpc"
)

func init() {
	config.InitCockroachDB()
}

func main() {
	port := ":9001"
	srv := grpc.NewServer()
	var authorServer handler.AuthorServer

	authorServer.AuthorService = services.NewAuthorService()

	pb.RegisterAuthorServiceServer(srv, authorServer)

	log.Println("Starting author RPC server at", port)

	listen, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("Could not listen to %s: %v", port, err)
	}

	log.Fatal("Author server is listening", srv.Serve(listen))
}
