package handler

import (
	"context"

	"gitlab.com/fannyhasbi/kumpalite/author/domain"

	pb "gitlab.com/fannyhasbi/kumpalite/author/proto"
)

// AuthorServerService wrap Author Service interface for mock
type AuthorServerService interface {
	FindAuthorByUsernamePass(username, password string) (domain.Author, error)
	FindAuthorByUsername(username string) (domain.Author, error)
	FindAuthorByID(id string) (domain.Author, error)
	CreateAuthor(username, fullname, email, password string) (domain.Author, error)
}

// AuthorServer wrap author service handler
type AuthorServer struct {
	AuthorService AuthorServerService
}

// NewAuthorServer create new Author server instance
func NewAuthorServer(service AuthorServerService) pb.AuthorServiceServer {
	return &AuthorServer{
		AuthorService: service,
	}
}

// CreateAuthor create an author for registration
func (as AuthorServer) CreateAuthor(ctx context.Context, params *pb.AuthorCreateRequest) (*pb.AuthorResponse, error) {
	result, err := as.AuthorService.CreateAuthor(params.Username, params.Name, params.Email, params.Password)
	if err != nil {
		return &pb.AuthorResponse{}, err
	}

	response := ParseAuthorToProto(result)

	return response, nil
}

// FindAuthorByUsernamePass implement find author by username and password
func (as AuthorServer) FindAuthorByUsernamePass(ctx context.Context, params *pb.AuthorRequest) (*pb.AuthorResponse, error) {
	result, err := as.AuthorService.FindAuthorByUsernamePass(params.Username, params.Password)

	if err != nil {
		return &pb.AuthorResponse{}, err
	}

	var response pb.AuthorResponse
	response.ID = result.ID.String()
	response.Name = result.Name
	response.Email = result.Email
	response.Username = result.Username

	return &response, nil
}

// FindAuthorByUsername implement find author by username
func (as AuthorServer) FindAuthorByUsername(ctx context.Context, params *pb.AuthorRequestByUsername) (*pb.AuthorResponse, error) {
	result, err := as.AuthorService.FindAuthorByUsername(params.Username)

	if err != nil {
		return &pb.AuthorResponse{}, err
	}
	var response pb.AuthorResponse
	response.ID = result.ID.String()
	response.Name = result.Name
	response.Email = result.Email
	response.Username = result.Username

	return &response, nil
}

// FindAuthorByID implements find author by id
func (as AuthorServer) FindAuthorByID(ctx context.Context, params *pb.AuthorRequestByID) (*pb.AuthorResponse, error) {
	result, err := as.AuthorService.FindAuthorByID(params.ID)

	if err != nil {
		return &pb.AuthorResponse{}, err
	}

	var response pb.AuthorResponse
	response.ID = result.ID.String()
	response.Name = result.Name
	response.Email = result.Email
	response.Username = result.Username

	return &response, nil
}
