package handler

import (
	"gitlab.com/fannyhasbi/kumpalite/author/domain"
	pb "gitlab.com/fannyhasbi/kumpalite/author/proto"
)

// ParseAuthorToProto parse author domain struct into author proto struct
func ParseAuthorToProto(param domain.Author) *pb.AuthorResponse {
	var response pb.AuthorResponse
	response.ID = param.ID.String()
	response.Name = param.Name
	response.Email = param.Email
	response.Username = param.Username

	return &response
}
