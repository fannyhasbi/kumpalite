package handler

import (
	"context"
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/fannyhasbi/kumpalite/author/domain"
	pb "gitlab.com/fannyhasbi/kumpalite/author/proto"
)

type MockAuthorService struct {
	mock.Mock
}

func (mock MockAuthorService) CreateAuthor(username, fullname, email, password string) (domain.Author, error) {
	args := mock.Called(username, fullname, email, password)
	return args.Get(0).(domain.Author), args.Error(1)
}

// FindAuthorByUsernamePass is implements FindAuthorByUsernamePass service interface
func (mock MockAuthorService) FindAuthorByUsernamePass(username, password string) (domain.Author, error) {
	args := mock.Called(username, password)
	return args.Get(0).(domain.Author), args.Error(1)
}

// FindAuthorByUsername is implements FindAuthorByUsername service interface
func (mock MockAuthorService) FindAuthorByUsername(username string) (domain.Author, error) {
	args := mock.Called(username)
	return args.Get(0).(domain.Author), args.Error(1)
}

// FindAuthorByID is implements FindAuthorByID service interface
func (mock MockAuthorService) FindAuthorByID(id string) (domain.Author, error) {
	args := mock.Called(id)
	return args.Get(0).(domain.Author), args.Error(1)
}

func TestCanFindAuthorByUsernamePass(t *testing.T) {
	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")

	author := domain.Author{
		ID:       userID1,
		Name:     "Budi",
		Email:    "budi@example.com",
		Username: "budi13",
		Password: "hello",
	}

	authorResponse := domain.Author{
		ID:       userID1,
		Name:     "Budi",
		Email:    "budi@example.com",
		Username: "budi13",
	}

	service := MockAuthorService{}
	service.On("FindAuthorByUsernamePass", author.Email, author.Password).Return(authorResponse, nil)

	server := NewAuthorServer(service)

	authorRequest := &pb.AuthorRequest{
		Username: author.Email,
		Password: author.Password,
	}

	result, err := server.FindAuthorByUsernamePass(context.Background(), authorRequest)

	assert.Nil(t, err)
	assert.Equal(t, "Budi", result.Name)

}

func TestCanFindAuthorByUsername(t *testing.T) {
	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")

	author := domain.Author{
		ID:       userID1,
		Name:     "Budi",
		Email:    "budi@example.com",
		Username: "budi13",
		Password: "hello",
	}

	authorResponse := domain.Author{
		ID:       userID1,
		Name:     "Budi",
		Email:    "budi@example.com",
		Username: "budi13",
	}

	service := MockAuthorService{}
	service.On("FindAuthorByUsername", author.Username).Return(authorResponse, nil)

	server := NewAuthorServer(service)

	authorRequestByUsername := &pb.AuthorRequestByUsername{
		Username: author.Username,
	}

	result, err := server.FindAuthorByUsername(context.Background(), authorRequestByUsername)

	assert.Nil(t, err)
	assert.Equal(t, "Budi", result.Name)
}

func TestCanFindAuthorByID(t *testing.T) {
	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")

	author := domain.Author{
		ID:       userID1,
		Name:     "Budi",
		Email:    "budi@example.com",
		Username: "budi13",
		Password: "hello",
	}

	authorResponse := domain.Author{
		ID:       userID1,
		Name:     "Budi",
		Email:    "budi@example.com",
		Username: "budi13",
	}

	service := MockAuthorService{}
	service.On("FindAuthorByID", author.ID.String()).Return(authorResponse, nil)

	server := NewAuthorServer(service)

	authorRequestByID := &pb.AuthorRequestByID{
		ID: author.ID.String(),
	}

	result, err := server.FindAuthorByID(context.Background(), authorRequestByID)

	assert.Nil(t, err)
	assert.Equal(t, "Budi", result.Name)
}

func TestCreateAuthor(t *testing.T) {
	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")

	author := domain.Author{
		ID:       userID1,
		Name:     "Budi",
		Email:    "budi@example.com",
		Username: "budi13",
		Password: "hello",
	}

	authorResponse := domain.Author{
		ID:       userID1,
		Name:     "Budi",
		Email:    "budi@example.com",
		Username: "budi13",
	}

	service := MockAuthorService{}
	service.On("CreateAuthor", author.Username, author.Name, author.Email, author.Password).Return(authorResponse, nil)

	server := NewAuthorServer(service)

	createRequest := &pb.AuthorCreateRequest{
		Username: author.Username,
		Name:     author.Name,
		Email:    author.Email,
		Password: author.Password,
	}

	result, err := server.CreateAuthor(context.Background(), createRequest)

	assert.Nil(t, err)
	assert.Equal(t, "Budi", result.Name)
}
