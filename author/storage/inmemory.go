package storage

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/fannyhasbi/kumpalite/author/domain"
)

// AuthorStorage wrap Author object
type AuthorStorage struct {
	AuthorMap []domain.Author
}

// NewAuthorStorage create new instance of Author storage
func NewAuthorStorage() *AuthorStorage {

	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")
	userID2, _ := uuid.FromString("42217852-9bb2-44f8-baea-fd5dbdced2b7")
	userID3, _ := uuid.FromString("be4d01ee-a16f-48c3-8623-2ea295caa4ff")
	userID4, _ := uuid.FromString("cc8c1c6c-b726-444e-bea5-d095a7c579bc")
	userID5, _ := uuid.FromString("bbe24777-1962-492b-b1a6-c303b438c660")

	users := []domain.Author{
		domain.Author{
			ID:       userID1,
			Name:     "Budi",
			Email:    "budi@example.com",
			Username: "budi13",
			Password: "hello",
		},
		domain.Author{
			ID:       userID2,
			Name:     "Andi",
			Email:    "andi@example.com",
			Username: "andi11",
			Password: "holla",
		},
		domain.Author{
			ID:       userID3,
			Name:     "Elza Melania Pradina",
			Email:    "elzamelaniapradina@gmail.com",
			Username: "elza12_",
			Password: "suikoden2",
		},
		domain.Author{
			ID:       userID4,
			Name:     "Fanny Hasbi",
			Email:    "fani.hasbi@kumparan.com",
			Username: "hasbi_fanny",
			Password: "despair",
		},
		domain.Author{
			ID:       userID5,
			Name:     "Fadli Hidayatullah",
			Email:    "fadli.hidayatullah@kumparan.com",
			Username: "fadlihdytullah",
			Password: "suikoden2",
		},
	}

	return &AuthorStorage{AuthorMap: users}

}
