package domain

import uuid "github.com/satori/go.uuid"

// Author wrap author object
type Author struct {
	ID       uuid.UUID `json:"id"`
	Name     string    `json:"name"`
	Email    string    `json:"email"`
	Username string    `json:"username"`
	Password string    `json:"password"`
}
