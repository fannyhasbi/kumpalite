package config

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
)

const (
	// AuthorDBName db name for author
	AuthorDBName = "kumpalite_author"
)

// InitCockroachDB init database connection
func InitCockroachDB() *sql.DB {
	connStr := fmt.Sprintf("postgresql://root@%s?sslmode=disable", GRPCDatabasePort)
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`CREATE DATABASE IF NOT EXISTS ` + AuthorDBName)
	if err != nil {
		log.Fatal(err)
	}
	db.Close()

	connStr = fmt.Sprintf("postgresql://root@%s/%s?sslmode=disable", GRPCDatabasePort, AuthorDBName)
	db, err = sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}

	ddl, err := ioutil.ReadFile("database/cockroach/ddl.sql")
	if err != nil {
		log.Fatal(err)
	}

	sql := string(ddl)
	_, err = db.Exec(sql)
	if err != nil {
		log.Println(err)
	}

	return db
}
