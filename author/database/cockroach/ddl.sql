CREATE TABLE IF NOT EXISTS author (
    id UUID,
    name VARCHAR NOT NULL,
    email VARCHAR NOT NULL,
    username VARCHAR NOT NULL,
    password VARCHAR,
    PRIMARY KEY (id)
)