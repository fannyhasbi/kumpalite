package cockroach

import (
	"testing"

	"gitlab.com/fannyhasbi/kumpalite/author/domain"

	"github.com/DATA-DOG/go-sqlmock"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

func TestFindAuthorByUsernamePass(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()
	newID, _ := uuid.FromString("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")

	query := NewAuthorQueryCockroachDB(db)

	rows := sqlmock.NewRows([]string{"id", "name", "email", "username", "password"}).
		AddRow(newID, "gogo", "goo@example.com", "goo", "12345")

	mock.ExpectQuery(`^SELECT (.+) FROM author WHERE username = ? AND password = ?`).
		WithArgs("goo", "12345").
		WillReturnRows(rows)

	result := query.FindAuthorByUsernamePass("goo", "12345")
	assert.NoError(t, result.Error)
	// assert.NotEmpty(t, result.Result)

}

func TestFindAuthorByUsername(t *testing.T) {
	db, mock, _ := sqlmock.New()
	newID, _ := uuid.FromString("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")
	defer db.Close()

	query := NewAuthorQueryCockroachDB(db)

	rows := sqlmock.NewRows([]string{"id", "name", "email", "username", "password"}).
		AddRow(newID, "gogo", "goo@example.com", "goo", "12345")

	mock.ExpectQuery(`^SELECT (.+) FROM author WHERE username = ?`).WithArgs("goo").WillReturnRows(rows)
	result := query.FindAuthorByUsername("goo")
	assert.NoError(t, result.Error)
	assert.Equal(t, "gogo", result.Result.(domain.Author).Name)
}

func TestFindAuthorByID(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()

	query := NewAuthorQueryCockroachDB(db)

	rows := sqlmock.NewRows([]string{"id", "name", "email", "username", "password"}).
		AddRow("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691", "gogo", "goo@example.com", "goo", "12345")

	mock.ExpectQuery("^SELECT (.+) FROM author WHERE id = ?").WithArgs("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691").WillReturnRows(rows)

	result := query.FindAuthorByID("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")
	assert.NoError(t, result.Error)
	assert.Equal(t, "gogo", result.Result.(domain.Author).Name)
}
