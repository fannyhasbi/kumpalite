package cockroach

import (
	"database/sql"
	"errors"

	"gitlab.com/fannyhasbi/kumpalite/author/domain"
	"gitlab.com/fannyhasbi/kumpalite/author/helper"
	"gitlab.com/fannyhasbi/kumpalite/author/repository"
)

// AuthorQueryCockroachDB wrap author quer on cockroachDB
type AuthorQueryCockroachDB struct {
	DB *sql.DB
}

// NewAuthorQueryCockroachDB create new instance of AuthorQueryCockroachDB
func NewAuthorQueryCockroachDB(DB *sql.DB) repository.AuthorQuery {
	return &AuthorQueryCockroachDB{
		DB: DB,
	}
}

// FindAuthorByUsernamePass find author by username and pass
func (aq AuthorQueryCockroachDB) FindAuthorByUsernamePass(username, password string) (result repository.QueryResult) {
	author := domain.Author{}

	row := aq.DB.QueryRow(`SELECT id, name, email, username, password FROM author WHERE username = $1`, username)
	row.Scan(
		&author.ID,
		&author.Name,
		&author.Email,
		&author.Username,
		&author.Password,
	)

	if len(author.Username) == 0 {
		result.Result = domain.Author{}
		result.Error = errors.New("User not found")
		return
	}

	valid := helper.CheckPasswordHash(password, author.Password)
	if !valid {
		result.Result = domain.Author{}
		result.Error = errors.New("User not found")
		return
	}

	result.Result = author
	result.Error = nil

	return result
}

// FindAuthorByUsername find author by username
func (aq AuthorQueryCockroachDB) FindAuthorByUsername(username string) repository.QueryResult {
	row := aq.DB.QueryRow(`SELECT id, name, email, username, password FROM author WHERE username = $1`, username)
	author := domain.Author{}
	result := repository.QueryResult{}

	row.Scan(
		&author.ID,
		&author.Name,
		&author.Email,
		&author.Username,
		&author.Password,
	)

	if len(author.ID) == 0 {
		result.Result = domain.Author{}
		result.Error = errors.New("User not found")
	} else {
		result.Result = author
		result.Error = nil
	}

	return result
}

// FindAuthorByID find author by id
func (aq AuthorQueryCockroachDB) FindAuthorByID(id string) repository.QueryResult {
	row := aq.DB.QueryRow(`SELECT id, name, email, username, password FROM author WHERE id = $1`, id)

	author := domain.Author{}
	result := repository.QueryResult{}

	row.Scan(
		&author.ID,
		&author.Name,
		&author.Email,
		&author.Username,
		&author.Password,
	)

	if len(author.Username) == 0 {
		result.Result = domain.Author{}
		result.Error = errors.New("User not found")
	} else {
		result.Result = author
		result.Error = nil
	}

	return result
}
