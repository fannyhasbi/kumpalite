package cockroach

import (
	"testing"

	"github.com/stretchr/testify/assert"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/fannyhasbi/kumpalite/author/domain"

	"github.com/DATA-DOG/go-sqlmock"
)

func TestSaveAuthor(t *testing.T) {
	db, mock, err := sqlmock.New()

	author1 := domain.Author{
		ID:       uuid.NewV4(),
		Name:     "Budi",
		Email:    "budi@example.com",
		Username: "budi13",
		Password: "hello",
	}

	defer db.Close()
	repo := NewAuthorRepositoryCockroachDB(db)

	mock.ExpectExec("INSERT INTO author").
		WithArgs(author1.ID, author1.Name, author1.Email, author1.Username, author1.Password).
		WillReturnResult(sqlmock.NewResult(1, 1))

	err = repo.Save(&author1)
	assert.NoError(t, err)
	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)

}
