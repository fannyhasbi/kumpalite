package cockroach

import (
	"database/sql"
	"errors"

	"gitlab.com/fannyhasbi/kumpalite/author/domain"
	"gitlab.com/fannyhasbi/kumpalite/author/repository"
)

// AuthorRepositoryCockroachDB create author repo instance
type AuthorRepositoryCockroachDB struct {
	DB *sql.DB
}

// NewAuthorRepositoryCockroachDB create author repo instance
func NewAuthorRepositoryCockroachDB(DB *sql.DB) repository.AuthorRepository {
	return &AuthorRepositoryCockroachDB{
		DB: DB,
	}
}

// Save is to save author
func (ar *AuthorRepositoryCockroachDB) Save(author *domain.Author) error {
	// check username and email if exists
	var authorCount int

	row := ar.DB.QueryRow(`SELECT COUNT(*) FROM author WHERE username = $1 OR email = $2`, author.Username, author.Email)
	row.Scan(&authorCount)

	if authorCount > 0 {
		return errors.New("Username or email exists")
	}

	_, err := ar.DB.Exec(`INSERT INTO author(id, name, email, username, password)VALUES($1, $2, $3, $4, $5)`,
		author.ID, author.Name, author.Email, author.Username, author.Password)
	if err != nil {
		return err
	}
	return nil
}
