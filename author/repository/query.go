package repository

// QueryResult wrap query result
type QueryResult struct {
	Result interface{}
	Error  error
}

// AuthorQuery wrap interfaces for author query
type AuthorQuery interface {
	FindAuthorByUsernamePass(username, password string) QueryResult
	FindAuthorByUsername(username string) QueryResult
	FindAuthorByID(id string) QueryResult
}
