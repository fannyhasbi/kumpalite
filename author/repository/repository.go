package repository

import (
	"gitlab.com/fannyhasbi/kumpalite/author/domain"
)

type AuthorRepository interface {
	Save(author *domain.Author) error
}
