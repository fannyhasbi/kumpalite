package inmemory

import (
	"testing"

	"gitlab.com/fannyhasbi/kumpalite/author/domain"

	"github.com/stretchr/testify/assert"
	"gitlab.com/fannyhasbi/kumpalite/author/storage"
)

func TestQueryAuthor(t *testing.T) {
	t.Run("Can Find User By Username and Password", func(t *testing.T) {
		// GIVEN
		storage := storage.NewAuthorStorage()
		query := NewAuthorQueryInMemory(storage)

		// WHEN
		author := query.FindAuthorByUsernamePass("budi13", "hello")

		// THEN
		assert.NoError(t, author.Error)
		assert.Equal(t, "budi13", author.Result.(domain.Author).Username)

	})

	t.Run("Can Find User By Email and Password", func(t *testing.T) {
		// GIVEN
		storage := storage.NewAuthorStorage()
		query := NewAuthorQueryInMemory(storage)

		// WHEN
		author := query.FindAuthorByUsernamePass("budi@example.com", "hello")

		// THEN
		assert.NoError(t, author.Error)
		assert.Equal(t, "budi@example.com", author.Result.(domain.Author).Email)

	})

	t.Run("Can Handle User not Found", func(t *testing.T) {
		// GIVEN
		storage := storage.NewAuthorStorage()
		query := NewAuthorQueryInMemory(storage)

		// WHEN
		author := query.FindAuthorByUsernamePass("ani@example.com", "hello")

		// THEN
		assert.NoError(t, author.Error)
		assert.Empty(t, author.Result)
	})

	t.Run("Can Find User By Username", func(t *testing.T) {
		// GIVEN
		storage := storage.NewAuthorStorage()
		query := NewAuthorQueryInMemory(storage)

		// WHEN
		author := query.FindAuthorByUsername("budi13")

		// THEN
		assert.NoError(t, author.Error)
		assert.NotEmpty(t, author.Result)
	})

	t.Run("Can Find User By ID", func(t *testing.T) {
		// GIVEN
		storage := storage.NewAuthorStorage()
		query := NewAuthorQueryInMemory(storage)

		// WHEN
		author := query.FindAuthorByUsername("budi13")

		// THEN
		assert.NoError(t, author.Error)
		assert.NotEmpty(t, author.Result)
	})

}
