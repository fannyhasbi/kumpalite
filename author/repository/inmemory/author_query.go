package inmemory

import (
	"errors"
	"strings"

	"gitlab.com/fannyhasbi/kumpalite/author/domain"
	"gitlab.com/fannyhasbi/kumpalite/author/repository"
	"gitlab.com/fannyhasbi/kumpalite/author/storage"
)

// AuthorQueryInMemory wrap implement of AuthorQuery in memory
type AuthorQueryInMemory struct {
	Storage *storage.AuthorStorage
}

// NewAuthorQueryInMemory create new instance og AuthorQueryInMemory
func NewAuthorQueryInMemory(storage *storage.AuthorStorage) repository.AuthorQuery {
	return AuthorQueryInMemory{
		Storage: storage,
	}
}

// FindAuthorByUsernamePass implement interface of FindAuthorByUsernamePass
func (aq AuthorQueryInMemory) FindAuthorByUsernamePass(username, password string) repository.QueryResult {
	var response repository.QueryResult

	var stateIsEmail bool
	var stateIsAuthorFound bool

	if strings.Contains(username, "@") {
		stateIsEmail = true
	}

	for _, author := range aq.Storage.AuthorMap {

		if stateIsEmail {
			if author.Email == username && author.Password == password {
				stateIsAuthorFound = true
			}
		} else {
			if author.Username == username && author.Password == password {
				stateIsAuthorFound = true
			}
		}

		if stateIsAuthorFound {
			var authorResult domain.Author
			authorResult.ID = author.ID
			authorResult.Name = author.Name
			authorResult.Username = author.Username
			authorResult.Email = author.Email

			response.Result = authorResult

			return response
		}
	}

	return repository.QueryResult{
		Result: domain.Author{},
	}
}

// FindAuthorByUsername is implement interface of FindAuthorByUsername
func (aq AuthorQueryInMemory) FindAuthorByUsername(username string) repository.QueryResult {
	var response repository.QueryResult

	for _, author := range aq.Storage.AuthorMap {
		if author.Username == username {
			response.Result = author
			return response
		}
	}

	response.Result = domain.Author{}
	response.Error = errors.New("Error 404: user not found")

	return response
}

// FindAuthorByID is implement interface of FindAuthorByID
func (aq AuthorQueryInMemory) FindAuthorByID(id string) repository.QueryResult {
	var response repository.QueryResult

	for _, author := range aq.Storage.AuthorMap {
		if author.ID.String() == id {
			response.Result = author
			return response
		}
	}

	response.Result = domain.Author{}
	response.Error = errors.New("Error 404: user not found")

	return response
}
