package inmemory

import (
	"errors"

	"gitlab.com/fannyhasbi/kumpalite/author/domain"
	"gitlab.com/fannyhasbi/kumpalite/author/repository"
	"gitlab.com/fannyhasbi/kumpalite/author/storage"
)

// AuthorRepositoryInMemory is auhtor repository implementation
type AuthorRepositoryInMemory struct {
	Storage *storage.AuthorStorage
}

// NewAuthorRepositoryInMemory create author repo instance
func NewAuthorRepositoryInMemory(storage *storage.AuthorStorage) repository.AuthorRepository {
	return &AuthorRepositoryInMemory{
		Storage: storage,
	}
}

// Save is to save author
func (repo *AuthorRepositoryInMemory) Save(author *domain.Author) error {
	found := false
	authors := repo.Storage.AuthorMap

	// check username and email is exist
	for _, v := range authors {
		if v.Username == author.Username || v.Email == author.Email {
			found = true
		}
	}

	if found == false {
		// check the length of password
		if len(author.Password) < 5 {
			return errors.New("Password length must be more than 5 characters")
		}

		repo.Storage.AuthorMap = append(authors, *author)
	} else {
		return errors.New("Author exists")
	}

	return nil
}
