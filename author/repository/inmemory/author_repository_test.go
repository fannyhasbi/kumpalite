package inmemory

import (
	"github.com/satori/go.uuid"
	"gitlab.com/fannyhasbi/kumpalite/author/domain"
	"gitlab.com/fannyhasbi/kumpalite/author/storage"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSaveAuthor(t *testing.T) {
	t.Run("Can save", func(t *testing.T) {
		// given
		authorID1 := uuid.NewV4()

		author := domain.Author{
			ID:       authorID1,
			Name:     "Fanny Hasbi",
			Email:    "fanny@example.com",
			Username: "fannyhasbi",
			Password: "hahahaha",
		}

		storage := storage.AuthorStorage{
			AuthorMap: []domain.Author{},
		}

		repo := NewAuthorRepositoryInMemory(&storage)

		// when
		err := repo.Save(&author)

		// then
		assert.NoError(t, err)
		assert.Contains(t, storage.AuthorMap, author)
	})

	t.Run("Cannot save username is same", func(t *testing.T) {
		// given
		authorID1 := uuid.NewV4()

		author1 := domain.Author{
			ID:       authorID1,
			Name:     "Fanny Hasbi",
			Email:    "fanny@example.com",
			Username: "fannyhasbi",
			Password: "hahahaha",
		}

		author2 := domain.Author{
			ID:       authorID1,
			Name:     "Fanny Hasbi",
			Email:    "fannyhasbi@example.com",
			Username: "fannyhasbi",
			Password: "hahahaha",
		}

		storage := storage.AuthorStorage{
			AuthorMap: []domain.Author{},
		}

		repo := NewAuthorRepositoryInMemory(&storage)

		// when
		err1 := repo.Save(&author1)
		err2 := repo.Save(&author2)

		// then
		assert.NoError(t, err1)
		assert.Contains(t, storage.AuthorMap, author1)
		assert.Error(t, err2)
		assert.NotContains(t, storage.AuthorMap, author2)
	})

	t.Run("Cannot save email is same", func(t *testing.T) {
		// given
		authorID1 := uuid.NewV4()

		author1 := domain.Author{
			ID:       authorID1,
			Name:     "Fanny Hasbi",
			Email:    "fanny@example.com",
			Username: "fannyhasbi",
			Password: "hahahaha",
		}

		author2 := domain.Author{
			ID:       authorID1,
			Name:     "Fanny Hasbi",
			Email:    "fanny@example.com",
			Username: "fannyuhuy",
			Password: "hahahaha",
		}

		storage := storage.AuthorStorage{
			AuthorMap: []domain.Author{},
		}

		repo := NewAuthorRepositoryInMemory(&storage)

		// when
		err1 := repo.Save(&author1)
		err2 := repo.Save(&author2)

		// then
		assert.NoError(t, err1)
		assert.Contains(t, storage.AuthorMap, author1)
		assert.Error(t, err2)
		assert.NotContains(t, storage.AuthorMap, author2)
	})

	t.Run("Cannot save username and email is same", func(t *testing.T) {
		// given
		authorID1 := uuid.NewV4()

		author1 := domain.Author{
			ID:       authorID1,
			Name:     "Fanny Hasbi",
			Email:    "fanny@example.com",
			Username: "fannyhasbi",
			Password: "hahahaha",
		}

		author2 := domain.Author{
			ID:       authorID1,
			Name:     "Fanny Hasbi",
			Email:    "fanny@example.com",
			Username: "fannyhasbi",
			Password: "hahahaha",
		}

		storage := storage.AuthorStorage{
			AuthorMap: []domain.Author{},
		}

		repo := NewAuthorRepositoryInMemory(&storage)

		// when
		err1 := repo.Save(&author1)
		err2 := repo.Save(&author2)

		// then
		assert.NoError(t, err1)
		assert.Contains(t, storage.AuthorMap, author1)
		assert.Equal(t, 1, len(storage.AuthorMap))
		assert.Error(t, err2)
	})

	t.Run("Password is less than the required", func(t *testing.T) {
		// given
		authorID1 := uuid.NewV4()

		author1 := domain.Author{
			ID:       authorID1,
			Name:     "Fanny Hasbi",
			Email:    "fanny@example.com",
			Username: "fannyhasbi",
			Password: "a",
		}

		storage := storage.AuthorStorage{
			AuthorMap: []domain.Author{},
		}

		repo := NewAuthorRepositoryInMemory(&storage)

		// when
		err := repo.Save(&author1)

		// then
		assert.Error(t, err)
		assert.NotContains(t, storage.AuthorMap, author1)
	})
}
