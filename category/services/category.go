package services

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/fannyhasbi/kumpalite/category/config"
	"gitlab.com/fannyhasbi/kumpalite/category/domain"
	"gitlab.com/fannyhasbi/kumpalite/category/repository"
	"gitlab.com/fannyhasbi/kumpalite/category/repository/cockroach"
	"gitlab.com/fannyhasbi/kumpalite/category/repository/inmemory"
	"gitlab.com/fannyhasbi/kumpalite/category/storage"
)

// CategoryService wrap article services
type CategoryService struct {
	Repository repository.CategoryRepository
	Query      repository.CategoryQuery
}

// NewCategoryService create new category service interface
func NewCategoryService() *CategoryService {
	var categoryQuery repository.CategoryQuery
	var categoryRepo repository.CategoryRepository

	driver := "cockroach"
	switch driver {
	case "cockroach":
		db := config.InitCockroachDB()
		categoryQuery = cockroach.NewCategoryQueryCockroachDB(db)
		categoryRepo = cockroach.NewCategoryRepositoryCockroachDB(db)
	default:
		db := storage.NewCategoryStorage()
		categoryQuery = inmemory.NewCategoryQueryInMemory(db)
		categoryRepo = inmemory.NewCategoryRepositoryInMemory(db)
	}

	return &CategoryService{
		Repository: categoryRepo,
		Query:      categoryQuery,
	}
}

// CreateCategory create a new category
func (cs CategoryService) CreateCategory(name, slug, authorID string) (domain.Category, error) {
	categoryID := uuid.NewV4()
	authorIDConv, _ := uuid.FromString(authorID)

	category := domain.Category{
		ID:       categoryID,
		AuthorID: authorIDConv,
		Slug:     slug,
		Name:     name,
	}

	err := cs.Repository.Save(&category)
	if err != nil {
		return domain.Category{}, err
	}

	return category, nil
}

// FindCategoryByID find category by ID
func (cs CategoryService) FindCategoryByID(id string) (domain.Category, error) {
	result := cs.Query.FindCategoryByID(id)

	if result.Error != nil {
		return domain.Category{}, result.Error
	}

	return result.Result.(domain.Category), nil
}

// GetCategoriesByAuthorID get categories by author id
func (cs CategoryService) GetCategoriesByAuthorID(authorID string) ([]domain.Category, error) {
	result := cs.Query.GetCategoriesByAuthorID(authorID)

	if result.Error != nil {
		return []domain.Category{}, result.Error
	}

	return result.Result.([]domain.Category), nil
}

// GetCategoriesByAuthorIDAndArticleID get categories
func (cs CategoryService) GetCategoriesByAuthorIDAndArticleID(authorID, articleID string) ([]domain.Category, error) {
	result := cs.Query.GetCategoriesByAuthorIDAndArticleID(authorID, articleID)

	if result.Error != nil {
		return []domain.Category{}, result.Error
	}

	return result.Result.([]domain.Category), nil
}
