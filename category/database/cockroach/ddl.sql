CREATE TABLE IF NOT EXISTS category (
    id UUID,
    name VARCHAR NOT NULL,
    author_id UUID,
    slug VARCHAR NOT NULL,
    PRIMARY KEY (id)
);


INSERT INTO category (id, name, author_id, slug)
VALUES
    ('e35e5de1-af3c-4c38-8a13-1c72800839a0', 'News', '00000000-0000-0000-0000-000000000000', 'news'),
    ('2fe9ae3d-2d53-4972-8cd4-9a1f7e891ac8', 'Entertainment', '00000000-0000-0000-0000-000000000000', 'entertainment'),
    ('aa6a38a6-a812-47e5-aa4d-80d597dd6244', 'Automotive', '00000000-0000-0000-0000-000000000000', 'automotive'),
    ('795f5e47-264f-4436-9aac-5d2b78c0cd40', 'Politic', '00000000-0000-0000-0000-000000000000', 'politic')
ON CONFLICT (id) DO NOTHING;