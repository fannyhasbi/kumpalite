package repository

import (
	"gitlab.com/fannyhasbi/kumpalite/category/domain"
)

// CategoryRepository wrap category repository
type CategoryRepository interface {
	Save(category *domain.Category) error
}
