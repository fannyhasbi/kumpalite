package repository

// QueryResult wrap query results
type QueryResult struct {
	Result interface{}
	Error  error
}

// CategoryQuery wrap category queries
type CategoryQuery interface {
	FindCategoryByID(id string) QueryResult
	GetCategoriesByAuthorID(authorID string) QueryResult
	GetCategoriesByAuthorIDAndArticleID(authorID, articleID string) QueryResult
}
