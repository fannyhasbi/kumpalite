package inmemory

import (
	"errors"

	"gitlab.com/fannyhasbi/kumpalite/category/domain"

	"gitlab.com/fannyhasbi/kumpalite/category/repository"
	"gitlab.com/fannyhasbi/kumpalite/category/storage"
)

// CategoryQueryInMemory struct that implement CategoryQuery interface
type CategoryQueryInMemory struct {
	Storage *storage.CategoryStorage
}

// NewCategoryQueryInMemory create new category query instance
func NewCategoryQueryInMemory(storage *storage.CategoryStorage) repository.CategoryQuery {
	return CategoryQueryInMemory{
		Storage: storage,
	}
}

// FindCategoryByID find category by id
func (cq CategoryQueryInMemory) FindCategoryByID(id string) repository.QueryResult {

	for _, v := range cq.Storage.CategoryMap {
		if v.ID.String() == id {
			return repository.QueryResult{
				Result: v,
			}
		}
	}

	return repository.QueryResult{
		Error: errors.New("Category not found"),
	}
}

// GetCategoriesByAuthorID get category by author id
func (cq CategoryQueryInMemory) GetCategoriesByAuthorID(authorID string) repository.QueryResult {
	var result []domain.Category
	for _, v := range cq.Storage.CategoryMap {
		if v.AuthorID.String() == authorID {
			result = append(result, v)
		}
	}

	return repository.QueryResult{
		Result: result,
	}
}

// GetCategoriesByAuthorIDAndArticleID get categories
func (cq CategoryQueryInMemory) GetCategoriesByAuthorIDAndArticleID(authorID, articleID string) repository.QueryResult {
	// todo
	return repository.QueryResult{
		Result: []domain.Category{},
		Error:  nil,
	}
}
