package inmemory

import (
	"testing"

	"gitlab.com/fannyhasbi/kumpalite/category/storage"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/fannyhasbi/kumpalite/category/domain"
)

func TestSaveCategory(t *testing.T) {
	// given
	categoryID := uuid.NewV4()
	authorID, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")
	newCategory := domain.Category{
		ID:       categoryID,
		AuthorID: authorID,
		Name:     "OLAHRAGA",
	}

	storage := storage.CategoryStorage{
		CategoryMap: []domain.Category{},
	}

	repo := NewCategoryRepositoryInMemory(&storage)

	// when
	err := repo.Save(&newCategory)

	// then
	assert.NoError(t, err)
	assert.Contains(t, storage.CategoryMap, newCategory)
}
