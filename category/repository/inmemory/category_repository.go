package inmemory

import (
	"errors"
	"strings"

	"gitlab.com/fannyhasbi/kumpalite/category/domain"
	"gitlab.com/fannyhasbi/kumpalite/category/repository"
	"gitlab.com/fannyhasbi/kumpalite/category/storage"
)

type CategoryRepositoryInMemory struct {
	Storage *storage.CategoryStorage
}

func NewCategoryRepositoryInMemory(storage *storage.CategoryStorage) repository.CategoryRepository {
	return &CategoryRepositoryInMemory{
		Storage: storage,
	}
}

// contains is helper function to check is new category was reserved category
func contains(listStr []string, item string) bool {
	for _, str := range listStr {
		if strings.ToLower(str) == strings.ToLower(item) {
			return true
		}
	}
	return false
}

// Save is to save category
func (repo *CategoryRepositoryInMemory) Save(category *domain.Category) error {
	reservedCategory := []string{
		"NEWS",
		"ENTERTAIMENT",
		"POLITIC",
		"AUTOMOTIVE",
	}

	isNewCategoryReserved := contains(reservedCategory, category.Name)

	if isNewCategoryReserved {
		return errors.New("New category was reserved category")
	}

	repo.Storage.CategoryMap = append(repo.Storage.CategoryMap, *category)
	return nil
}
