package inmemory

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/fannyhasbi/kumpalite/category/storage"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/fannyhasbi/kumpalite/category/domain"
)

func TestCanFindCategoryByID(t *testing.T) {
	t.Run("Correct", func(t *testing.T) {
		// given
		authorID1 := uuid.NewV4()
		categoryID1 := uuid.NewV4()
		categoryID2 := uuid.NewV4()

		categories := []domain.Category{
			domain.Category{
				ID:       categoryID1,
				AuthorID: authorID1,
				Name:     "SPORT",
			},
			domain.Category{
				ID:       categoryID2,
				AuthorID: authorID1,
				Name:     "CULTURE",
			},
		}

		storage := &storage.CategoryStorage{
			CategoryMap: categories,
		}

		query := NewCategoryQueryInMemory(storage)

		// when
		result := query.FindCategoryByID(categoryID1.String())

		// then
		assert.NoError(t, result.Error)
		assert.Equal(t, result.Result.(domain.Category).ID, categoryID1)
	})
}

func TestCanGetCategoriesByAuthorID(t *testing.T) {
	t.Run("Correct", func(t *testing.T) {
		// given
		authorID1 := uuid.NewV4()
		categoryID1 := uuid.NewV4()
		categoryID2 := uuid.NewV4()

		categories := []domain.Category{
			domain.Category{
				ID:       categoryID1,
				AuthorID: authorID1,
				Name:     "SPORT",
			},
			domain.Category{
				ID:       categoryID2,
				AuthorID: authorID1,
				Name:     "CULTURE",
			},
		}

		storage := &storage.CategoryStorage{
			CategoryMap: categories,
		}

		query := NewCategoryQueryInMemory(storage)

		// when
		result := query.GetCategoriesByAuthorID(authorID1.String())

		// then
		assert.NoError(t, result.Error)
		assert.Contains(t, result.Result, categories[0])
	})

}
