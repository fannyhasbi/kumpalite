package cockroach

import (
	"database/sql"
	"errors"

	"gitlab.com/fannyhasbi/kumpalite/category/domain"
	"gitlab.com/fannyhasbi/kumpalite/category/repository"
)

// CategoryQueryCockroachDB store category db
type CategoryQueryCockroachDB struct {
	DB *sql.DB
}

// NewCategoryQueryCockroachDB create new cockroach category db instance
func NewCategoryQueryCockroachDB(DB *sql.DB) repository.CategoryQuery {
	return &CategoryQueryCockroachDB{
		DB: DB,
	}
}

// FindCategoryByID find category by id
func (cq CategoryQueryCockroachDB) FindCategoryByID(id string) repository.QueryResult {
	row := cq.DB.QueryRow(`SELECT id, name, author_id FROM category WHERE id = $1`, id)
	category := domain.Category{}
	result := repository.QueryResult{}

	row.Scan(
		&category.ID,
		&category.Name,
		&category.AuthorID,
	)

	if len(category.ID) == 0 {
		result.Result = domain.Category{}
		result.Error = errors.New("User not found")
	} else {
		result.Result = category
		result.Error = nil
	}

	return result
}

// GetCategoriesByAuthorID get categories by author id
func (cq CategoryQueryCockroachDB) GetCategoriesByAuthorID(authorID string) repository.QueryResult {
	rows, err := cq.DB.Query(`
		SELECT id, name, author_id, slug
		FROM category
		WHERE author_id = $1
			OR slug IN (
				'news', 'politic', 'entertainment', 'automotive'
			)
	`, authorID)

	categories := []domain.Category{}
	result := repository.QueryResult{}

	if err != nil {
		result.Error = err
	} else {
		for rows.Next() {
			temp := domain.Category{}
			rows.Scan(
				&temp.ID,
				&temp.Name,
				&temp.AuthorID,
				&temp.Slug,
			)

			categories = append(categories, temp)
		}
		result.Result = categories
	}

	return result
}

// GetCategoriesByAuthorIDAndArticleID get categories
func (cq CategoryQueryCockroachDB) GetCategoriesByAuthorIDAndArticleID(authorID, articleID string) repository.QueryResult {
	result := repository.QueryResult{}

	rows, err := cq.DB.Query(`
		SELECT ac.category_id, ac.article_id, c.name, c.slug
		FROM article_category ac
		INNER JOIN category c
			ON ac.category_id = c.id
			WHERE (c.author_id = $1
			OR C.author_id = '00000000-0000-0000-0000-000000000000')
	AND ac.article_id = $2
	`, authorID, articleID)

	categories := []domain.Category{}
	if err != nil {
		result.Error = err
	} else {
		for rows.Next() {
			temp := domain.Category{}
			rows.Scan(
				&temp.ID,
				&temp.AuthorID,
				&temp.Name,
				&temp.Slug,
			)

			categories = append(categories, temp)
		}
		result.Result = categories
	}

	return result
}
