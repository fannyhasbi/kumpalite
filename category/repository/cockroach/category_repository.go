package cockroach

import (
	"database/sql"
	"errors"
	"strings"

	"gitlab.com/fannyhasbi/kumpalite/category/domain"
	"gitlab.com/fannyhasbi/kumpalite/category/repository"
)

// CategoryRepositoryCockroachDB store db
type CategoryRepositoryCockroachDB struct {
	DB *sql.DB
}

// NewCategoryRepositoryCockroachDB create new category repository
func NewCategoryRepositoryCockroachDB(DB *sql.DB) repository.CategoryRepository {
	return &CategoryRepositoryCockroachDB{
		DB: DB,
	}
}

// Save save a category
func (cr CategoryRepositoryCockroachDB) Save(category *domain.Category) error {
	var categoryCount int

	row := cr.DB.QueryRow(`
		SELECT COUNT(*) as cnt
		FROM category
		WHERE (author_id = $1 AND slug = lower($2))
			OR lower($2) IN (
				'news', 'politic', 'entertainment', 'automotive'
			)
	`, category.AuthorID, category.Slug)
	row.Scan(&categoryCount)

	if categoryCount > 0 {
		return errors.New("Category exists")
	}

	_, err := cr.DB.Exec(`INSERT INTO category (
		id, 
		name, 
		slug,
		author_id) VALUES($1, $2, $3, $4)`,
		category.ID, category.Name, strings.ToLower(category.Slug), category.AuthorID)

	if err != nil {
		return err
	}

	return nil
}
