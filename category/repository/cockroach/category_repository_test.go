package cockroach

import (
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/fannyhasbi/kumpalite/category/domain"
)

func TestCanSaveCategory(t *testing.T) {
	db, mock, err := sqlmock.New()
	authorID, _ := uuid.FromString("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")
	categoryID := uuid.NewV4()

	category1 := domain.Category{
		ID:       categoryID,
		Name:     "CULTURE",
		AuthorID: authorID,
	}

	defer db.Close()
	repo := NewCategoryRepositoryCockroachDB(db)

	mock.ExpectExec("INSERT INTO category").
		WithArgs(
			category1.ID,
			category1.Name,
			category1.AuthorID,
		).
		WillReturnResult(sqlmock.NewResult(1, 1))

	err = repo.Save(&category1)
	assert.NoError(t, err)
	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}
