package cockroach

import (
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

func TestCanFindCategoryByID(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()
	newID, _ := uuid.FromString("c3a68043-9cff-4f50-a74b-cd306d861c07")
	authorID, _ := uuid.FromString("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")

	query := NewCategoryQueryCockroachDB(db)

	rows := sqlmock.NewRows([]string{"id", "name", "author_id"}).
		AddRow(newID, "CULTURE", authorID)

	mock.ExpectQuery(`^SELECT (.+) FROM category WHERE id = ?`).
		WithArgs("c3a68043-9cff-4f50-a74b-cd306d861c07").
		WillReturnRows(rows)

	result := query.FindCategoryByID("c3a68043-9cff-4f50-a74b-cd306d861c07")
	assert.NoError(t, result.Error)
}

func TestCanGetCategoriesByAuthorID(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()
	newID, _ := uuid.FromString("c3a68043-9cff-4f50-a74b-cd306d861c07")
	authorID, _ := uuid.FromString("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")

	query := NewCategoryQueryCockroachDB(db)

	rows := sqlmock.NewRows([]string{"id", "name", "author_id"}).
		AddRow(newID, "CULTURE", authorID)

	mock.ExpectQuery(`^SELECT (.+) FROM category WHERE author_id = ?`).
		WithArgs("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691").
		WillReturnRows(rows)

	result := query.GetCategoriesByAuthorID("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")
	assert.NoError(t, result.Error)
}
