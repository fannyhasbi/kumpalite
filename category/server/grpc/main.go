package main

import (
	"log"
	"net"

	_ "github.com/lib/pq"
	pb "gitlab.com/fannyhasbi/kumpalite/category/proto"
	"gitlab.com/fannyhasbi/kumpalite/category/server/grpc/handler"
	"gitlab.com/fannyhasbi/kumpalite/category/services"
	"google.golang.org/grpc"
)

func main() {
	port := ":9004"
	srv := grpc.NewServer()
	var categoryServer handler.CategoryServer

	categoryServer.CategoryService = services.NewCategoryService()

	pb.RegisterCategoryServiceServer(srv, categoryServer)

	log.Println("Starting category RPC server at", port)

	listen, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("Could not listen to %s: %v", port, err)
	}

	log.Fatal("Category server is listening", srv.Serve(listen))
}
