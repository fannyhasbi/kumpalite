package handler

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/mock"
	"gitlab.com/fannyhasbi/kumpalite/category/domain"
	pb "gitlab.com/fannyhasbi/kumpalite/category/proto"
)

type MockCategoryService struct {
	mock.Mock
}

func (mock MockCategoryService) CreateCategory(name, authorID string) (domain.Category, error) {
	args := mock.Called(name, authorID)
	return args.Get(0).(domain.Category), args.Error(1)
}

func (mock MockCategoryService) FindCategoryByID(id string) (domain.Category, error) {
	args := mock.Called(id)
	return args.Get(0).(domain.Category), args.Error(1)
}

func (mock MockCategoryService) GetCategoriesByAuthorID(authorID string) ([]domain.Category, error) {
	args := mock.Called(authorID)
	return args.Get(0).([]domain.Category), args.Error(1)
}

func TestCanCreateCategory(t *testing.T) {
	// given
	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")
	categoryID := uuid.NewV4()

	newCategory := domain.Category{
		ID:       categoryID,
		AuthorID: userID1,
		Name:     "OLAHRAGA",
	}
	responseCategory := domain.Category{
		ID:       categoryID,
		AuthorID: userID1,
		Name:     "OLAHRAGA",
	}

	service := MockCategoryService{}
	service.On(
		"CreateCategory",
		newCategory.Name,
		newCategory.AuthorID.String()).Return(responseCategory, nil)

	server := NewCategoryServer(service)

	createRequest := &pb.CreateCategoryRequest{
		AuthorID: newCategory.AuthorID.String(),
		Name:     newCategory.Name,
	}

	// when
	result, err := server.CreateCategory(context.Background(), createRequest)

	// then
	assert.Nil(t, err)
	assert.Equal(t, newCategory.ID.String(), result.ID)

}

func TestCanFindCategoryByID(t *testing.T) {
	// given
	categoryID1, _ := uuid.FromString("85cf8ed1-63df-4381-834a-dcf9af5e96cc")
	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")

	responseCategory := domain.Category{
		ID:       categoryID1,
		AuthorID: userID1,
		Name:     "NEWS",
	}

	service := MockCategoryService{}
	service.On("FindCategoryByID", categoryID1.String()).Return(responseCategory, nil)

	server := NewCategoryServer(service)

	categoryQueryByID := &pb.CategoryQueryByID{
		ID: categoryID1.String(),
	}

	// when
	result, err := server.FindCategoryByID(context.Background(), categoryQueryByID)

	// then
	assert.Nil(t, err)
	assert.Equal(t, responseCategory.ID.String(), result.ID)
}

func TestCanGetCategoriesByAuthorID(t *testing.T) {
	// given
	categoryID1, _ := uuid.FromString("85cf8ed1-63df-4381-834a-dcf9af5e96cc")
	categoryID3, _ := uuid.FromString("ee833113-2169-43d3-8b72-b484fb457b44")
	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")

	responseCategory := []domain.Category{
		domain.Category{
			ID:       categoryID1,
			AuthorID: userID1,
			Name:     "NEWS",
		},
		domain.Category{
			ID:       categoryID3,
			AuthorID: userID1,
			Name:     "POLITIC",
		},
	}

	service := MockCategoryService{}
	service.On("GetCategoriesByAuthorID", userID1.String()).Return(responseCategory, nil)

	server := NewCategoryServer(service)

	QueryCategoryByAuthorID := &pb.CategoryQueryByAuthorID{
		AuthorID: userID1.String(),
	}

	// when
	result, err := server.GetCategoriesByAuthorID(context.Background(), QueryCategoryByAuthorID)

	// then
	assert.Nil(t, err)
	assert.NotEmpty(t, result)
}
