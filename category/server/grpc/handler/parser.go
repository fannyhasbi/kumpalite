package handler

import (
	"gitlab.com/fannyhasbi/kumpalite/category/domain"
	pb "gitlab.com/fannyhasbi/kumpalite/category/proto"
)

// ParserCategoryToProto parse category domain struct int category proto struct
func ParserCategoryToProto(params domain.Category) *pb.Category {
	var response pb.Category
	response.ID = params.ID.String()
	response.AuthorID = params.AuthorID.String()
	response.Name = params.Name

	return &response
}
