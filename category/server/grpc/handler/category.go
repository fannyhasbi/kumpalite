package handler

import (
	"context"

	"gitlab.com/fannyhasbi/kumpalite/category/domain"
	pb "gitlab.com/fannyhasbi/kumpalite/category/proto"
)

// CategoryServerService wrap category service interface
type CategoryServerService interface {
	CreateCategory(name, slug, authorID string) (domain.Category, error)
	FindCategoryByID(id string) (domain.Category, error)
	GetCategoriesByAuthorID(authorID string) ([]domain.Category, error)
	GetCategoriesByAuthorIDAndArticleID(authorID, articleID string) ([]domain.Category, error)
}

// CategoryServer wrap category service handler
type CategoryServer struct {
	CategoryService CategoryServerService
}

// NewCategoryServer create new Category server
func NewCategoryServer(service CategoryServerService) pb.CategoryServiceServer {
	return &CategoryServer{
		CategoryService: service,
	}
}

// CreateCategory create an category
func (cs CategoryServer) CreateCategory(ctx context.Context, params *pb.CreateCategoryRequest) (*pb.Category, error) {
	result, err := cs.CategoryService.CreateCategory(params.Name, params.Slug, params.AuthorID)

	if err != nil {
		return &pb.Category{}, err
	}

	response := ParserCategoryToProto(result)

	return response, nil
}

// FindCategoryByID find a acategory by ID
func (cs CategoryServer) FindCategoryByID(ctx context.Context, params *pb.CategoryQueryByID) (*pb.Category, error) {
	result, err := cs.CategoryService.FindCategoryByID(params.ID)

	if err != nil {
		return &pb.Category{}, err
	}

	response := ParserCategoryToProto(result)

	return response, nil
}

// GetCategoriesByAuthorID get gategories by author id
func (cs CategoryServer) GetCategoriesByAuthorID(ctx context.Context, params *pb.CategoryQueryByAuthorID) (*pb.CategoryList, error) {
	result, _ := cs.CategoryService.GetCategoriesByAuthorID(params.AuthorID)

	var categories []*pb.Category
	for _, v := range result {
		category := ParserCategoryToProto(v)
		categories = append(categories, category)
	}

	return &pb.CategoryList{
		Categories: categories,
	}, nil
}

// GetCategoriesByAuthorIDAndArticleID get categories
func (cs CategoryServer) GetCategoriesByAuthorIDAndArticleID(ctx context.Context, params *pb.CategoryQueryByAuthorIDAndArticleID) (*pb.CategoryList, error) {
	result, err := cs.CategoryService.GetCategoriesByAuthorIDAndArticleID(params.AuthorID, params.ArticleID)

	if err != nil {
		return &pb.CategoryList{}, err
	}

	var categories []*pb.Category
	for _, v := range result {
		category := ParserCategoryToProto(v)
		categories = append(categories, category)
	}

	return &pb.CategoryList{
		Categories: categories,
	}, nil
}
