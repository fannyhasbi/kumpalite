package storage

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/fannyhasbi/kumpalite/category/domain"
)

// CategoryStorage store categories
type CategoryStorage struct {
	CategoryMap []domain.Category
}

// NewCategoryStorage instantiate te category storage
func NewCategoryStorage() *CategoryStorage {
	userDefault, _ := uuid.FromString("00000000-0000-0000-0000-000000000000")

	categoryID1, _ := uuid.FromString("85cf8ed1-63df-4381-834a-dcf9af5e96cc")
	categoryID2, _ := uuid.FromString("d5874a9b-cce5-40a5-afa3-ad6798b9a71c")
	categoryID3, _ := uuid.FromString("ee833113-2169-43d3-8b72-b484fb457b44")
	categoryID4, _ := uuid.FromString("215f695e-37b1-4e49-a216-34de6536840c")

	categories := []domain.Category{
		domain.Category{
			ID:       categoryID1,
			AuthorID: userDefault,
			Name:     "NEWS",
		},
		domain.Category{
			ID:       categoryID2,
			AuthorID: userDefault,
			Name:     "ENTERTAIMENT",
		},
		domain.Category{
			ID:       categoryID3,
			AuthorID: userDefault,
			Name:     "POLITIC",
		},
		domain.Category{
			ID:       categoryID4,
			AuthorID: userDefault,
			Name:     "ENTERTAIMENT",
		},
	}

	return &CategoryStorage{
		CategoryMap: categories,
	}
}
