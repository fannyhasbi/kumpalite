function printSuccess() {
  echo "\033[0;32m[*] $1 proto successfully generated\033[0m"
}

protoc --go_out=plugins=grpc:./author/proto --proto_path=./author/proto ./author/proto/*.proto && printSuccess "author"
protoc --go_out=plugins=grpc:./graphql/proto --proto_path=./graphql/proto ./graphql/proto/*.proto && printSuccess "graphql"
protoc --go_out=plugins=grpc:./article/proto --proto_path=./article/proto ./article/proto/*.proto && printSuccess "article"
protoc --go_out=plugins=grpc:./comment/proto --proto_path=./comment/proto ./comment/proto/*.proto && printSuccess "comment"
protoc --go_out=plugins=grpc:./category/proto --proto_path=./category/proto ./category/proto/*.proto && printSuccess "category"