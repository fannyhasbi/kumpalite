package domain

import (
	"time"

	uuid "github.com/satori/go.uuid"
	pb "gitlab.com/fannyhasbi/kumpalite/article/proto"
)

const (
	StatusPublished = "PUBLISHED"
	StatusDraft     = "DRAFT"
)

const (
	CategoryNews          = "NEWS"
	CategoryPolitic       = "POLITIC"
	CategoryEntertainment = "ENTERTAINMENT"
	CategoryAutomotive    = "AUTOMOTIVE"
)

// Article store a single article
type Article struct {
	ID            uuid.UUID `json:"id"`
	Slug          string    `json:"slug"`
	Name          string    `json:"name"`
	Body          string    `json:"body"`
	Status        string    `json:"status"`
	Category      []string  `json:"category"`
	CreatedAt     time.Time `json:"created_at"`
	UpdatedAt     time.Time `json:"updated_at"`
	AuthorID      uuid.UUID `json:"user_id"`
	CommentsCount int32     `json:"comments_count"`
	ImageURL      string    `json:"image_url"`
}

// ParseProtoToDomain parse article proto struct into article domain struct
func ParseProtoToDomain(param *pb.Article) Article {
	var article Article

	article.ID, _ = uuid.FromString(param.ID)
	article.Slug = param.Slug
	article.Name = param.Name
	article.Body = param.Body
	article.Status = param.Status
	article.AuthorID, _ = uuid.FromString(param.AuthorID)
	article.ImageURL = param.ImageURL

	return article
}
