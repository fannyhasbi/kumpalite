package config

import (
	"os"

	"github.com/joho/godotenv"
)

var (
	// ENV store environment variable (development|production)
	ENV string

	// GRPCCommentServerPort is port used to connect comment grpc server
	GRPCCommentServerPort = ":9003"
	// GRPCDatabasePort is port used to connect database grpc server
	GRPCDatabasePort = ":26257"
)

func init() {
	godotenv.Load()

	// Check the environment variable
	env := os.Getenv("ENV")
	if env != "production" {
		ENV = "development"
	} else {
		GRPCCommentServerPort = "comment:9003"
		GRPCDatabasePort = "database:26257"
		ENV = "production"
	}
}
