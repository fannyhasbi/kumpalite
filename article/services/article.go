package services

import (
	"log"
	"time"

	"gitlab.com/fannyhasbi/kumpalite/article/config"
	"gitlab.com/fannyhasbi/kumpalite/article/repository/cockroach"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/fannyhasbi/kumpalite/article/domain"
	"gitlab.com/fannyhasbi/kumpalite/article/helper"
	"gitlab.com/fannyhasbi/kumpalite/article/repository"
	"gitlab.com/fannyhasbi/kumpalite/article/repository/inmemory"
	"gitlab.com/fannyhasbi/kumpalite/article/storage"
)

// ArticleService wrap article services
type ArticleService struct {
	Query      repository.ArticleQuery
	Repository repository.ArticleRepository
}

// NewArticleService create new article service instance
func NewArticleService() *ArticleService {
	var articleQuery repository.ArticleQuery
	var articleRepo repository.ArticleRepository

	driver := "cockroach"

	switch driver {
	case "cockroach":
		db := config.InitCockroachDB()
		articleQuery = cockroach.NewArticleQueryCockroachDB(db)
		articleRepo = cockroach.NewArticleRepositoryCockroachDB(db)
	default:
		db := storage.NewArticleStorage()
		articleQuery = inmemory.NewArticleQueryInMemory(db)
		articleRepo = inmemory.NewArticleRepositoryInMemory(db)
	}

	return &ArticleService{
		Query:      articleQuery,
		Repository: articleRepo,
	}
}

// CreateArticle create an article
func (as ArticleService) CreateArticle(id, name, body, slug, status, authorID, imageURL string, categoryIDs []string) (domain.Article, error) {
	var articleID uuid.UUID

	if len(slug) == 0 {
		slug = helper.GenerateSlug(name)
	} else {
		slug = helper.RemoveSymbols(slug)
	}

	if len(status) == 0 || status != domain.StatusPublished {
		status = domain.StatusDraft
	}

	aID, _ := uuid.FromString(authorID)

	if len(id) == 0 {
		articleID = uuid.NewV4()
	} else {
		articleID, _ = uuid.FromString(id)
	}

	article := domain.Article{
		ID:        articleID,
		Slug:      slug,
		Name:      name,
		Body:      body,
		Status:    status,
		CreatedAt: time.Now().In(helper.TimeZone),
		UpdatedAt: time.Now().In(helper.TimeZone),
		AuthorID:  aID,
		ImageURL:  imageURL,
		Category:  categoryIDs,
	}

	var err error
	if len(id) == 0 {
		log.Println("MASUK SAVE", id)
		err = as.Repository.Save(&article)
	} else {
		log.Println("MASUK UPDATE", id)
		err = as.Repository.Update(&article)
	}

	if err != nil {
		return domain.Article{}, err
	}

	return article, nil
}

func (as ArticleService) UpdateArticle(id, name, body, slug, status, authorID, imageURL string, categoryIDs []string) (domain.Article, error) {
	authorUUID, _ := uuid.FromString(authorID)
	articleUUID, _ := uuid.FromString(id)

	article := domain.Article{
		ID:        articleUUID,
		Slug:      slug,
		Name:      name,
		Body:      body,
		Status:    status,
		CreatedAt: time.Now().In(helper.TimeZone),
		UpdatedAt: time.Now().In(helper.TimeZone),
		AuthorID:  authorUUID,
		ImageURL:  imageURL,
		Category:  categoryIDs,
	}

	err := as.Repository.Update(&article)
	if err != nil {
		return domain.Article{}, err
	}

	return article, nil
}

// GetPublishedArticles get all published articles
func (as ArticleService) GetPublishedArticles() []domain.Article {
	result := as.Query.GetPublishedArticles()

	return result.Result.([]domain.Article)
}

// GetArticlesByName is implement get articles by name
func (as ArticleService) GetArticlesByName(name string) ([]domain.Article, error) {

	result := as.Query.GetArticlesByName(name)

	if result.Error != nil {
		return []domain.Article{}, result.Error
	}

	return result.Result.([]domain.Article), nil
}

// FindArticleBySlug is implement find article by slug
func (as ArticleService) FindArticleBySlug(slug string) (domain.Article, error) {
	result := as.Query.FindArticleBySlug(slug)

	if result.Error != nil {
		return domain.Article{}, nil
	}

	return result.Result.(domain.Article), nil
}

// GetArticlesByCategoryAndAuthorID is implement find articles by Category and authorID
func (as ArticleService) GetArticlesByCategoryAndAuthorID(category, authorID string) ([]domain.Article, error) {
	result := as.Query.GetArticlesByCategoryAndAuthorID(category, authorID)

	if result.Error != nil {
		return []domain.Article{}, nil
	}

	return result.Result.([]domain.Article), nil
}

// GetRelatedArticles get related articles by category
func (as ArticleService) GetRelatedArticles(category []string) ([]domain.Article, error) {
	result := as.Query.GetRelatedArticles(category)

	if result.Error != nil {
		return []domain.Article{}, nil
	}

	return result.Result.([]domain.Article), nil
}

// GetPublishedArticlesByAuthorID get published articles by author id for public profile
func (as ArticleService) GetPublishedArticlesByAuthorID(id string) ([]domain.Article, error) {
	result := as.Query.GetPublishedArticlesByAuthorID(id)
	if result.Error != nil {
		return []domain.Article{}, nil
	}

	return result.Result.([]domain.Article), nil
}

// GetArticlesByCategory get articles by category
func (as ArticleService) GetArticlesByCategory(category string) ([]domain.Article, error) {
	result := as.Query.GetArticlesByCategory(category)
	if result.Error != nil {
		return []domain.Article{}, nil
	}

	return result.Result.([]domain.Article), nil
}

// GetArticlesByAuthorIDAndName get articles by author id and name as search feature
func (as ArticleService) GetArticlesByAuthorIDAndName(authorID, name string) []domain.Article {
	result := as.Query.GetArticlesByAuthorIDAndName(authorID, name)
	return result.Result.([]domain.Article)
}

// GetArticlesByAuthorID get articles by authorID
func (as ArticleService) GetArticlesByAuthorID(authorID string) ([]domain.Article, error) {
	result := as.Query.GetArticlesByAuthorID(authorID)
	if result.Error != nil {
		return []domain.Article{}, result.Error
	}

	return result.Result.([]domain.Article), nil
}

// GetPublishedArticlesByCategorySlug get articles
func (as ArticleService) GetPublishedArticlesByCategorySlug(slug string) ([]domain.Article, error) {
	result := as.Query.GetPublishedArticlesByCategorySlug(slug)
	if result.Error != nil {
		return []domain.Article{}, result.Error
	}

	return result.Result.([]domain.Article), nil
}
