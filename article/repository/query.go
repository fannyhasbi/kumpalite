package repository

// QueryResult wrap query results
type QueryResult struct {
	Result interface{}
	Error  error
}

// ArticleQuery wrap article queries
type ArticleQuery interface {
	GetPublishedArticles() QueryResult
	GetArticlesByName(name string) QueryResult
	FindArticleBySlug(slug string) QueryResult
	GetArticlesByCategoryAndAuthorID(category, authorID string) QueryResult
	GetRelatedArticles(category []string) QueryResult
	GetPublishedArticlesByAuthorID(id string) QueryResult
	GetArticlesByCategory(category string) QueryResult
	GetArticlesByAuthorIDAndName(authorID, name string) QueryResult
	GetArticlesByAuthorID(authorID string) QueryResult
	GetPublishedArticlesByCategorySlug(slug string) QueryResult
}
