package repository

import (
	"gitlab.com/fannyhasbi/kumpalite/article/domain"
)

type ArticleRepository interface {
	Save(article *domain.Article) error
	Update(article *domain.Article) error
}
