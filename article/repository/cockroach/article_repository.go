package cockroach

import (
	"database/sql"
	"errors"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/fannyhasbi/kumpalite/article/domain"
	"gitlab.com/fannyhasbi/kumpalite/article/repository"
)

// ArticleRepositoryCockroachDB struct that implement ArticleQuery interface
type ArticleRepositoryCockroachDB struct {
	DB *sql.DB
}

// NewArticleRepositoryCockroachDB create new article query cockroach instance
func NewArticleRepositoryCockroachDB(DB *sql.DB) repository.ArticleRepository {
	return &ArticleRepositoryCockroachDB{
		DB: DB,
	}
}

// Save is to save article
func (ar *ArticleRepositoryCockroachDB) Save(article *domain.Article) error {
	var articleCount int
	categories := article.Category

	row := ar.DB.QueryRow(`SELECT COUNT(*) FROM article WHERE slug = $1`, article.Slug)
	row.Scan(&articleCount)

	if articleCount > 0 {
		return errors.New("Slug exists")
	}

	_, err := ar.DB.Exec(`INSERT INTO article(
		id, 
		slug, 
		name, 
		body, 
		status, 
		created_at, 
		updated_at, 
		author_id, 
		comments_count, 
		image_url)VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`,
		article.ID, article.Slug, article.Name, article.Body,
		article.Status, article.CreatedAt, article.UpdatedAt,
		article.AuthorID, article.CommentsCount, article.ImageURL)

	if err != nil {
		return err
	}

	for i := range categories {
		id := uuid.NewV4()
		categoryID, err := uuid.FromString(categories[i])
		_, err = ar.DB.Exec(`INSERT INTO article_category(
			id, 
			category_id, 
			article_id)VALUES($1, $2, $3)`,
			id, categoryID, article.ID)

		if err != nil {
			return err
		}
	}

	return nil
}

// Update update an article
func (ar *ArticleRepositoryCockroachDB) Update(article *domain.Article) error {
	categories := article.Category

	_, err := ar.DB.Exec(`UPDATE article SET
		slug = $2, 
		name = $3, 
		body = $4, 
		status = $5, 
		created_at = $6, 
		updated_at = $7, 
		author_id = $8, 
		comments_count = $9, 
		image_url = $10 
		WHERE id = $1`,
		article.ID, article.Slug, article.Name, article.Body,
		article.Status, article.CreatedAt, article.UpdatedAt,
		article.AuthorID, article.CommentsCount, article.ImageURL)

	if err != nil {
		return err
	}

	if len(categories) > 0 {
		// first, delete all the joining article categories
		_, err := ar.DB.Exec(`DELETE FROM article_category WHERE article_id = $1`, article.ID)
		if err != nil {
			return err
		}

		for i := range categories {
			id := uuid.NewV4()
			categoryID, err := uuid.FromString(categories[i])
			_, err = ar.DB.Exec(`INSERT INTO article_category(
				id, 
				category_id, 
				article_id)VALUES($1, $2, $3)`,
				id, categoryID, article.ID)

			if err != nil {
				return err
			}
		}
	}

	return nil
}
