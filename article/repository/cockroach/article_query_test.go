package cockroach

import (
	"testing"
	"time"

	"gitlab.com/fannyhasbi/kumpalite/article/domain"

	"github.com/DATA-DOG/go-sqlmock"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

func TestCanGetPublishedArticles(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()
	newID, _ := uuid.FromString("f0eedfa3-f575-41fb-8993-a77cd7cc009b")
	authorID, _ := uuid.FromString("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")

	query := NewArticleQueryCockroachDB(db)

	rows := sqlmock.NewRows([]string{"id", "slug", "name", "body", "status",
		"created_at", "updated_at", "author_id", "comments_count", "image_url"}).
		AddRow(newID, "ini-slug", "ini judul", "ini body", domain.StatusPublished, time.Now(), time.Now(),
			authorID, 0, "https://www.thamesvalley.police.uk/SysSiteAssets/media/images/thames-valley/featured-content/news_website_box_640x394px.png")

	mock.ExpectQuery(`^SELECT (.+) FROM article WHERE status = ?`).
		WithArgs(domain.StatusPublished).
		WillReturnRows(rows)

	result := query.GetPublishedArticles()
	assert.NoError(t, result.Error)
	assert.NotEmpty(t, result.Result)
}

func TestCanGetArticlesByName(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()
	newID, _ := uuid.FromString("f0eedfa3-f575-41fb-8993-a77cd7cc009b")
	authorID, _ := uuid.FromString("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")

	query := NewArticleQueryCockroachDB(db)

	rows := sqlmock.NewRows([]string{"id", "slug", "name", "body", "status",
		"created_at", "updated_at", "author_id", "comments_count", "image_url"}).
		AddRow(newID, "ini-slug", "ini judul", "ini body", domain.StatusPublished, time.Now(), time.Now(),
			authorID, 0, "https://www.thamesvalley.police.uk/SysSiteAssets/media/images/thames-valley/featured-content/news_website_box_640x394px.png")

	mock.ExpectQuery(`^SELECT (.+) FROM article WHERE name LIKE %?%`).
		WithArgs("ini").
		WillReturnRows(rows)

	result := query.GetArticlesByName("ini")
	assert.NoError(t, result.Error)
	assert.NotEmpty(t, result.Result)
}

func TestCanFindArticleBySlug(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()
	newID, _ := uuid.FromString("f0eedfa3-f575-41fb-8993-a77cd7cc009b")
	authorID, _ := uuid.FromString("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")

	query := NewArticleQueryCockroachDB(db)

	rows := sqlmock.NewRows([]string{"id", "slug", "name", "body", "status",
		"created_at", "updated_at", "author_id", "comments_count", "image_url"}).
		AddRow(newID, "ini-slug", "ini judul", "ini body", domain.StatusPublished, time.Now(), time.Now(),
			authorID, 0, "https://www.thamesvalley.police.uk/SysSiteAssets/media/images/thames-valley/featured-content/news_website_box_640x394px.png")

	mock.ExpectQuery(`^SELECT (.+) FROM article WHERE slug = ?`).
		WithArgs("ini-slug").
		WillReturnRows(rows)

	result := query.FindArticleBySlug("ini-slug")
	assert.NoError(t, result.Error)
	assert.NotEmpty(t, result.Result)
}

func TestCanGetArticlesByCategoryAndAuthorID(t *testing.T) {
	db, mock, _ := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	defer db.Close()
	newID1, _ := uuid.FromString("343d8f6e-813f-46e9-b05f-216c699b24cb")
	newID2, _ := uuid.FromString("f0eedfa3-f575-41fb-8993-a77cd7cc009b")
	authorID, _ := uuid.FromString("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")
	categoryID, _ := uuid.FromString("fd71fa3a-e2dc-414a-a29d-3f788599b7e8")

	query := NewArticleQueryCockroachDB(db)

	rows := sqlmock.NewRows([]string{"id", "slug", "name", "body", "status",
		"created_at", "updated_at", "author_id", "comments_count", "image_url"}).
		AddRow(newID2, "ini-slug", "ini judul", "ini body", domain.StatusPublished, time.Now(), time.Now(),
			authorID, 0, "https://www.thamesvalley.police.uk/SysSiteAssets/media/images/thames-valley/featured-content/news_website_box_640x394px.png").
		AddRow(newID1, "berita-lama", "berita lama", "ini berita lama", domain.StatusPublished, time.Now(), time.Now(),
			authorID, 0, "https://www.thamesvalley.police.uk/SysSiteAssets/media/images/thames-valley/featured-content/news_website_box_640x394px.png")

	mock.ExpectQuery(`SELECT 
		ac.article_id id,
		a.slug,
		a.name,
		a.body,
		a.status,
		a.created_at,
		a.updated_at,
		a.author_id,
		a.comments_count,
		a.image_url
	FROM article_category ac 
	INNER JOIN article a
	ON a.id = ac.article_id
	WHERE ac.category_id = $1
	AND a.author_id = $2`).
		WithArgs(categoryID, authorID).
		WillReturnRows(rows)

	result := query.GetArticlesByCategoryAndAuthorID("fd71fa3a-e2dc-414a-a29d-3f788599b7e8", "3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")
	assert.NoError(t, result.Error)
	assert.NotEmpty(t, result.Result)

}

func TestCanGetPublishedArticlesByAuthorID(t *testing.T) {
	db, mock, _ := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	defer db.Close()
	newID, _ := uuid.FromString("f0eedfa3-f575-41fb-8993-a77cd7cc009b")
	authorID, _ := uuid.FromString("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")

	query := NewArticleQueryCockroachDB(db)

	rows := sqlmock.NewRows([]string{"id", "slug", "name", "body", "status",
		"created_at", "updated_at", "author_id", "comments_count", "image_url"}).
		AddRow(newID, "ini-slug", "ini judul", "ini body", domain.StatusPublished, time.Now(), time.Now(),
			authorID, 0, "https://www.thamesvalley.police.uk/SysSiteAssets/media/images/thames-valley/featured-content/news_website_box_640x394px.png")

	mock.ExpectQuery(`SELECT id, slug, name, body, status, created_at, updated_at, author_id, 
	comments_count, image_url FROM article WHERE status = $1 AND author_id = $2 ORDER BY updated_at DESC`).
		WithArgs(domain.StatusPublished, "3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691").
		WillReturnRows(rows)

	result := query.GetPublishedArticlesByAuthorID("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")
	assert.NoError(t, result.Error)
	assert.NotEmpty(t, result.Result)
}

func TestCanGetArticlesByAuthorID(t *testing.T) {
	db, mock, _ := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	defer db.Close()
	newID, _ := uuid.FromString("f0eedfa3-f575-41fb-8993-a77cd7cc009b")
	authorID, _ := uuid.FromString("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")

	query := NewArticleQueryCockroachDB(db)

	rows := sqlmock.NewRows([]string{"id", "slug", "name", "body", "status",
		"created_at", "updated_at", "author_id", "comments_count", "image_url"}).
		AddRow(newID, "ini-slug", "ini judul", "ini body", domain.StatusPublished, time.Now(), time.Now(),
			authorID, 0, "https://www.thamesvalley.police.uk/SysSiteAssets/media/images/thames-valley/featured-content/news_website_box_640x394px.png")

	mock.ExpectQuery(`SELECT id, slug, name, body, status, created_at, updated_at, author_id, 
	comments_count, image_url FROM article WHERE author_id = $1 ORDER BY updated_at DESC`).
		WithArgs("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691").
		WillReturnRows(rows)

	result := query.GetArticlesByAuthorID("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")
	assert.NoError(t, result.Error)
	assert.NotEmpty(t, result.Result)
}

func TestCanGetArticlesByAuthorIDAndName(t *testing.T) {
	db, mock, _ := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	defer db.Close()
	newID, _ := uuid.FromString("f0eedfa3-f575-41fb-8993-a77cd7cc009b")
	authorID, _ := uuid.FromString("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")

	query := NewArticleQueryCockroachDB(db)

	rows := sqlmock.NewRows([]string{"id", "slug", "name", "body", "status",
		"created_at", "updated_at", "author_id", "comments_count", "image_url"}).
		AddRow(newID, "ini-slug", "ini judul", "ini body", domain.StatusPublished, time.Now(), time.Now(),
			authorID, 0, "https://www.thamesvalley.police.uk/SysSiteAssets/media/images/thames-valley/featured-content/news_website_box_640x394px.png")

	mock.ExpectQuery(`SELECT id, slug, name, body, status, created_at, updated_at, author_id, 
	comments_count, image_url FROM article WHERE author_id = $1 AND name LIKE %$2% ORDER BY updated_at DESC`).
		WithArgs("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691", "ini").
		WillReturnRows(rows)

	result := query.GetArticlesByAuthorIDAndName("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691", "ini")
	assert.NoError(t, result.Error)
	assert.NotEmpty(t, result.Result)
}

func TestCanGetArticlesByCategory(t *testing.T) {
	db, mock, _ := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	defer db.Close()
	newID1, _ := uuid.FromString("343d8f6e-813f-46e9-b05f-216c699b24cb")
	newID2, _ := uuid.FromString("f0eedfa3-f575-41fb-8993-a77cd7cc009b")
	authorID, _ := uuid.FromString("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")
	categoryID, _ := uuid.FromString("fd71fa3a-e2dc-414a-a29d-3f788599b7e8")

	query := NewArticleQueryCockroachDB(db)

	rows := sqlmock.NewRows([]string{"id", "slug", "name", "body", "status",
		"created_at", "updated_at", "author_id", "comments_count", "image_url"}).
		AddRow(newID2, "ini-slug", "ini judul", "ini body", domain.StatusPublished, time.Now(), time.Now(),
			authorID, 0, "https://www.thamesvalley.police.uk/SysSiteAssets/media/images/thames-valley/featured-content/news_website_box_640x394px.png").
		AddRow(newID1, "berita-lama", "berita lama", "ini berita lama", domain.StatusPublished, time.Now(), time.Now(),
			authorID, 0, "https://www.thamesvalley.police.uk/SysSiteAssets/media/images/thames-valley/featured-content/news_website_box_640x394px.png")

	mock.ExpectQuery(`SELECT 
		ac.article_id id,
		a.slug,
		a.name,
		a.body,
		a.status,
		a.created_at,
		a.updated_at,
		a.author_id,
		a.comments_count,
		a.image_url
	FROM article_category ac 
	INNER JOIN article a
	ON a.id = ac.article_id
	WHERE ac.category_id = $1
	AND a.status = $2`).
		WithArgs(categoryID, domain.StatusPublished).
		WillReturnRows(rows)

	result := query.GetArticlesByCategory("fd71fa3a-e2dc-414a-a29d-3f788599b7e8")
	assert.NoError(t, result.Error)
	assert.NotEmpty(t, result.Result)
}

func TestRelatedArticle(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()
	newID, _ := uuid.FromString("0929ac04-a9c4-48cc-9819-4f2b89fc1f1a")
	authorID, _ := uuid.FromString("ada79ec3-8e5b-4ea6-b7db-d623fb334c2a")

	query := NewArticleQueryCockroachDB(db)

	columns := []string{"id", "slug", "name", "body", "status", "created_at", "updated_at", "author_id", "comments_count", "image_url"}

	t.Run("With no category", func(t *testing.T) {
		// given
		rows := sqlmock.NewRows(columns).
			AddRow(newID, "ini-slug", "ini judul", "ini body", domain.StatusPublished, time.Now(), time.Now(),
				authorID, 0, "https://www.thamesvalley.police.uk/SysSiteAssets/media/images/thames-valley/featured-content/news_website_box_640x394px.png")

		mock.ExpectQuery(`^SELECT (.+) FROM article a ORDER BY created_at DESC LIMIT 4`).
			WillReturnRows(rows)

		// when
		result := query.GetRelatedArticles([]string{})

		// then
		assert.NoError(t, result.Error)
		assert.NotEmpty(t, result.Result)
	})

	// t.Run("With category", func(t *testing.T) {
	// 	// given
	// 	rows := sqlmock.NewRows(columns).
	// 		AddRow(newID, "ini-slug", "ini judul", "ini body", domain.StatusPublished, time.Now(), time.Now(),
	// 			authorID, 0, "https://www.thamesvalley.police.uk/SysSiteAssets/media/images/thames-valley/featured-content/news_website_box_640x394px.png")

	// 	mock.ExpectQuery(`SELECT ac.article_id id, a.slug, a.name, a.body, a.status, a.created_at, a.updated_at, a.author_id, a.comments_count, a.image_url FROM article_category ac INNER JOIN article a ON a.id = ac.article_id WHERE ac.category_id = $1 ORDER BY a.created_at DESC`).
	// 		WithArgs("'5d8b9407-888d-495d-bff4-6c482d9ea620'").
	// 		WillReturnRows(rows)

	// 	// when
	// 	result := query.GetRelatedArticles([]string{"5d8b9407-888d-495d-bff4-6c482d9ea620"})

	// 	// then
	// 	assert.NoError(t, result.Error)
	// 	assert.NotEmpty(t, result.Result)
	// })
}
