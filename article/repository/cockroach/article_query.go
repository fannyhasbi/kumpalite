package cockroach

import (
	"database/sql"
	"strings"

	"gitlab.com/fannyhasbi/kumpalite/article/domain"
	"gitlab.com/fannyhasbi/kumpalite/article/repository"
)

// ArticleQueryCockroachDB struct that implement ArticleQuery interface
type ArticleQueryCockroachDB struct {
	DB *sql.DB
}

// NewArticleQueryCockroachDB create new article query cockroach instance
func NewArticleQueryCockroachDB(DB *sql.DB) repository.ArticleQuery {
	return &ArticleQueryCockroachDB{
		DB: DB,
	}
}

// GetPublishedArticles get all published articles
func (aq ArticleQueryCockroachDB) GetPublishedArticles() repository.QueryResult {
	rows, err := aq.DB.Query(`SELECT id, slug, name, body, status, created_at, updated_at, author_id, 
	comments_count, image_url FROM article WHERE status = $1 ORDER BY updated_at DESC`, domain.StatusPublished)

	articles := []domain.Article{}
	result := repository.QueryResult{}

	if err != nil {
		result.Error = err
	} else {
		for rows.Next() {
			temp := domain.Article{}
			rows.Scan(
				&temp.ID,
				&temp.Slug,
				&temp.Name,
				&temp.Body,
				&temp.Status,
				&temp.CreatedAt,
				&temp.UpdatedAt,
				&temp.AuthorID,
				&temp.CommentsCount,
				&temp.ImageURL,
			)

			articles = append(articles, temp)
		}
		result.Result = articles
	}
	return result
}

// GetArticlesByName is get Articles by name
func (aq ArticleQueryCockroachDB) GetArticlesByName(name string) repository.QueryResult {
	rows, err := aq.DB.Query(`SELECT id, slug, name, body, status, created_at, updated_at, author_id, 
	comments_count, image_url FROM article WHERE name ILIKE '%'|| $1 ||'%' ORDER BY updated_at DESC`, name)

	articles := []domain.Article{}
	result := repository.QueryResult{}

	if err != nil {
		result.Error = err
	} else {
		for rows.Next() {
			temp := domain.Article{}
			rows.Scan(
				&temp.ID,
				&temp.Slug,
				&temp.Name,
				&temp.Body,
				&temp.Status,
				&temp.CreatedAt,
				&temp.UpdatedAt,
				&temp.AuthorID,
				&temp.CommentsCount,
				&temp.ImageURL,
			)

			articles = append(articles, temp)
		}
		result.Result = articles

	}
	return result
}

// FindArticleBySlug find an article by slug and status published
func (aq ArticleQueryCockroachDB) FindArticleBySlug(slug string) repository.QueryResult {
	row := aq.DB.QueryRow(`SELECT id, slug, name, body, status, created_at, updated_at, author_id, 
	comments_count, image_url FROM article WHERE slug = $1 ORDER BY updated_at DESC`, slug)

	result := repository.QueryResult{}

	temp := domain.Article{}
	row.Scan(
		&temp.ID,
		&temp.Slug,
		&temp.Name,
		&temp.Body,
		&temp.Status,
		&temp.CreatedAt,
		&temp.UpdatedAt,
		&temp.AuthorID,
		&temp.CommentsCount,
		&temp.ImageURL,
	)

	result.Result = temp

	return result
}

// GetArticlesByCategoryAndAuthorID is get articles by
func (aq ArticleQueryCockroachDB) GetArticlesByCategoryAndAuthorID(category, authorID string) repository.QueryResult {
	category = strings.ToLower(category)
	rows, err := aq.DB.Query(`
		SELECT a.id, a.slug, a.name, a.body, a.status, a.created_at, a.updated_at, a.author_id,
		a.comments_count, a.image_url
		FROM article_category ac
		INNER JOIN article a
			ON a.id = ac.article_id
		INNER JOIN category c
			ON c.id = ac.category_id
		WHERE lower(c.slug) = $1
			AND a.id = $2
		ORDER BY a.updated_at DESC
	`, category, authorID)

	articles := []domain.Article{}
	result := repository.QueryResult{}

	if err != nil {
		result.Error = err
	} else {
		for rows.Next() {
			temp := domain.Article{}
			rows.Scan(
				&temp.ID,
				&temp.Slug,
				&temp.Name,
				&temp.Body,
				&temp.Status,
				&temp.CreatedAt,
				&temp.UpdatedAt,
				&temp.AuthorID,
				&temp.CommentsCount,
				&temp.ImageURL,
			)

			articles = append(articles, temp)
		}
		result.Result = articles

	}
	return result
}

// GetRelatedArticles get related articles by category
func (aq ArticleQueryCockroachDB) GetRelatedArticles(category []string) repository.QueryResult {
	result := repository.QueryResult{}
	var rows *sql.Rows
	var err error

	// store related categories
	// categories := ""

	// if len(category) > 0 {
	// 	for i, v := range category {
	// 		if v == "" {
	// 			continue
	// 		}

	// 		categories = fmt.Sprintf("'%s'", v)
	// 		if i != len(category)-1 {
	// 			categories = fmt.Sprintf("%s,", categories)
	// 		}
	// 	}
	// }

	// check wheter the cateories is empty
	// if categories == "" {
	// 00000000-0000-0000-0000-000000000000
	query := `
			SELECT
				a.id,
				a.slug,
				a.name,
				a.body,
				a.status,
				a.created_at,
				a.updated_at,
				a.author_id,
				a.comments_count,
				a.image_url
			FROM article a
			ORDER BY created_at DESC
			LIMIT 4
		`
	rows, err = aq.DB.Query(query)
	// } else {
	// 	query := `
	// 		SELECT
	// 			ac.article_id id,
	// 			a.slug,
	// 			a.name,
	// 			a.body,
	// 			a.status,
	// 			a.created_at,
	// 			a.updated_at,
	// 			a.author_id,
	// 			a.comments_count,
	// 			a.image_url
	// 		FROM article_category ac
	// 		INNER JOIN article a
	// 		ON a.id = ac.article_id
	// 		WHERE ac.category_id::text IN ($1)
	// 		ORDER BY a.created_at DESC
	// 		LIMIT 4
	// 	`
	// 	// categories := "'5d8b9407-888d-495d-bff4-6c482d9ea620' OR ac.category_id = '5d8b9407-888d-495d-bff4-6c482d9ea620'"
	// 	log.Println("UHUY", categories)
	// 	rows, err = aq.DB.Query(query, categories)
	// 	log.Println("ERROR KEDUA", err)
	// }

	articles := []domain.Article{}

	if err != nil {
		result.Error = err
	} else {
		for rows.Next() {
			temp := domain.Article{}
			rows.Scan(
				&temp.ID,
				&temp.Slug,
				&temp.Name,
				&temp.Body,
				&temp.Status,
				&temp.CreatedAt,
				&temp.UpdatedAt,
				&temp.AuthorID,
				&temp.CommentsCount,
				&temp.ImageURL,
			)

			articles = append(articles, temp)
		}
		result.Result = articles
	}

	return result
}

// GetPublishedArticlesByAuthorID get published article for author public profile
func (aq ArticleQueryCockroachDB) GetPublishedArticlesByAuthorID(id string) repository.QueryResult {
	rows, err := aq.DB.Query(`SELECT id, slug, name, body, status, created_at, updated_at, author_id, 
	comments_count, image_url FROM article WHERE status = $1 AND author_id = $2 ORDER BY updated_at DESC`, domain.StatusPublished, id)

	articles := []domain.Article{}
	result := repository.QueryResult{}

	if err != nil {
		result.Error = err
	} else {
		for rows.Next() {
			temp := domain.Article{}
			rows.Scan(
				&temp.ID,
				&temp.Slug,
				&temp.Name,
				&temp.Body,
				&temp.Status,
				&temp.CreatedAt,
				&temp.UpdatedAt,
				&temp.AuthorID,
				&temp.CommentsCount,
				&temp.ImageURL,
			)

			articles = append(articles, temp)
		}
		result.Result = articles
	}
	return result
}

// GetArticlesByCategory get articles by category
func (aq ArticleQueryCockroachDB) GetArticlesByCategory(category string) repository.QueryResult {
	rows, err := aq.DB.Query(`SELECT
		ac.article_id id,
		a.slug,
		a.name,
		a.body,
		a.status,
		a.created_at,
		a.updated_at,
		a.author_id,
		a.comments_count,
		a.image_url
	FROM article_category ac 
	INNER JOIN article a
	ON a.id = ac.article_id
	WHERE ac.category_id = $1 AND a.status = $2`, category, domain.StatusPublished)

	articles := []domain.Article{}
	result := repository.QueryResult{}

	if err != nil {
		result.Error = err
	} else {
		for rows.Next() {
			temp := domain.Article{}
			rows.Scan(
				&temp.ID,
				&temp.Slug,
				&temp.Name,
				&temp.Body,
				&temp.Status,
				&temp.CreatedAt,
				&temp.UpdatedAt,
				&temp.AuthorID,
				&temp.CommentsCount,
				&temp.ImageURL,
			)

			articles = append(articles, temp)
		}
		result.Result = articles

	}
	return result
}

// GetArticlesByAuthorIDAndName get articles by author id and name
func (aq ArticleQueryCockroachDB) GetArticlesByAuthorIDAndName(authorID, name string) repository.QueryResult {
	rows, err := aq.DB.Query(`SELECT id, slug, name, body, status, created_at, updated_at, author_id, 
	comments_count, image_url FROM article WHERE author_id = $1 AND name ILIKE '%'|| $2 ||'%' ORDER BY updated_at DESC`, authorID, name)

	articles := []domain.Article{}
	result := repository.QueryResult{}

	if err != nil {
		result.Error = err
	} else {
		for rows.Next() {
			temp := domain.Article{}
			rows.Scan(
				&temp.ID,
				&temp.Slug,
				&temp.Name,
				&temp.Body,
				&temp.Status,
				&temp.CreatedAt,
				&temp.UpdatedAt,
				&temp.AuthorID,
				&temp.CommentsCount,
				&temp.ImageURL,
			)

			articles = append(articles, temp)
		}
		result.Result = articles
	}
	return result
}

// GetArticlesByAuthorID get articles by authorID
func (aq ArticleQueryCockroachDB) GetArticlesByAuthorID(authorID string) repository.QueryResult {
	rows, err := aq.DB.Query(`SELECT id, slug, name, body, status, created_at, updated_at, author_id, 
	comments_count, image_url FROM article WHERE author_id = $1 ORDER BY updated_at DESC`, authorID)

	articles := []domain.Article{}
	result := repository.QueryResult{}

	if err != nil {
		result.Error = err
	} else {
		for rows.Next() {
			temp := domain.Article{}
			rows.Scan(
				&temp.ID,
				&temp.Slug,
				&temp.Name,
				&temp.Body,
				&temp.Status,
				&temp.CreatedAt,
				&temp.UpdatedAt,
				&temp.AuthorID,
				&temp.CommentsCount,
				&temp.ImageURL,
			)

			// temp.Category = categories
			articles = append(articles, temp)
		}
		result.Result = articles
	}
	return result
}

// GetPublishedArticlesByCategorySlug get articles
func (aq ArticleQueryCockroachDB) GetPublishedArticlesByCategorySlug(slug string) repository.QueryResult {
	slug = strings.ToLower(slug)

	rows, err := aq.DB.Query(`
		SELECT a.id, a.slug, a.name, a.body, a.status, a.created_at, a.updated_at, a.author_id,
		a.comments_count, a.image_url
		FROM article_category ac
		INNER JOIN article a
			ON a.id = ac.article_id
		INNER JOIN category c
			ON c.id = ac.category_id
		WHERE lower(c.slug) = $1
			AND a.status = 'PUBLISHED'
		ORDER BY a.updated_at DESC
	`, slug)

	articles := []domain.Article{}
	result := repository.QueryResult{}

	if err != nil {
		result.Error = err
	} else {
		for rows.Next() {
			temp := domain.Article{}
			rows.Scan(
				&temp.ID,
				&temp.Slug,
				&temp.Name,
				&temp.Body,
				&temp.Status,
				&temp.CreatedAt,
				&temp.UpdatedAt,
				&temp.AuthorID,
				&temp.CommentsCount,
				&temp.ImageURL,
			)

			articles = append(articles, temp)
		}
		result.Result = articles
	}
	return result
}
