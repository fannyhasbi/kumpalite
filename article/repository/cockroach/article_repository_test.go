package cockroach

import (
	"testing"
	"time"

	"gitlab.com/fannyhasbi/kumpalite/article/domain"

	"github.com/DATA-DOG/go-sqlmock"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

func TestSaveArticle(t *testing.T) {
	db, mock, err := sqlmock.New()
	authorID, _ := uuid.FromString("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")

	article1 := domain.Article{
		ID:            uuid.NewV4(),
		Name:          "Ini Article",
		Body:          "Ini Bodynya",
		Status:        domain.StatusPublished,
		Category:      []string{domain.CategoryNews, domain.CategoryPolitic},
		CreatedAt:     time.Now(),
		UpdatedAt:     time.Now(),
		AuthorID:      authorID,
		CommentsCount: 20,
		ImageURL:      "https://www.thamesvalley.police.uk/SysSiteAssets/media/images/thames-valley/featured-content/news_website_box_640x394px.png",
	}

	defer db.Close()
	repo := NewArticleRepositoryCockroachDB(db)

	mock.ExpectExec("INSERT INTO article").
		WithArgs(
			article1.ID,
			article1.Name,
			article1.Body,
			article1.Status,
			article1.CreatedAt,
			article1.UpdatedAt,
			article1.AuthorID,
			article1.CommentsCount,
			article1.ImageURL,
		).
		WillReturnResult(sqlmock.NewResult(1, 1))

	err = repo.Save(&article1)
	assert.NoError(t, err)
	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}
