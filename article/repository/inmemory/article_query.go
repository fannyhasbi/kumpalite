package inmemory

import (
	"errors"
	"math/rand"
	"strings"

	"gitlab.com/fannyhasbi/kumpalite/article/domain"
	"gitlab.com/fannyhasbi/kumpalite/article/repository"
	"gitlab.com/fannyhasbi/kumpalite/article/storage"
)

// ArticleQueryInMemory struct that implement ArticleQuery interface
type ArticleQueryInMemory struct {
	Storage *storage.ArticleStorage
}

// NewArticleQueryInMemory create new article query inmemory instance
func NewArticleQueryInMemory(storage *storage.ArticleStorage) repository.ArticleQuery {
	return ArticleQueryInMemory{
		Storage: storage,
	}
}

// contains is helper function to find is article contain a specific category
func contains(listStr []string, item string) bool {
	for _, str := range listStr {
		if strings.ToLower(str) == strings.ToLower(item) {
			return true
		}
	}
	return false
}

// GetPublishedArticles get all published articles
func (aq ArticleQueryInMemory) GetPublishedArticles() repository.QueryResult {
	var result []domain.Article

	for _, v := range aq.Storage.ArticleMap {
		if v.Status == domain.StatusPublished {
			result = append(result, v)
		}
	}

	return repository.QueryResult{
		Result: result,
	}
}

// GetArticlesByName is get Articles by name
func (aq ArticleQueryInMemory) GetArticlesByName(name string) repository.QueryResult {
	var result []domain.Article

	for _, v := range aq.Storage.ArticleMap {
		lowercaseName := strings.ToLower(v.Name)
		lowercaseNameReq := strings.ToLower(name)

		if v.Status == domain.StatusPublished && strings.Contains(lowercaseName, lowercaseNameReq) {
			result = append(result, v)
		}
	}

	return repository.QueryResult{
		Result: result,
	}
}

// FindArticleBySlug find an article by slug and status published
func (aq ArticleQueryInMemory) FindArticleBySlug(slug string) repository.QueryResult {
	var result domain.Article

	for _, v := range aq.Storage.ArticleMap {
		if v.Slug == slug && v.Status == domain.StatusPublished {
			result = v
			return repository.QueryResult{
				Result: result,
			}
		}
	}

	return repository.QueryResult{
		Error: errors.New("Error 404: Article not Found"),
	}
}

// GetArticlesByCategoryAndAuthorID is get articles by
func (aq ArticleQueryInMemory) GetArticlesByCategoryAndAuthorID(category, authorID string) repository.QueryResult {
	var result []domain.Article

	for _, v := range aq.Storage.ArticleMap {
		isCategoryExist := contains(v.Category, category)
		if v.Status == domain.StatusPublished &&
			isCategoryExist &&
			v.AuthorID.String() == authorID {

			result = append(result, v)
		}
	}

	return repository.QueryResult{
		Result: result,
	}
}

func containsCategory(s []string, c []string) bool {
	for i := range s {
		for j := range c {
			if s[i] == c[j] {
				return true
			}
		}
	}
	return false
}

func containsIndex(s []int, i int) bool {
	for j := range s {
		if s[j] == i {
			return true
		}
	}
	return false
}

// GetRelatedArticles get related articles by category
func (aq ArticleQueryInMemory) GetRelatedArticles(category []string) (result repository.QueryResult) {
	var articles []domain.Article

	defer func() {
		result.Result = articles
	}()

	var relatedCount int

	articleCount := len(aq.Storage.ArticleMap)

	// check the article count to serve better related articles
	if articleCount > 4 {
		relatedCount = 4
	} else {
		relatedCount = articleCount
	}

	if len(category) == 0 {
		var randomNumbers []int

		i := 0
		for i < relatedCount {
			randInt := rand.Intn(articleCount)

			if !containsIndex(randomNumbers, randInt) {
				randomNumbers = append(randomNumbers, randInt)

				article := aq.Storage.ArticleMap[randInt]
				// check the status
				if article.Status == domain.StatusPublished {
					articles = append(articles, article)
				}

				i++
			}
		}
		return
	}

	// set to false
	// check whether the category input is exist in the default category
	// or within the articles`s categories
	containFlag := false

	for _, v := range aq.Storage.ArticleMap {
		if containsCategory(v.Category, category) {
			containFlag = true
			// check the status
			if v.Status == domain.StatusPublished {
				articles = append(articles, v)

				if len(articles) == relatedCount {
					break
				}
			}
		}
	}

	if !containFlag {
		var randomNumbers []int

		i := len(articles)
		for i < relatedCount {
			randInt := rand.Intn(articleCount)

			if !containsIndex(randomNumbers, randInt) {
				randomNumbers = append(randomNumbers, randInt)

				article := aq.Storage.ArticleMap[randInt]
				// check the status
				if article.Status == domain.StatusPublished {
					articles = append(articles, article)
				}

				i++
			}
		}
	}

	return
}

// GetPublishedArticlesByAuthorID get published article for author public profile
func (aq ArticleQueryInMemory) GetPublishedArticlesByAuthorID(id string) repository.QueryResult {
	var result []domain.Article

	for _, v := range aq.Storage.ArticleMap {
		if v.Status == domain.StatusPublished && v.AuthorID.String() == id {
			result = append(result, v)
		}
	}

	return repository.QueryResult{
		Result: result,
	}
}

// GetArticlesByCategory get articles by category
func (aq ArticleQueryInMemory) GetArticlesByCategory(category string) repository.QueryResult {
	var result []domain.Article

	for _, v := range aq.Storage.ArticleMap {
		if v.Status == domain.StatusPublished &&
			contains(v.Category, category) {
			result = append(result, v)
		}
	}

	return repository.QueryResult{
		Result: result,
	}
}

// GetArticlesByAuthorIDAndName get articles by author id and name
func (aq ArticleQueryInMemory) GetArticlesByAuthorIDAndName(authorID, name string) repository.QueryResult {
	var result []domain.Article

	for _, v := range aq.Storage.ArticleMap {
		lowercaseName := strings.ToLower(v.Name)
		lowercaseNameReq := strings.ToLower(name)

		// get all published and draft articles
		// because this service is for manage the articles for each author
		// this service is for Dashboard on Front End
		if strings.Contains(lowercaseName, lowercaseNameReq) && v.AuthorID.String() == authorID {
			result = append(result, v)
		}
	}

	return repository.QueryResult{
		Result: result,
	}
}

// GetArticlesByAuthorID get articles by authorID
func (aq ArticleQueryInMemory) GetArticlesByAuthorID(authorID string) repository.QueryResult {
	var result []domain.Article

	for _, v := range aq.Storage.ArticleMap {
		if v.AuthorID.String() == authorID {
			result = append(result, v)
		}
	}

	return repository.QueryResult{
		Result: result,
	}
}

// GetPublishedArticlesByCategorySlug get articles
func (aq ArticleQueryInMemory) GetPublishedArticlesByCategorySlug(slug string) repository.QueryResult {
	return repository.QueryResult{}
}
