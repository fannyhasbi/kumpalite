package inmemory

import (
	"errors"

	"gitlab.com/fannyhasbi/kumpalite/article/domain"
	"gitlab.com/fannyhasbi/kumpalite/article/repository"
	"gitlab.com/fannyhasbi/kumpalite/article/storage"
)

// ArticleRepositoryInMemory is article repository implementation
type ArticleRepositoryInMemory struct {
	Storage *storage.ArticleStorage
}

// NewArticleRepositoryInMemory create article repo instance
func NewArticleRepositoryInMemory(storage *storage.ArticleStorage) repository.ArticleRepository {
	return &ArticleRepositoryInMemory{
		Storage: storage,
	}
}

// Save is to save article
func (repo *ArticleRepositoryInMemory) Save(article *domain.Article) error {
	found := false
	articles := repo.Storage.ArticleMap

	// check slug is exist
	for _, v := range articles {
		if v.Slug == article.Slug {
			found = true
		}
	}

	if found {
		return errors.New("Article exists")
	}

	if len(article.Name) < 10 || len(article.Name) > 125 {
		return errors.New("Title length must be more than 200 and less than 999 characters")
	}

	repo.Storage.ArticleMap = append(repo.Storage.ArticleMap, *article)

	return nil
}

func (repo *ArticleRepositoryInMemory) Update(article *domain.Article) error {
	return nil
}
