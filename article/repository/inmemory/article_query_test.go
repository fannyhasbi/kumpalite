package inmemory

import (
	"testing"

	"gitlab.com/fannyhasbi/kumpalite/article/storage"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/fannyhasbi/kumpalite/article/domain"
)

func TestCanGetArticles(t *testing.T) {
	t.Run("Can get all published articles", func(t *testing.T) {
		// given
		articleID1 := uuid.NewV4()

		articles := []domain.Article{
			domain.Article{
				ID:     articleID1,
				Slug:   "artikel-lama",
				Name:   "Artikel Lama",
				Body:   "Ini body artikel lama",
				Status: domain.StatusPublished,
			},
			domain.Article{
				ID:     articleID1,
				Slug:   "artikel-baru",
				Name:   "Artikel Baru",
				Body:   "Ini body artikel baru",
				Status: domain.StatusPublished,
			},
		}

		storage := &storage.ArticleStorage{
			ArticleMap: articles,
		}

		query := NewArticleQueryInMemory(storage)

		// when
		result := query.GetPublishedArticles()

		// then
		assert.NoError(t, result.Error)
		assert.Contains(t, result.Result, articles[0])
		assert.Contains(t, result.Result, articles[1])
	})

	t.Run("Can get one published article", func(t *testing.T) {
		// given
		articleID1 := uuid.NewV4()

		articles := []domain.Article{
			domain.Article{
				ID:     articleID1,
				Slug:   "artikel-lama",
				Name:   "Artikel Lama",
				Body:   "Ini body artikel lama",
				Status: domain.StatusPublished,
			},
			domain.Article{
				ID:     articleID1,
				Slug:   "artikel-baru",
				Name:   "Artikel Baru",
				Body:   "Ini body artikel baru",
				Status: domain.StatusDraft,
			},
		}

		storage := &storage.ArticleStorage{
			ArticleMap: articles,
		}

		query := NewArticleQueryInMemory(storage)

		// when
		result := query.GetPublishedArticles()

		// then
		assert.NoError(t, result.Error)
		assert.Contains(t, result.Result, articles[0])
		assert.NotContains(t, result.Result, articles[1])
	})

	t.Run("Empty storage", func(t *testing.T) {
		// given
		storage := &storage.ArticleStorage{
			ArticleMap: []domain.Article{},
		}

		query := NewArticleQueryInMemory(storage)

		// when
		result := query.GetPublishedArticles()

		// then
		assert.NoError(t, result.Error)
		assert.Empty(t, result.Result)
	})

	t.Run("All articles are not published", func(t *testing.T) {
		// given
		articleID1 := uuid.NewV4()

		articles := []domain.Article{
			domain.Article{
				ID:     articleID1,
				Slug:   "artikel-lama",
				Name:   "Artikel Lama",
				Body:   "Ini body artikel lama",
				Status: domain.StatusDraft,
			},
			domain.Article{
				ID:     articleID1,
				Slug:   "artikel-baru",
				Name:   "Artikel Baru",
				Body:   "Ini body artikel baru",
				Status: domain.StatusDraft,
			},
		}

		storage := &storage.ArticleStorage{
			ArticleMap: articles,
		}

		query := NewArticleQueryInMemory(storage)

		// when
		result := query.GetPublishedArticles()

		// then
		assert.NoError(t, result.Error)
		assert.Empty(t, result.Result)
	})

	t.Run("Can get all articles with similiar name", func(t *testing.T) {
		// given
		articleID1 := uuid.NewV4()
		articleID2 := uuid.NewV4()
		articleID3 := uuid.NewV4()
		var expectedLen int32 = 2

		articles := []domain.Article{
			domain.Article{
				ID:     articleID1,
				Slug:   "artikel-lama",
				Name:   "Artikel Lama",
				Body:   "Ini body artikel lama",
				Status: domain.StatusPublished,
			},
			domain.Article{
				ID:     articleID2,
				Slug:   "artikel-baru",
				Name:   "Artikel Baru",
				Body:   "Ini body artikel baru",
				Status: domain.StatusPublished,
			},
			domain.Article{
				ID:     articleID3,
				Slug:   "artikel-kemaren",
				Name:   "Artikel kemaren",
				Body:   "Ini body artikel kemaren",
				Status: domain.StatusDraft,
			},
		}

		// when

		storage := storage.ArticleStorage{
			ArticleMap: articles,
		}

		query := NewArticleQueryInMemory(&storage)

		// then
		result := query.GetArticlesByName("artikel")
		resultLen := int32(len(result.Result.([]domain.Article)))

		assert.NoError(t, result.Error)
		assert.Equal(t, expectedLen, resultLen)
	})

	t.Run("Can get article by slug", func(t *testing.T) {
		// given
		articleID1 := uuid.NewV4()
		articleID2 := uuid.NewV4()
		articleID3 := uuid.NewV4()

		slug1 := "artikel-baru"

		articles := []domain.Article{
			domain.Article{
				ID:     articleID1,
				Slug:   "artikel-lama",
				Name:   "Artikel Lama",
				Body:   "Ini body artikel lama",
				Status: domain.StatusPublished,
			},
			domain.Article{
				ID:     articleID2,
				Slug:   "artikel-baru",
				Name:   "Artikel Baru",
				Body:   "Ini body artikel baru",
				Status: domain.StatusPublished,
			},
			domain.Article{
				ID:     articleID3,
				Slug:   "artikel-kemaren",
				Name:   "Artikel kemaren",
				Body:   "Ini body artikel kemaren",
				Status: domain.StatusDraft,
			},
		}

		storage := storage.ArticleStorage{
			ArticleMap: articles,
		}

		query := NewArticleQueryInMemory(&storage)

		// when

		result := query.FindArticleBySlug(slug1)

		// then
		assert.NoError(t, result.Error)
		assert.Equal(t, articles[1].Slug, result.Result.(domain.Article).Slug)
	})

	t.Run("Can handle slug which doesn't exist", func(t *testing.T) {
		// given
		articleID1 := uuid.NewV4()
		articleID2 := uuid.NewV4()
		articleID3 := uuid.NewV4()

		slug1 := "artikel-kapan"

		articles := []domain.Article{
			domain.Article{
				ID:     articleID1,
				Slug:   "artikel-lama",
				Name:   "Artikel Lama",
				Body:   "Ini body artikel lama",
				Status: domain.StatusPublished,
			},
			domain.Article{
				ID:     articleID2,
				Slug:   "artikel-baru",
				Name:   "Artikel Baru",
				Body:   "Ini body artikel baru",
				Status: domain.StatusPublished,
			},
			domain.Article{
				ID:     articleID3,
				Slug:   "artikel-kemaren",
				Name:   "Artikel kemaren",
				Body:   "Ini body artikel kemaren",
				Status: domain.StatusDraft,
			},
		}

		storage := storage.ArticleStorage{
			ArticleMap: articles,
		}

		query := NewArticleQueryInMemory(&storage)

		// when

		result := query.FindArticleBySlug(slug1)

		// then
		assert.Error(t, result.Error)
	})

	t.Run("Cannot get article by slug if status not published", func(t *testing.T) {
		// given
		articleID1 := uuid.NewV4()
		articleID2 := uuid.NewV4()
		articleID3 := uuid.NewV4()

		slug1 := "artikel-kemaren"

		articles := []domain.Article{
			domain.Article{
				ID:     articleID1,
				Slug:   "artikel-lama",
				Name:   "Artikel Lama",
				Body:   "Ini body artikel lama",
				Status: domain.StatusPublished,
			},
			domain.Article{
				ID:     articleID2,
				Slug:   "artikel-baru",
				Name:   "Artikel Baru",
				Body:   "Ini body artikel baru",
				Status: domain.StatusPublished,
			},
			domain.Article{
				ID:     articleID3,
				Slug:   "artikel-kemaren",
				Name:   "Artikel kemaren",
				Body:   "Ini body artikel kemaren",
				Status: domain.StatusDraft,
			},
		}

		storage := storage.ArticleStorage{
			ArticleMap: articles,
		}

		query := NewArticleQueryInMemory(&storage)

		// when

		result := query.FindArticleBySlug(slug1)

		// then
		assert.Error(t, result.Error)
	})

	t.Run("Can get article by category and authorID", func(t *testing.T) {
		// given
		articleID1 := uuid.NewV4()
		articleID2 := uuid.NewV4()
		articleID3 := uuid.NewV4()

		authorID1 := uuid.NewV4()
		authorID2 := uuid.NewV4()

		categoryReq := domain.CategoryNews

		articles := []domain.Article{
			domain.Article{
				ID:       articleID1,
				Slug:     "artikel-lama",
				Name:     "Artikel Lama",
				Body:     "Ini body artikel lama",
				Category: []string{domain.CategoryNews, domain.CategoryPolitic},
				AuthorID: authorID1,
				Status:   domain.StatusPublished,
			},
			domain.Article{
				ID:       articleID2,
				Slug:     "artikel-baru",
				Name:     "Artikel Baru",
				Body:     "Ini body artikel baru",
				Category: []string{domain.CategoryNews, domain.CategoryEntertainment},
				AuthorID: authorID1,
				Status:   domain.StatusPublished,
			},
			domain.Article{
				ID:       articleID3,
				Slug:     "artikel-kemaren",
				Name:     "Artikel kemaren",
				Body:     "Ini body artikel kemaren",
				Category: []string{domain.CategoryAutomotive, domain.CategoryPolitic},
				AuthorID: authorID2,
				Status:   domain.StatusDraft,
			},
		}
		storage := storage.ArticleStorage{
			ArticleMap: articles,
		}
		query := NewArticleQueryInMemory(&storage)

		// when
		result := query.GetArticlesByCategoryAndAuthorID(categoryReq, authorID1.String())

		// then
		assert.NotEmpty(t, result.Result)
	})
}

func TestRelatedArticles(t *testing.T) {
	articleID1 := uuid.NewV4()
	articleID2 := uuid.NewV4()
	articleID3 := uuid.NewV4()

	articles := []domain.Article{
		domain.Article{
			ID:       articleID1,
			Slug:     "artikel-lama",
			Name:     "Artikel Lama",
			Body:     "Ini body artikel lama",
			Category: []string{domain.CategoryAutomotive, domain.CategoryEntertainment},
			Status:   domain.StatusPublished,
		},
		domain.Article{
			ID:       articleID2,
			Slug:     "artikel-baru",
			Name:     "Artikel Baru",
			Body:     "Ini body artikel baru",
			Category: []string{domain.CategoryAutomotive},
			Status:   domain.StatusPublished,
		},
		domain.Article{
			ID:       articleID3,
			Slug:     "artikel-kemaren",
			Name:     "Artikel kemaren",
			Body:     "Ini body artikel kemaren",
			Category: []string{domain.CategoryNews},
			Status:   domain.StatusDraft,
		},
		domain.Article{
			ID:       articleID3,
			Slug:     "artikel-oke",
			Name:     "Artikel Oke",
			Body:     "Ini body artikel oke",
			Category: []string{domain.CategoryNews},
			Status:   domain.StatusPublished,
		},
	}

	articleStorage := storage.ArticleStorage{
		ArticleMap: articles,
	}

	query := NewArticleQueryInMemory(&articleStorage)

	t.Run("Correct", func(t *testing.T) {
		// given
		searchedCategory := []string{domain.CategoryNews}

		// when
		result := query.GetRelatedArticles(searchedCategory)

		// then
		assert.Equal(t, articles[3], result.Result.([]domain.Article)[0])
		assert.NoError(t, result.Error)
	})

	t.Run("No result", func(t *testing.T) {
		// given
		searchedCategory := []string{domain.CategoryPolitic}

		// when
		result := query.GetRelatedArticles(searchedCategory)

		// then
		assert.NotEmpty(t, result.Result)
	})

	t.Run("Not default category", func(t *testing.T) {
		// given
		searchedCategory := []string{"KOMUNITAS"}

		// when
		result := query.GetRelatedArticles(searchedCategory)

		// then
		assert.NotEmpty(t, result.Result)
	})

	t.Run("Multiple search categories", func(t *testing.T) {
		// given
		searchedCategory := []string{domain.CategoryAutomotive, domain.CategoryNews}

		// when
		result := query.GetRelatedArticles(searchedCategory)

		// then
		assert.Contains(t, result.Result.([]domain.Article), articles[0])
		assert.Contains(t, result.Result.([]domain.Article), articles[1])
		assert.Contains(t, result.Result.([]domain.Article), articles[3])
		assert.NoError(t, result.Error)
	})

	t.Run("Search with no category", func(t *testing.T) {
		// given
		searchedCategory := []string{}

		// when
		result := query.GetRelatedArticles(searchedCategory)

		// then
		assert.NotEmpty(t, result.Result)
	})
}

func TestAuthorPublishedArticle(t *testing.T) {
	articleID1, _ := uuid.FromString("85cf8ed1-63df-4381-834a-dcf9af5e96cc")
	articleID2, _ := uuid.FromString("d5874a9b-cce5-40a5-afa3-ad6798b9a71c")
	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")
	userID2, _ := uuid.FromString("42217852-9bb2-44f8-baea-fd5dbdced2b7")

	articles := []domain.Article{
		domain.Article{
			ID:       articleID1,
			Slug:     "artikel-lama",
			Name:     "Artikel Lama",
			Body:     "Ini body artikel lama",
			Status:   domain.StatusPublished,
			AuthorID: userID1,
		},
		domain.Article{
			ID:       articleID2,
			Slug:     "artikel-baru",
			Name:     "Artikel Baru",
			Body:     "Ini body artikel baru",
			Status:   domain.StatusPublished,
			AuthorID: userID2,
		},
		domain.Article{
			ID:       articleID1,
			Slug:     "artikel-keren",
			Name:     "Artikel Keren",
			Body:     "Ini body artikel keren",
			Status:   domain.StatusDraft,
			AuthorID: userID1,
		},
		domain.Article{
			ID:       articleID2,
			Slug:     "artikel-mantap",
			Name:     "Artikel Mantap",
			Body:     "Ini body artikel mantap",
			Status:   domain.StatusPublished,
			AuthorID: userID2,
		},
	}

	storage := &storage.ArticleStorage{
		ArticleMap: articles,
	}

	query := NewArticleQueryInMemory(storage)

	t.Run("Correct", func(t *testing.T) {
		// given
		authorID := userID2

		// when
		result := query.GetPublishedArticlesByAuthorID(authorID.String())

		// then
		assert.NoError(t, result.Error)
		assert.Contains(t, result.Result, articles[1])
		assert.Contains(t, result.Result, articles[3])
		assert.NotContains(t, result.Result, articles[0])
		assert.NotContains(t, result.Result, articles[2])
	})

	t.Run("Draft article not returned", func(t *testing.T) {
		// given
		authorID := userID1

		// when
		result := query.GetPublishedArticlesByAuthorID(authorID.String())

		// then
		assert.NoError(t, result.Error)
		assert.Contains(t, result.Result, articles[0])
		assert.NotContains(t, result.Result, articles[1])
		assert.NotContains(t, result.Result, articles[2])
		assert.NotContains(t, result.Result, articles[3])
	})

	t.Run("No result", func(t *testing.T) {
		// given
		authorID := uuid.NewV4()

		// when
		result := query.GetPublishedArticlesByAuthorID(authorID.String())

		// then
		assert.NoError(t, result.Error)
		assert.NotContains(t, result.Result, articles[0])
		assert.NotContains(t, result.Result, articles[1])
		assert.NotContains(t, result.Result, articles[2])
		assert.NotContains(t, result.Result, articles[3])
	})
}

func TestGetArticlesByCategory(t *testing.T) {
	articleID1, _ := uuid.FromString("85cf8ed1-63df-4381-834a-dcf9af5e96cc")
	articleID2, _ := uuid.FromString("d5874a9b-cce5-40a5-afa3-ad6798b9a71c")
	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")
	userID2, _ := uuid.FromString("42217852-9bb2-44f8-baea-fd5dbdced2b7")

	articles := []domain.Article{
		domain.Article{
			ID:       articleID1,
			Slug:     "artikel-lama",
			Name:     "Artikel Lama",
			Body:     "Ini body artikel lama",
			Status:   domain.StatusPublished,
			Category: []string{domain.CategoryNews, domain.CategoryPolitic},
			AuthorID: userID1,
		},
		domain.Article{
			ID:       articleID2,
			Slug:     "artikel-baru",
			Name:     "Artikel Baru",
			Body:     "Ini body artikel baru",
			Status:   domain.StatusPublished,
			Category: []string{domain.CategoryNews, domain.CategoryEntertainment},
			AuthorID: userID2,
		},
		domain.Article{
			ID:       articleID1,
			Slug:     "artikel-keren",
			Name:     "Artikel Keren",
			Body:     "Ini body artikel keren",
			Status:   domain.StatusDraft,
			Category: []string{domain.CategoryNews, domain.CategoryPolitic},
			AuthorID: userID1,
		},
		domain.Article{
			ID:       articleID2,
			Slug:     "artikel-mantap",
			Name:     "Artikel Mantap",
			Body:     "Ini body artikel mantap",
			Status:   domain.StatusDraft,
			Category: []string{domain.CategoryAutomotive, domain.CategoryPolitic},
			AuthorID: userID2,
		},
	}

	storage := &storage.ArticleStorage{
		ArticleMap: articles,
	}

	query := NewArticleQueryInMemory(storage)

	t.Run("Correct", func(t *testing.T) {
		// given
		newsCategory := domain.CategoryNews

		// when
		result := query.GetArticlesByCategory(newsCategory)

		// then
		assert.NoError(t, result.Error)
		assert.Equal(t, 2, len(result.Result.([]domain.Article)))
	})
}

func TestGetArticlesByAuthorIDAndName(t *testing.T) {
	articleID1, _ := uuid.FromString("85cf8ed1-63df-4381-834a-dcf9af5e96cc")
	articleID2, _ := uuid.FromString("d5874a9b-cce5-40a5-afa3-ad6798b9a71c")
	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")
	userID2, _ := uuid.FromString("42217852-9bb2-44f8-baea-fd5dbdced2b7")

	articles := []domain.Article{
		domain.Article{
			ID:       articleID1,
			Slug:     "artikel-lama",
			Name:     "Artikel Lama",
			Body:     "Ini body artikel lama",
			Status:   domain.StatusPublished,
			Category: []string{domain.CategoryNews, domain.CategoryPolitic},
			AuthorID: userID1,
		},
		domain.Article{
			ID:       articleID2,
			Slug:     "artikel-baru",
			Name:     "Artikel Baru",
			Body:     "Ini body artikel baru",
			Status:   domain.StatusPublished,
			Category: []string{domain.CategoryNews, domain.CategoryEntertainment},
			AuthorID: userID2,
		},
		domain.Article{
			ID:       articleID1,
			Slug:     "artikel-keren",
			Name:     "Artikel Keren",
			Body:     "Ini body artikel keren",
			Status:   domain.StatusDraft,
			Category: []string{domain.CategoryNews, domain.CategoryPolitic},
			AuthorID: userID1,
		},
		domain.Article{
			ID:       articleID2,
			Slug:     "artikel-mantap",
			Name:     "Artikel Mantap",
			Body:     "Ini body artikel mantap",
			Status:   domain.StatusDraft,
			Category: []string{domain.CategoryAutomotive, domain.CategoryPolitic},
			AuthorID: userID2,
		},
	}

	storage := &storage.ArticleStorage{
		ArticleMap: articles,
	}

	query := NewArticleQueryInMemory(storage)

	t.Run("Correct", func(t *testing.T) {
		// when
		result := query.GetArticlesByAuthorIDAndName(userID1.String(), "Lama")

		// then
		assert.NoError(t, result.Error)
		assert.Contains(t, result.Result, articles[0])
		assert.NotContains(t, result.Result, articles[1])
	})

	t.Run("No result", func(t *testing.T) {
		// when
		result := query.GetArticlesByAuthorIDAndName(userID1.String(), "hmmmmmmmmmm")

		// then
		assert.NoError(t, result.Error)
		assert.NotContains(t, result.Result, articles[0])
		assert.NotContains(t, result.Result, articles[2])
	})

	t.Run("Multiple result", func(t *testing.T) {
		// when
		result := query.GetArticlesByAuthorIDAndName(userID2.String(), "Artikel")

		// result
		assert.NoError(t, result.Error)
		assert.Equal(t, len(result.Result.([]domain.Article)), 2)
		assert.Contains(t, result.Result, articles[1])
		assert.Contains(t, result.Result, articles[3])
	})

	t.Run("Match name but not the author", func(t *testing.T) {
		// when
		result := query.GetArticlesByAuthorIDAndName(userID1.String(), "Baru")

		// then
		assert.NoError(t, result.Error)
		assert.Empty(t, result.Result)
		assert.NotContains(t, result.Result, articles[1])
	})

	t.Run("CamelCase doesnt matter to us", func(t *testing.T) {
		// when
		result := query.GetArticlesByAuthorIDAndName(userID2.String(), "bARu")

		// then
		assert.NoError(t, result.Error)
		assert.Contains(t, result.Result, articles[1])
	})

	t.Run("Draft article also returned", func(t *testing.T) {
		// when
		result := query.GetArticlesByAuthorIDAndName(userID1.String(), "Keren")

		// then
		assert.NoError(t, result.Error)
		assert.Contains(t, result.Result, articles[2])
	})
}

func TestGetArticlesByAuthorID(t *testing.T) {
	articleID1, _ := uuid.FromString("85cf8ed1-63df-4381-834a-dcf9af5e96cc")
	articleID2, _ := uuid.FromString("d5874a9b-cce5-40a5-afa3-ad6798b9a71c")
	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")
	userID2, _ := uuid.FromString("42217852-9bb2-44f8-baea-fd5dbdced2b7")

	articles := []domain.Article{
		domain.Article{
			ID:       articleID1,
			Slug:     "artikel-lama",
			Name:     "Artikel Lama",
			Body:     "Ini body artikel lama",
			Status:   domain.StatusPublished,
			Category: []string{domain.CategoryNews, domain.CategoryPolitic},
			AuthorID: userID1,
		},
		domain.Article{
			ID:       articleID2,
			Slug:     "artikel-baru",
			Name:     "Artikel Baru",
			Body:     "Ini body artikel baru",
			Status:   domain.StatusPublished,
			Category: []string{domain.CategoryNews, domain.CategoryEntertainment},
			AuthorID: userID2,
		},
		domain.Article{
			ID:       articleID1,
			Slug:     "artikel-keren",
			Name:     "Artikel Keren",
			Body:     "Ini body artikel keren",
			Status:   domain.StatusDraft,
			Category: []string{domain.CategoryNews, domain.CategoryPolitic},
			AuthorID: userID1,
		},
		domain.Article{
			ID:       articleID2,
			Slug:     "artikel-mantap",
			Name:     "Artikel Mantap",
			Body:     "Ini body artikel mantap",
			Status:   domain.StatusDraft,
			Category: []string{domain.CategoryAutomotive, domain.CategoryPolitic},
			AuthorID: userID2,
		},
	}

	storage := &storage.ArticleStorage{
		ArticleMap: articles,
	}

	query := NewArticleQueryInMemory(storage)

	t.Run("Correct", func(t *testing.T) {
		// given
		authorID := userID1.String()

		// when
		result := query.GetArticlesByAuthorID(authorID)

		// then
		assert.NoError(t, result.Error)
		assert.Equal(t, 2, len(result.Result.([]domain.Article)))
	})
}
