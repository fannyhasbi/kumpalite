package inmemory

import (
	"testing"

	"github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/fannyhasbi/kumpalite/article/domain"
	"gitlab.com/fannyhasbi/kumpalite/article/storage"
)

func TestSaveArticle(t *testing.T) {
	t.Run("Can save", func(t *testing.T) {
		// given
		articleID1 := uuid.NewV4()

		article := domain.Article{
			ID:     articleID1,
			Slug:   "artikel-lama",
			Name:   "Artikel Lama",
			Body:   "Ini adalah body artikel lama",
			Status: domain.StatusPublished,
		}

		storage := storage.ArticleStorage{
			ArticleMap: []domain.Article{},
		}

		repo := NewArticleRepositoryInMemory(&storage)

		// when
		err := repo.Save(&article)

		// then
		assert.NoError(t, err)
		assert.Contains(t, storage.ArticleMap, article)
	})

	t.Run("Cannot save slug is same", func(t *testing.T) {
		// given
		articleID1 := uuid.NewV4()
		articleID2 := uuid.NewV4()

		article1 := domain.Article{
			ID:     articleID1,
			Slug:   "artikel-lama",
			Name:   "Artikel Lama",
			Body:   "Ini adalah body artikel lama",
			Status: domain.StatusPublished,
		}

		article2 := domain.Article{
			ID:     articleID2,
			Slug:   "artikel-lama",
			Name:   "Artikel Sebenernya Beda",
			Body:   "Ini adalah body sebenernya beda",
			Status: domain.StatusPublished,
		}

		storage := storage.ArticleStorage{
			ArticleMap: []domain.Article{},
		}

		repo := NewArticleRepositoryInMemory(&storage)

		// when
		err1 := repo.Save(&article1)
		err2 := repo.Save(&article2)

		// then
		assert.NoError(t, err1)
		assert.Contains(t, storage.ArticleMap, article1)
		assert.Error(t, err2)
		assert.NotContains(t, storage.ArticleMap, article2)
	})

	t.Run("Title less than the required", func(t *testing.T) {
		// given
		articleID1 := uuid.NewV4()

		article1 := domain.Article{
			ID:     articleID1,
			Slug:   "artikel-lama",
			Name:   "a",
			Body:   "Ini adalah body artikel lama",
			Status: domain.StatusPublished,
		}

		storage := storage.ArticleStorage{
			ArticleMap: []domain.Article{},
		}

		repo := NewArticleRepositoryInMemory(&storage)

		// when
		err := repo.Save(&article1)

		// then
		assert.Error(t, err)
		assert.NotContains(t, storage.ArticleMap, article1)
	})
}
