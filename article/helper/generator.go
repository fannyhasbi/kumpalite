package helper

import (
	"regexp"
	"strconv"
	"strings"
	"time"
)

// RemoveSymbols remove symbols for skug
func RemoveSymbols(s string) string {
	reg := regexp.MustCompile(`[\\~#%&*{}\/:<>?|%\"']`)
	s = reg.ReplaceAllString(s, `$1.$2`)
	return s
}

// GenerateSlug generate a slug based on name
func GenerateSlug(s string) string {
	s = strings.ToLower(s)
	s = strings.Replace(s, " ", "-", -1)
	n := strconv.FormatInt(time.Now().Unix(), 10)
	s = s + "-" + n
	s = RemoveSymbols(s)

	return s
}
