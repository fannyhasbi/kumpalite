package helper

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGenerateSlug(t *testing.T) {
	// given =
	str := "hello world"

	// when
	result := GenerateSlug(str)

	t.Log(result)

	// then
	assert.NotEmpty(t, result)
}
