package helper

import (
	"log"
	"time"
)

// TimeZone for the local timezone
var TimeZone *time.Location

func init() {
	// t, err := time.LoadLocation("Asia/Jakarta")
	// if err != nil {
	// 	log.Println("TIMEZONE_PARSING][ERROR]", t, err)
	// }
	// log.Println(t)
	log.Println(time.Local)
	TimeZone = time.Local
}
