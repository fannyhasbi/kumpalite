package handler

import (
	"github.com/golang/protobuf/ptypes"
	"gitlab.com/fannyhasbi/kumpalite/article/domain"
	pb "gitlab.com/fannyhasbi/kumpalite/article/proto"
)

// ParseArticleToProto parse article domain struct into article proto struct
func ParseArticleToProto(param domain.Article) *pb.Article {
	createdAt, _ := ptypes.TimestampProto(param.CreatedAt)
	updatedAt, _ := ptypes.TimestampProto(param.UpdatedAt)

	var response pb.Article
	response.ID = param.ID.String()
	response.Slug = param.Slug
	response.Name = param.Name
	response.Body = param.Body
	response.Status = param.Status
	response.Category = param.Category
	response.CreatedAt = createdAt
	response.UpdatedAt = updatedAt
	response.AuthorID = param.AuthorID.String()
	response.CommentsCount = param.CommentsCount
	response.ImageURL = param.ImageURL

	return &response
}
