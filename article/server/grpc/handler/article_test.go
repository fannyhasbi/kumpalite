package handler

import (
	"context"
	"testing"
	"time"

	"github.com/golang/protobuf/ptypes/empty"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/fannyhasbi/kumpalite/article/domain"
	pb "gitlab.com/fannyhasbi/kumpalite/article/proto"
)

type MockArticleService struct {
	mock.Mock
}

func (mock MockArticleService) CreateArticle(name, body, slug, status, authorID, imageURL string, categoryIDs []string) (domain.Article, error) {
	args := mock.Called(name, body, slug, status, authorID, imageURL, categoryIDs)
	return args.Get(0).(domain.Article), args.Error(1)
}

func (mock MockArticleService) GetPublishedArticles() []domain.Article {
	args := mock.Called()
	return args.Get(0).([]domain.Article)
}

func (mock MockArticleService) GetArticlesByName(name string) ([]domain.Article, error) {
	args := mock.Called(name)
	return args.Get(0).([]domain.Article), args.Error(1)
}

func (mock MockArticleService) FindArticleBySlug(slug string) (domain.Article, error) {
	args := mock.Called(slug)
	return args.Get(0).(domain.Article), args.Error(1)
}

func (mock MockArticleService) GetArticlesByCategoryAndAuthorID(category, authorID string) ([]domain.Article, error) {
	args := mock.Called(category, authorID)
	return args.Get(0).([]domain.Article), args.Error(1)
}

func (mock MockArticleService) GetRelatedArticles(category []string) ([]domain.Article, error) {
	args := mock.Called(category)
	return args.Get(0).([]domain.Article), nil
}

func (mock MockArticleService) GetPublishedArticlesByAuthorID(id string) ([]domain.Article, error) {
	args := mock.Called(id)
	return args.Get(0).([]domain.Article), nil
}

func (mock MockArticleService) GetArticlesByCategory(category string) ([]domain.Article, error) {
	args := mock.Called(category)
	return args.Get(0).([]domain.Article), nil
}

func (mock MockArticleService) GetArticlesByAuthorIDAndName(authorID, name string) []domain.Article {
	args := mock.Called(authorID, name)
	return args.Get(0).([]domain.Article)
}

func (mock MockArticleService) GetArticlesByAuthorID(authorID string) ([]domain.Article, error) {
	args := mock.Called(authorID)
	return args.Get(0).([]domain.Article), nil
}

func TestCanGetPublishedArticles(t *testing.T) {
	// given
	articleID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")
	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")

	articles := []domain.Article{
		domain.Article{
			ID:        articleID1,
			Slug:      "artikel-lama",
			Name:      "Artikel Lama",
			Body:      "Ini body artikel lama",
			Status:    domain.StatusPublished,
			Category:  []string{domain.CategoryAutomotive},
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
			AuthorID:  userID1,
		},
	}

	service := MockArticleService{}
	service.On("GetPublishedArticles").Return(articles)

	server := NewArticleServer(service)

	// when
	result, err := server.GetPublishedArticles(context.Background(), &empty.Empty{})

	// then
	assert.Nil(t, err)
	assert.Equal(t, result.Articles[0].AuthorID, articles[0].AuthorID.String())
}

func TestCanGetArticlesByName(t *testing.T) {
	// given
	articleID1 := uuid.NewV4()
	articleID2 := uuid.NewV4()
	requestName := "artikel"

	articles := []domain.Article{
		domain.Article{
			ID:        articleID1,
			Slug:      "artikel-lama",
			Name:      "Artikel Lama",
			Body:      "Ini body artikel lama",
			Status:    domain.StatusPublished,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		},
		domain.Article{
			ID:     articleID2,
			Slug:   "artikel-baru",
			Name:   "Artikel Baru",
			Body:   "Ini body artikel baru",
			Status: domain.StatusPublished,
		},
	}

	service := MockArticleService{}
	service.On("GetArticlesByName", requestName).Return(articles, nil)

	server := NewArticleServer(service)

	articleRequest := pb.ArticleRequestByName{
		Name: requestName,
	}

	// when
	result, err := server.GetArticlesByName(context.Background(), &articleRequest)

	// then
	assert.NoError(t, err)
	assert.Equal(t, 2, len(result.Articles))
}

func TestCanFindArticleBySlug(t *testing.T) {
	// given
	requestSlug := "artikel-lama"
	articleID1 := uuid.NewV4()

	article := domain.Article{
		ID:        articleID1,
		Slug:      "artikel-lama",
		Name:      "Artikel Lama",
		Body:      "Ini body artikel lama",
		Status:    domain.StatusPublished,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	service := MockArticleService{}
	service.On("FindArticleBySlug", requestSlug).Return(article, nil)

	server := NewArticleServer(service)
	articleRequest := pb.ArticleRequestBySlug{
		Slug: requestSlug,
	}

	// when
	result, err := server.FindArticleBySlug(context.Background(), &articleRequest)

	// then
	assert.NoError(t, err)
	assert.Equal(t, article.Slug, result.Slug)
}

func TestGetArticlesByCategoryAndAuthorID(t *testing.T) {
	// given
	articleID1 := uuid.NewV4()
	articleID2 := uuid.NewV4()

	authorID1 := uuid.NewV4()

	categoryReq := domain.CategoryNews

	articles := []domain.Article{
		domain.Article{
			ID:       articleID1,
			Slug:     "artikel-lama",
			Name:     "Artikel Lama",
			Body:     "Ini body artikel lama",
			Category: []string{domain.CategoryNews, domain.CategoryPolitic},
			AuthorID: authorID1,
			Status:   domain.StatusPublished,
		},
		domain.Article{
			ID:       articleID2,
			Slug:     "artikel-baru",
			Name:     "Artikel Baru",
			Body:     "Ini body artikel baru",
			Category: []string{domain.CategoryNews, domain.CategoryEntertainment},
			AuthorID: authorID1,
			Status:   domain.StatusPublished,
		},
	}

	service := MockArticleService{}
	service.On("GetArticlesByCategoryAndAuthorID", categoryReq, authorID1.String()).Return(articles, nil)

	server := NewArticleServer(service)

	articleRequest := pb.ArticleRequestByCategoryAndAuthorID{
		AuthorID: authorID1.String(),
		Category: categoryReq,
	}

	// when
	result, err := server.GetArticlesByCategoryAndAuthorID(context.Background(), &articleRequest)

	// then
	assert.NoError(t, err)
	assert.NotEmpty(t, result)
	assert.Equal(t, 2, len(result.Articles))
}

func TestGetRelatedArticles(t *testing.T) {
	// given
	articleID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")
	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")

	articles := []domain.Article{
		domain.Article{
			ID:        articleID1,
			Slug:      "artikel-lama",
			Name:      "Artikel Lama",
			Body:      "Ini body artikel lama",
			Status:    domain.StatusPublished,
			Category:  []string{domain.CategoryAutomotive},
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
			AuthorID:  userID1,
			ImageURL:  "https://upload.wikimedia.org/wikipedia/commons/b/b0/Logo-plus-simple.png",
		},
		domain.Article{
			ID:        articleID1,
			Slug:      "artikel-baru",
			Name:      "Artikel Baru",
			Body:      "Ini body artikel baru",
			Status:    domain.StatusPublished,
			Category:  []string{domain.CategoryAutomotive},
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
			AuthorID:  userID1,
			ImageURL:  "https://upload.wikimedia.org/wikipedia/commons/b/b0/Logo-plus-simple.png",
		},
	}

	searchedCategory := []string{domain.CategoryAutomotive}

	service := MockArticleService{}
	service.On("GetRelatedArticles", searchedCategory).Return(articles)

	server := NewArticleServer(service)

	relatedRequest := &pb.RelatedArticleRequst{
		Category: searchedCategory,
	}

	// when
	result, err := server.GetRelatedArticles(context.Background(), relatedRequest)

	// then
	assert.Nil(t, err)
	assert.NotEmpty(t, result.Articles)
}

func TestGetPublishedArticlesByAuthorID(t *testing.T) {
	// given
	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")

	articleID1 := uuid.NewV4()
	articleID2 := uuid.NewV4()

	articles := []domain.Article{
		domain.Article{
			ID:       articleID1,
			Slug:     "artikel-lama",
			Name:     "Artikel Lama",
			Body:     "Ini body artikel lama",
			Status:   domain.StatusPublished,
			AuthorID: userID1,
			ImageURL: "https://upload.wikimedia.org/wikipedia/commons/b/b0/Logo-plus-simple.png",
		},
		domain.Article{
			ID:       articleID2,
			Slug:     "artikel-baru",
			Name:     "Artikel Baru",
			Body:     "Ini body artikel baru",
			Status:   domain.StatusPublished,
			AuthorID: userID1,
			ImageURL: "https://upload.wikimedia.org/wikipedia/commons/b/b0/Logo-plus-simple.png",
		},
	}

	service := MockArticleService{}
	service.On("GetPublishedArticlesByAuthorID", userID1.String()).Return(articles)

	server := NewArticleServer(service)

	authorIDRequest := &pb.ArticleRequestByAuthorID{
		AuthorID: userID1.String(),
	}

	// when
	result, err := server.GetPublishedArticlesByAuthorID(context.Background(), authorIDRequest)

	// then
	assert.NoError(t, err)
	assert.Equal(t, 2, len(result.Articles))
}

func TestGetArticlesByCategory(t *testing.T) {
	// given
	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")

	articleID1 := uuid.NewV4()
	articleID2 := uuid.NewV4()

	newsCategory := domain.CategoryNews

	articles := []domain.Article{
		domain.Article{
			ID:       articleID1,
			Slug:     "artikel-lama",
			Name:     "Artikel Lama",
			Body:     "Ini body artikel lama",
			Status:   domain.StatusPublished,
			Category: []string{domain.CategoryNews, domain.CategoryPolitic},
			AuthorID: userID1,
			ImageURL: "https://upload.wikimedia.org/wikipedia/commons/b/b0/Logo-plus-simple.png",
		},
		domain.Article{
			ID:       articleID2,
			Slug:     "artikel-baru",
			Name:     "Artikel Baru",
			Body:     "Ini body artikel baru",
			Category: []string{domain.CategoryNews, domain.CategoryEntertainment},
			Status:   domain.StatusPublished,
			AuthorID: userID1,
			ImageURL: "https://upload.wikimedia.org/wikipedia/commons/b/b0/Logo-plus-simple.png",
		},
	}

	service := MockArticleService{}
	service.On("GetArticlesByCategory", newsCategory).Return(articles)

	server := NewArticleServer(service)

	articleCategoryRequest := &pb.ArticleRequestByCategory{
		Category: newsCategory,
	}

	// when
	result, err := server.GetArticlesByCategory(context.Background(), articleCategoryRequest)

	// then
	assert.NoError(t, err)
	assert.Equal(t, 2, len(result.Articles))
}

func TestGetArticlesByAuthorID(t *testing.T) {
	// given
	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")

	articleID1 := uuid.NewV4()
	articleID2 := uuid.NewV4()

	articles := []domain.Article{
		domain.Article{
			ID:       articleID1,
			Slug:     "artikel-lama",
			Name:     "Artikel Lama",
			Body:     "Ini body artikel lama",
			Status:   domain.StatusPublished,
			Category: []string{domain.CategoryNews, domain.CategoryPolitic},
			AuthorID: userID1,
			ImageURL: "https://upload.wikimedia.org/wikipedia/commons/b/b0/Logo-plus-simple.png",
		},
		domain.Article{
			ID:       articleID2,
			Slug:     "artikel-baru",
			Name:     "Artikel Baru",
			Body:     "Ini body artikel baru",
			Category: []string{domain.CategoryNews, domain.CategoryEntertainment},
			Status:   domain.StatusPublished,
			AuthorID: userID1,
			ImageURL: "https://upload.wikimedia.org/wikipedia/commons/b/b0/Logo-plus-simple.png",
		},
	}

	service := MockArticleService{}
	service.On("GetArticlesByAuthorID", userID1.String()).Return(articles)

	server := NewArticleServer(service)

	articleAuthorRequest := &pb.ArticleRequestByAuthorID{
		AuthorID: userID1.String(),
	}

	// when
	result, err := server.GetArticlesByAuthorID(context.Background(), articleAuthorRequest)

	// then
	assert.NoError(t, err)
	assert.Equal(t, 2, len(result.Articles))
}

func TestCreateArticle(t *testing.T) {
	// given
	articleID1, _ := uuid.FromString("85cf8ed1-63df-4381-834a-dcf9af5e96cc")
	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")

	article := domain.Article{
		ID:       articleID1,
		Slug:     "artikel-lama",
		Name:     "Artikel Lama",
		Body:     "Ini body artikel lama",
		Status:   domain.StatusPublished,
		Category: []string{"85cf8ed1-63df-4381-834a-dcf9af5e96cc", "ee833113-2169-43d3-8b72-b484fb457b44"},
		AuthorID: userID1,
		ImageURL: "https://upload.wikimedia.org/wikipedia/commons/b/b0/Logo-plus-simple.png",
	}

	articleResponse := domain.Article{
		ID:       articleID1,
		Slug:     "artikel-lama",
		Name:     "Artikel Lama",
		Body:     "Ini body artikel lama",
		Status:   domain.StatusPublished,
		Category: []string{"85cf8ed1-63df-4381-834a-dcf9af5e96cc", "ee833113-2169-43d3-8b72-b484fb457b44"},
		AuthorID: userID1,
		ImageURL: "https://upload.wikimedia.org/wikipedia/commons/b/b0/Logo-plus-simple.png",
	}

	service := MockArticleService{}
	service.On(
		"CreateArticle",
		article.Name,
		article.Body,
		article.Slug,
		article.Status,
		article.AuthorID.String(),
		article.ImageURL,
		article.Category).Return(articleResponse, nil)

	server := NewArticleServer(service)

	createRequest := &pb.CreateRequest{
		Name:        article.Name,
		Body:        article.Body,
		Slug:        article.Slug,
		Status:      article.Status,
		AuthorID:    article.AuthorID.String(),
		ImageURL:    article.ImageURL,
		CategoryIDs: article.Category,
	}

	// when
	result, err := server.CreateArticle(context.Background(), createRequest)

	// then
	assert.Nil(t, err)
	assert.Equal(t, article.Name, result.Name)
}

func TestGetArticlesByAuthorIDAndName(t *testing.T) {
	// given
	articleID := uuid.NewV4()
	userID := uuid.NewV4()
	requestName := "artikel"

	articles := []domain.Article{
		domain.Article{
			ID:        articleID,
			Slug:      "artikel-lama",
			Name:      "Artikel Lama",
			Body:      "Ini body artikel lama",
			Status:    domain.StatusPublished,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
			AuthorID:  userID,
		},
	}

	service := MockArticleService{}
	service.On("GetArticlesByAuthorIDAndName", userID.String(), requestName).Return(articles)

	server := NewArticleServer(service)

	articleRequest := pb.ArticleRequestByAuthorIDAndName{
		AuthorID: userID.String(),
		Name:     requestName,
	}

	// when
	result, err := server.GetArticlesByAuthorIDAndName(context.Background(), &articleRequest)

	// then
	assert.NoError(t, err)
	assert.Equal(t, result.Articles[0].Slug, articles[0].Slug)
}
