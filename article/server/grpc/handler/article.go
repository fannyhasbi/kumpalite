package handler

import (
	"context"
	"log"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/fannyhasbi/kumpalite/article/config"
	"gitlab.com/fannyhasbi/kumpalite/article/domain"
	pb "gitlab.com/fannyhasbi/kumpalite/article/proto"
)

// ArticleServerService wrap Article service interface
type ArticleServerService interface {
	CreateArticle(id, name, body, slug, status, authorID, imageURL string, categoryIDs []string) (domain.Article, error)
	GetPublishedArticles() []domain.Article
	GetArticlesByName(name string) ([]domain.Article, error)
	FindArticleBySlug(slug string) (domain.Article, error)
	GetArticlesByCategoryAndAuthorID(category, authorID string) ([]domain.Article, error)
	GetRelatedArticles(category []string) ([]domain.Article, error)
	GetPublishedArticlesByAuthorID(id string) ([]domain.Article, error)
	GetArticlesByCategory(category string) ([]domain.Article, error)
	GetArticlesByAuthorIDAndName(authorID, name string) []domain.Article
	GetArticlesByAuthorID(authorID string) ([]domain.Article, error)
	GetPublishedArticlesByCategorySlug(slug string) ([]domain.Article, error)
}

// ArticleServer wrap article service handler
type ArticleServer struct {
	ArticleService ArticleServerService
	CommentService pb.CommentServiceClient
}

// NewArticleServer create new Article server instance
func NewArticleServer(service ArticleServerService) pb.ArticleServiceServer {
	return &ArticleServer{
		ArticleService: service,
		CommentService: config.InitCommentGRPC(),
	}
}

// CreateArticle create an article
func (as ArticleServer) CreateArticle(ctx context.Context, params *pb.CreateRequest) (*pb.Article, error) {
	result, err := as.ArticleService.CreateArticle(params.Id, params.Name, params.Body, params.Slug, params.Status, params.AuthorID, params.ImageURL, params.CategoryIDs)
	if err != nil {
		return &pb.Article{}, err
	}

	response := ParseArticleToProto(result)
	log.Println(response.CreatedAt.String())
	log.Println(response.UpdatedAt.String())

	return response, nil
}

// GetPublishedArticles get all published articles
func (as ArticleServer) GetPublishedArticles(ctx context.Context, _ *empty.Empty) (*pb.ArticleList, error) {
	result := as.ArticleService.GetPublishedArticles()

	var articles []*pb.Article
	for _, v := range result {
		commentRequest := &pb.CommentRequest{
			ArticleID: v.ID.String(),
		}
		commentCount, _ := as.CommentService.GetCommentCountByArticleID(ctx, commentRequest)
		v.CommentsCount = commentCount.Count

		article := ParseArticleToProto(v)

		articles = append(articles, article)
	}

	return &pb.ArticleList{
		Articles: articles,
	}, nil
}

// GetArticlesByName is get all articles with similiar name
func (as ArticleServer) GetArticlesByName(ctx context.Context, params *pb.ArticleRequestByName) (*pb.ArticleList, error) {
	result, _ := as.ArticleService.GetArticlesByName(params.Name)

	var articles []*pb.Article
	for _, v := range result {
		commentRequest := &pb.CommentRequest{
			ArticleID: v.ID.String(),
		}
		commentCount, _ := as.CommentService.GetCommentCountByArticleID(ctx, commentRequest)
		v.CommentsCount = commentCount.Count

		article := ParseArticleToProto(v)

		articles = append(articles, article)
	}

	return &pb.ArticleList{
		Articles: articles,
	}, nil
}

// FindArticleBySlug is find article by slug
func (as ArticleServer) FindArticleBySlug(ctx context.Context, params *pb.ArticleRequestBySlug) (*pb.Article, error) {
	result, _ := as.ArticleService.FindArticleBySlug(params.Slug)
	commentRequest := &pb.CommentRequest{
		ArticleID: result.ID.String(),
	}
	commentCount, _ := as.CommentService.GetCommentCountByArticleID(ctx, commentRequest)
	result.CommentsCount = commentCount.Count

	article := ParseArticleToProto(result)

	return article, nil
}

// GetArticlesByCategoryAndAuthorID is find articles with specific category and belong to specific author
func (as ArticleServer) GetArticlesByCategoryAndAuthorID(ctx context.Context, params *pb.ArticleRequestByCategoryAndAuthorID) (*pb.ArticleList, error) {
	result, _ := as.ArticleService.GetArticlesByCategoryAndAuthorID(params.Category, params.AuthorID)

	var articles []*pb.Article
	for _, v := range result {
		commentRequest := &pb.CommentRequest{
			ArticleID: v.ID.String(),
		}
		commentCount, _ := as.CommentService.GetCommentCountByArticleID(ctx, commentRequest)
		v.CommentsCount = commentCount.Count

		article := ParseArticleToProto(v)

		articles = append(articles, article)
	}

	return &pb.ArticleList{
		Articles: articles,
	}, nil
}

// GetRelatedArticles get related articles by category
func (as ArticleServer) GetRelatedArticles(ctx context.Context, params *pb.RelatedArticleRequst) (*pb.ArticleList, error) {
	result, _ := as.ArticleService.GetRelatedArticles(params.Category)

	var articles []*pb.Article
	for _, v := range result {
		commentRequest := &pb.CommentRequest{
			ArticleID: v.ID.String(),
		}
		commentCount, _ := as.CommentService.GetCommentCountByArticleID(ctx, commentRequest)
		v.CommentsCount = commentCount.Count

		article := ParseArticleToProto(v)

		articles = append(articles, article)
	}

	return &pb.ArticleList{
		Articles: articles,
	}, nil
}

// GetPublishedArticlesByAuthorID get published articles by author id for public profile
func (as ArticleServer) GetPublishedArticlesByAuthorID(ctx context.Context, params *pb.ArticleRequestByAuthorID) (*pb.ArticleList, error) {
	result, _ := as.ArticleService.GetPublishedArticlesByAuthorID(params.AuthorID)

	var articles []*pb.Article
	for _, v := range result {
		commentRequest := &pb.CommentRequest{
			ArticleID: v.ID.String(),
		}
		commentCount, _ := as.CommentService.GetCommentCountByArticleID(ctx, commentRequest)
		v.CommentsCount = commentCount.Count

		article := ParseArticleToProto(v)

		articles = append(articles, article)
	}

	return &pb.ArticleList{
		Articles: articles,
	}, nil
}

// GetArticlesByCategory get published articles by category
func (as ArticleServer) GetArticlesByCategory(ctx context.Context, params *pb.ArticleRequestByCategory) (*pb.ArticleList, error) {
	result, _ := as.ArticleService.GetArticlesByCategory(params.Category)

	var articles []*pb.Article
	for _, v := range result {
		commentRequest := &pb.CommentRequest{
			ArticleID: v.ID.String(),
		}
		commentCount, _ := as.CommentService.GetCommentCountByArticleID(ctx, commentRequest)
		v.CommentsCount = commentCount.Count

		article := ParseArticleToProto(v)

		articles = append(articles, article)
	}

	return &pb.ArticleList{
		Articles: articles,
	}, nil
}

// GetArticlesByAuthorIDAndName get articles by author id and name as search feature
func (as ArticleServer) GetArticlesByAuthorIDAndName(ctx context.Context, params *pb.ArticleRequestByAuthorIDAndName) (*pb.ArticleList, error) {
	result := as.ArticleService.GetArticlesByAuthorIDAndName(params.AuthorID, params.Name)

	var articles []*pb.Article
	for _, v := range result {
		commentRequest := &pb.CommentRequest{
			ArticleID: v.ID.String(),
		}
		commentCount, _ := as.CommentService.GetCommentCountByArticleID(ctx, commentRequest)
		v.CommentsCount = commentCount.Count

		article := ParseArticleToProto(v)

		articles = append(articles, article)
	}

	return &pb.ArticleList{
		Articles: articles,
	}, nil
}

// GetArticlesByAuthorID get articles by authorID
func (as ArticleServer) GetArticlesByAuthorID(ctx context.Context, params *pb.ArticleRequestByAuthorID) (*pb.ArticleList, error) {
	result, _ := as.ArticleService.GetArticlesByAuthorID(params.AuthorID)

	var articles []*pb.Article
	for _, v := range result {
		commentRequest := &pb.CommentRequest{
			ArticleID: v.ID.String(),
		}
		commentCount, _ := as.CommentService.GetCommentCountByArticleID(ctx, commentRequest)
		v.CommentsCount = commentCount.Count

		article := ParseArticleToProto(v)

		articles = append(articles, article)
	}

	return &pb.ArticleList{
		Articles: articles,
	}, nil
}

// GetPublishedArticlesByCategorySlug get articles
func (as ArticleServer) GetPublishedArticlesByCategorySlug(ctx context.Context, params *pb.ArticleRequestByCategorySlug) (*pb.ArticleList, error) {
	result, err := as.ArticleService.GetPublishedArticlesByCategorySlug(params.Slug)
	if err != nil {
		return &pb.ArticleList{}, err
	}

	var articles []*pb.Article
	for _, v := range result {
		commentRequest := &pb.CommentRequest{
			ArticleID: v.ID.String(),
		}
		commentCount, _ := as.CommentService.GetCommentCountByArticleID(ctx, commentRequest)
		v.CommentsCount = commentCount.Count

		article := ParseArticleToProto(v)

		articles = append(articles, article)
	}

	return &pb.ArticleList{
		Articles: articles,
	}, nil
}
