package main

import (
	"log"
	"net"

	_ "github.com/lib/pq"
	"gitlab.com/fannyhasbi/kumpalite/article/config"
	pb "gitlab.com/fannyhasbi/kumpalite/article/proto"
	"gitlab.com/fannyhasbi/kumpalite/article/server/grpc/handler"
	"gitlab.com/fannyhasbi/kumpalite/article/services"
	"google.golang.org/grpc"
)

func main() {
	port := ":9002"
	srv := grpc.NewServer()
	var articleServer handler.ArticleServer

	articleServer.ArticleService = services.NewArticleService()
	articleServer.CommentService = config.InitCommentGRPC()

	pb.RegisterArticleServiceServer(srv, articleServer)

	log.Println("Starting article RPC server at", port)

	listen, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("Could not listen to %s: %v", port, err)
	}

	log.Fatal("Article server is listening", srv.Serve(listen))
}
