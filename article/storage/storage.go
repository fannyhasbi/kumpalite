package storage

import (
	"gitlab.com/fannyhasbi/kumpalite/article/helper"
	"log"
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/fannyhasbi/kumpalite/article/domain"
)

// ArticleStorage store articles
type ArticleStorage struct {
	ArticleMap []domain.Article
}

// NewArticleStorage instantiate the article storage
func NewArticleStorage() *ArticleStorage {
	articleID1, _ := uuid.FromString("85cf8ed1-63df-4381-834a-dcf9af5e96cc")
	articleID2, _ := uuid.FromString("d5874a9b-cce5-40a5-afa3-ad6798b9a71c")
	articleID3, _ := uuid.FromString("ee833113-2169-43d3-8b72-b484fb457b44")
	articleID4, _ := uuid.FromString("215f695e-37b1-4e49-a216-34de6536840c")
	articleID5, _ := uuid.FromString("eab8c481-ee85-4369-83cb-4ed0cfb5ad21")
	articleID6, _ := uuid.FromString("7a6fca83-8bc9-427f-822e-cb6eea01efc6")

	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")
	userID2, _ := uuid.FromString("42217852-9bb2-44f8-baea-fd5dbdced2b7")
	userID3, _ := uuid.FromString("be4d01ee-a16f-48c3-8623-2ea295caa4ff")
	userID4, _ := uuid.FromString("cc8c1c6c-b726-444e-bea5-d095a7c579bc")
	userID5, _ := uuid.FromString("bbe24777-1962-492b-b1a6-c303b438c660")

	log.Println("Article ID 1 reserved for testing : ", articleID1.String())
	log.Println("Article ID 2 reserved for testing : ", articleID2.String())
	log.Println("Article ID 3 reserved for testing : ", articleID3.String())

	log.Println("TIMEZONE_HELPER", helper.TimeZone)
	log.Println("TIMEZONE", helper.TimeZone)
	log.Println("TIME_WITH_TIMEZONE", time.Now().In(helper.TimeZone))

	articles := []domain.Article{
		domain.Article{
			ID:            articleID1,
			Slug:          "artikel-lama",
			Name:          "Artikel Lama",
			Body:          "{\"blocks\":[{\"key\":\"5vdda\",\"text\":\"Catatan Redaksi: Judul berita ini sudah mengalami ubahan dari 'AHM Sodorkan Honda PCX Listrik untuk Armada Gojek' ke 'Honda PCX Listrik Jadi Armada Gojek'. Penggunaan Honda PCX Listrik sebagai unit driver Gojek merupakan tindak lanjut dari kolaborasi Astra International Tbk ke layanan transportasi berbasis aplikasi itu.\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[{\"offset\":0,\"length\":321,\"style\":\"ITALIC\"},{\"offset\":0,\"length\":17,\"style\":\"BOLD\"}],\"entityRanges\":[],\"data\":{}},{\"key\":\"4cgeb\",\"text\":\"Layanan transportasi berbasis aplikasi Gojek resmi berkolaborasi dengan Astra, untuk mengoperasikan beberapa unit Honda PCX Listrik yang disediakan PT Astra Honda Motor (AHM)  buat mengangkut penumpang.\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[{\"offset\":114,\"length\":6,\"key\":0}],\"data\":{}},{\"key\":\"fdfb6\",\"text\":\"Namun, berdasarkan unggahan Gojek pada akun resmi Instagram-nya, ini masih tahap uji coba awal. Nantinya hasil dari tes ini, akan menjadi masukan untuk pengembangan ke tahap berikutnya.⁣\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}],\"entityMap\":{\"0\":{\"type\":\"LINK\",\"mutability\":\"MUTABLE\",\"data\":{\"href\":\"https://kumparan.com/@kumparanoto/gesits-laku-1-200-unit-honda-pcx-listrik-justru-sepi-peminat-1r2444Qrgp7\",\"rel\":\"noopener noreferrer\",\"target\":\"_blank\",\"url\":\"https://kumparan.com/@kumparanoto/gesits-laku-1-200-unit-honda-pcx-listrik-justru-sepi-peminat-1r2444Qrgp7\"}}}}",
			Status:        domain.StatusPublished,
			Category:      []string{domain.CategoryAutomotive},
			CreatedAt:     time.Now().In(helper.TimeZone),
			UpdatedAt:     time.Now().In(helper.TimeZone),
			CommentsCount: 20,
			AuthorID:      userID1,
			ImageURL:      "https://www.thamesvalley.police.uk/SysSiteAssets/media/images/thames-valley/featured-content/news_website_box_640x394px.png",
		},
		domain.Article{
			ID:            articleID2,
			Slug:          "artikel-udah-dipublish",
			Name:          "Artikel Udah Dipublish",
			Body:          "{\"blocks\":[{\"key\":\"5vdda\",\"text\":\"Catatan Redaksi: Judul berita ini sudah mengalami ubahan dari 'AHM Sodorkan Honda PCX Listrik untuk Armada Gojek' ke 'Honda PCX Listrik Jadi Armada Gojek'. Penggunaan Honda PCX Listrik sebagai unit driver Gojek merupakan tindak lanjut dari kolaborasi Astra International Tbk ke layanan transportasi berbasis aplikasi itu.\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[{\"offset\":0,\"length\":321,\"style\":\"ITALIC\"},{\"offset\":0,\"length\":17,\"style\":\"BOLD\"}],\"entityRanges\":[],\"data\":{}},{\"key\":\"4cgeb\",\"text\":\"Layanan transportasi berbasis aplikasi Gojek resmi berkolaborasi dengan Astra, untuk mengoperasikan beberapa unit Honda PCX Listrik yang disediakan PT Astra Honda Motor (AHM)  buat mengangkut penumpang.\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[{\"offset\":114,\"length\":6,\"key\":0}],\"data\":{}},{\"key\":\"fdfb6\",\"text\":\"Namun, berdasarkan unggahan Gojek pada akun resmi Instagram-nya, ini masih tahap uji coba awal. Nantinya hasil dari tes ini, akan menjadi masukan untuk pengembangan ke tahap berikutnya.⁣\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}],\"entityMap\":{\"0\":{\"type\":\"LINK\",\"mutability\":\"MUTABLE\",\"data\":{\"href\":\"https://kumparan.com/@kumparanoto/gesits-laku-1-200-unit-honda-pcx-listrik-justru-sepi-peminat-1r2444Qrgp7\",\"rel\":\"noopener noreferrer\",\"target\":\"_blank\",\"url\":\"https://kumparan.com/@kumparanoto/gesits-laku-1-200-unit-honda-pcx-listrik-justru-sepi-peminat-1r2444Qrgp7\"}}}}",
			Status:        domain.StatusPublished,
			Category:      []string{domain.CategoryEntertainment},
			CreatedAt:     time.Now().In(helper.TimeZone),
			UpdatedAt:     time.Now().In(helper.TimeZone),
			CommentsCount: 21,
			AuthorID:      userID1,
		},
		domain.Article{
			ID:            articleID3,
			Slug:          "artikel-baru",
			Name:          "Artikel Baru",
			Body:          "{\"blocks\":[{\"key\":\"5vdda\",\"text\":\"Catatan Redaksi: Judul berita ini sudah mengalami ubahan dari 'AHM Sodorkan Honda PCX Listrik untuk Armada Gojek' ke 'Honda PCX Listrik Jadi Armada Gojek'. Penggunaan Honda PCX Listrik sebagai unit driver Gojek merupakan tindak lanjut dari kolaborasi Astra International Tbk ke layanan transportasi berbasis aplikasi itu.\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[{\"offset\":0,\"length\":321,\"style\":\"ITALIC\"},{\"offset\":0,\"length\":17,\"style\":\"BOLD\"}],\"entityRanges\":[],\"data\":{}},{\"key\":\"4cgeb\",\"text\":\"Layanan transportasi berbasis aplikasi Gojek resmi berkolaborasi dengan Astra, untuk mengoperasikan beberapa unit Honda PCX Listrik yang disediakan PT Astra Honda Motor (AHM)  buat mengangkut penumpang.\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[{\"offset\":114,\"length\":6,\"key\":0}],\"data\":{}},{\"key\":\"fdfb6\",\"text\":\"Namun, berdasarkan unggahan Gojek pada akun resmi Instagram-nya, ini masih tahap uji coba awal. Nantinya hasil dari tes ini, akan menjadi masukan untuk pengembangan ke tahap berikutnya.⁣\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}],\"entityMap\":{\"0\":{\"type\":\"LINK\",\"mutability\":\"MUTABLE\",\"data\":{\"href\":\"https://kumparan.com/@kumparanoto/gesits-laku-1-200-unit-honda-pcx-listrik-justru-sepi-peminat-1r2444Qrgp7\",\"rel\":\"noopener noreferrer\",\"target\":\"_blank\",\"url\":\"https://kumparan.com/@kumparanoto/gesits-laku-1-200-unit-honda-pcx-listrik-justru-sepi-peminat-1r2444Qrgp7\"}}}}",
			Status:        domain.StatusDraft,
			Category:      []string{domain.CategoryNews, domain.CategoryPolitic},
			CreatedAt:     time.Now().In(helper.TimeZone),
			UpdatedAt:     time.Now().In(helper.TimeZone),
			CommentsCount: 23,
			AuthorID:      userID2,
			ImageURL:      "https://rhondabrowningwhite.files.wordpress.com/2016/05/breaking-news.jpg?w=640",
		},
		domain.Article{
			ID:            articleID3,
			Slug:          "artikel-punya-orang",
			Name:          "Artikel Punya Orang",
			Body:          "Ini body artikel punya orang",
			Status:        domain.StatusPublished,
			Category:      []string{domain.CategoryNews},
			CreatedAt:     time.Now().In(helper.TimeZone),
			UpdatedAt:     time.Now().In(helper.TimeZone),
			CommentsCount: 20,
			AuthorID:      userID2,
			ImageURL:      "https://www.fairprice.com.sg/wps/wcm/connect/c418dcd3-3cdd-45a4-8ae4-6f4fdf29bc5e/News+Banner.jpg?MOD=AJPERES&CACHEID=c418dcd3-3cdd-45a4-8ae4-6f4fdf29bc5e",
		},
		domain.Article{
			ID:            articleID4,
			Slug:          "belajar-dan-berusaha-a-la-fadli-hidayatullah",
			Name:          "Belajar dan Berusaha a la Fadli Hidayatullah",
			Body:          "{\"blocks\":[{\"key\":\"5vdda\",\"text\":\"‘Ngebul’ adalah satu kata yang diungkapkan Fadli Hidayatullah (22 tahun) ketika ditanya kesannya selama hampir sebulan mengikuti coding bootcamp di kumparan Academy. Banyaknya hal baru yang harus dipelajari dalam waktu yang cukup singkat kadang membuatnya pusing hingga bingung harus mulai dari mana.\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[{\"offset\":0,\"length\":9,\"style\":\"ITALIC\"},{\"offset\":129,\"length\":15,\"style\":\"ITALIC\"}],\"entityRanges\":[],\"data\":{}},{\"key\":\"828fq\",\"text\":\"Kepada tim kumparan, Fadli mengaku memahami materi bootcamp lebih lama ketimbang saat belajar di kampus. Ilmu dan teknik coding yang terbilang baru, memaksanya untuk bekerja keras agar dapat benar-benar paham sepenuhnya. “Saya merasa sudah belajar banyak hal di kampus, tapi ternyata setelah sampai di sini semuanya berubah. Ada banyak hal yang saya tidak ketahui dan belum saya pelajari,” tutur peserta yang berasal dari Telkom University Bandung ini.\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[{\"offset\":51,\"length\":8,\"style\":\"ITALIC\"},{\"offset\":121,\"length\":6,\"style\":\"ITALIC\"}],\"entityRanges\":[],\"data\":{}}],\"entityMap\":{}}",
			Status:        domain.StatusPublished,
			Category:      []string{domain.CategoryNews},
			CreatedAt:     time.Now().In(helper.TimeZone),
			UpdatedAt:     time.Now().In(helper.TimeZone),
			CommentsCount: 24,
			AuthorID:      userID3,
			ImageURL:      "https://blue.kumparan.com/image/upload/fl_progressive,fl_lossy,c_fill,q_auto:best,w_640/v1564549468/dwdckxk2ibre9dkp6z0z.jpg",
		},
		domain.Article{
			ID:            articleID5,
			Slug:          "honda-pcx-listrik-jadi-armada-gojek",
			Name:          "Honda PCX Listrik Jadi Armada Gojek",
			Body:          "{\"blocks\":[{\"key\":\"5vdda\",\"text\":\"Catatan Redaksi: Judul berita ini sudah mengalami ubahan dari 'AHM Sodorkan Honda PCX Listrik untuk Armada Gojek' ke 'Honda PCX Listrik Jadi Armada Gojek'. Penggunaan Honda PCX Listrik sebagai unit driver Gojek merupakan tindak lanjut dari kolaborasi Astra International Tbk ke layanan transportasi berbasis aplikasi itu.\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[{\"offset\":0,\"length\":321,\"style\":\"ITALIC\"},{\"offset\":0,\"length\":17,\"style\":\"BOLD\"}],\"entityRanges\":[],\"data\":{}},{\"key\":\"4cgeb\",\"text\":\"Layanan transportasi berbasis aplikasi Gojek resmi berkolaborasi dengan Astra, untuk mengoperasikan beberapa unit Honda PCX Listrik yang disediakan PT Astra Honda Motor (AHM)  buat mengangkut penumpang.\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[{\"offset\":114,\"length\":6,\"key\":0}],\"data\":{}},{\"key\":\"fdfb6\",\"text\":\"Namun, berdasarkan unggahan Gojek pada akun resmi Instagram-nya, ini masih tahap uji coba awal. Nantinya hasil dari tes ini, akan menjadi masukan untuk pengembangan ke tahap berikutnya.⁣\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}],\"entityMap\":{\"0\":{\"type\":\"LINK\",\"mutability\":\"MUTABLE\",\"data\":{\"href\":\"https://kumparan.com/@kumparanoto/gesits-laku-1-200-unit-honda-pcx-listrik-justru-sepi-peminat-1r2444Qrgp7\",\"rel\":\"noopener noreferrer\",\"target\":\"_blank\",\"url\":\"https://kumparan.com/@kumparanoto/gesits-laku-1-200-unit-honda-pcx-listrik-justru-sepi-peminat-1r2444Qrgp7\"}}}}",
			Status:        domain.StatusPublished,
			Category:      []string{domain.CategoryAutomotive},
			CreatedAt:     time.Now().In(helper.TimeZone),
			UpdatedAt:     time.Now().In(helper.TimeZone),
			CommentsCount: 27,
			AuthorID:      userID4,
			ImageURL:      "https://blue.kumparan.com/image/upload/fl_progressive,fl_lossy,c_fill,q_auto:best,w_640/v1541064273/w53wgwtkysuwhfgy9srp.jpg",
		},
		domain.Article{
			ID:            articleID6,
			Slug:          "kewajiban-berjilbab-bagi-muslimah",
			Name:          "Kewajiban Berjilbab bagi Muslimah",
			Body:          "{\"blocks\":[{\"key\":\"5vdda\",\"text\":\"“Hai Nabi, katakanlah kepada isteri-isterimu, anak-anak perempuanmu dan isteri-isteri orang mukmin: “Hendaklah mereka mengulurkan jilbabnyake seluruh tubuh mereka.” Yang demikian itu supaya mereka lebih mudah untuk dikenal, karena itu mereka tidak di ganggu. Dan Allah adalah Maha Pengampun lagi Maha Penyayang.” (QS. Al Ahzâb [33]: 59)\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}],\"entityMap\":{}}",
			Status:        domain.StatusPublished,
			Category:      []string{domain.CategoryEntertainment},
			CreatedAt:     time.Now().In(helper.TimeZone),
			UpdatedAt:     time.Now().In(helper.TimeZone),
			CommentsCount: 27,
			AuthorID:      userID5,
			ImageURL:      "https://blue.kumparan.com/image/upload/fl_progressive,fl_lossy,c_fill,q_auto:best,w_640/v1492756751/Hai_Nabi_katakanlah_kepada_isteri-isterimu_anak-anak_perempuanmu_dan_isteri-isteri_orang_mukmin-_Hendaklah_mereka_mengulurkan_jilbabnyake_seluruh_tubuh_mereka._Yang_demikian_itu_supaya_mereka_lebih_mudah_untu_gql0ne.jpg",
		},
	}

	return &ArticleStorage{
		ArticleMap: articles,
	}
}
