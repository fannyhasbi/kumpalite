module gitlab.com/fannyhasbi/kumpalite/comment

go 1.12

require (
	github.com/DATA-DOG/go-sqlmock v1.3.3
	github.com/golang/protobuf v1.3.2
	github.com/joho/godotenv v1.3.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/lib/pq v1.2.0
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.3.0
	golang.org/x/net v0.0.0-20190607181551-461777fb6f67 // indirect
	golang.org/x/sys v0.0.0-20190609082536-301114b31cce // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/grpc v1.22.1
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
