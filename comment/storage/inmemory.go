package storage

import (
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/fannyhasbi/kumpalite/comment/domain"
)

// CommentStorage wrap  Comment storage object
type CommentStorage struct {
	CommentMap []domain.Comment
}

// NewCommentStorage create new instance of comment storage
func NewCommentStorage() *CommentStorage {

	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")
	userID2, _ := uuid.FromString("42217852-9bb2-44f8-baea-fd5dbdced2b7")

	articleID1, _ := uuid.FromString("85cf8ed1-63df-4381-834a-dcf9af5e96cc")

	commentID1 := uuid.NewV4()
	commentID2 := uuid.NewV4()

	comments := []domain.Comment{
		domain.Comment{
			ID:        commentID1,
			Body:      "Komen ke satu",
			CreatedAt: time.Now(),
			AuthorID:  userID1,
			ArticleID: articleID1,
		},
		domain.Comment{
			ID:        commentID2,
			Body:      "Komen ke dua",
			CreatedAt: time.Now(),
			AuthorID:  userID2,
			ArticleID: articleID1,
		},
	}

	return &CommentStorage{CommentMap: comments}
}
