package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

var (
	// ENV store environment variable (development|production)
	ENV string

	// GRPCDatabasePort is port used to connect database grpc server
	GRPCDatabasePort = ":26257"
)

func init() {
	godotenv.Load()

	// Check the environment variable
	env := os.Getenv("ENV")
	log.Println("ENV", env)

	if env != "production" {
		ENV = "development"
	} else {
		GRPCDatabasePort = "database:26257"
		ENV = "production"
	}

	log.Println("Database PORT", GRPCDatabasePort)
}
