package main

import (
	"log"
	"net"

	_ "github.com/lib/pq"
	pb "gitlab.com/fannyhasbi/kumpalite/comment/proto"
	"gitlab.com/fannyhasbi/kumpalite/comment/server/grpc/handler"
	"gitlab.com/fannyhasbi/kumpalite/comment/services"
	"google.golang.org/grpc"
)

func main() {
	port := ":9003"
	srv := grpc.NewServer()
	var commentServer handler.CommentServer

	commentServer.CommentService = services.NewCommentService()

	pb.RegisterCommentServiceServer(srv, commentServer)

	log.Println("Starting comment RPC server at", port)

	listen, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("Could not listen to %s: %v", port, err)
	}

	log.Fatal("Comment server is listening", srv.Serve(listen))

}
