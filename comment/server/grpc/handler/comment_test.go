package handler

import (
	"context"
	"testing"
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/fannyhasbi/kumpalite/comment/domain"
	pb "gitlab.com/fannyhasbi/kumpalite/comment/proto"
)

type MockCommentService struct {
	mock.Mock
}

func (mock MockCommentService) CreateComment(body, articleID, userID string) (domain.Comment, error) {
	args := mock.Called(body, articleID, userID)
	return args.Get(0).(domain.Comment), args.Error(1)
}

func (mock MockCommentService) FindCommentsByArticleID(articleID string) ([]domain.Comment, error) {
	args := mock.Called(articleID)
	return args.Get(0).([]domain.Comment), args.Error(1)
}

func (mock MockCommentService) GetCommentCountByArticleID(articleID string) (int32, error) {
	args := mock.Called(articleID)
	return args.Get(0).(int32), args.Error(1)
}

func TestCanCreateNewComment(t *testing.T) {
	// given =
	commentID1 := uuid.NewV4()
	articleID1, _ := uuid.FromString("5dfbf327-1fd0-4729-a189-18eae8b4664d")
	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")
	body := "ini Comment"
	now := time.Now()

	comment1 := domain.Comment{
		ID:        commentID1,
		ArticleID: articleID1,
		AuthorID:  userID1,
		Body:      body,
		CreatedAt: now,
	}

	service := MockCommentService{}
	service.On("CreateComment", body, articleID1.String(), userID1.String()).Return(comment1, nil)

	server := NewCommentServer(service)

	commentRequest := &pb.CommentCreateRequest{
		ArticleID: articleID1.String(),
		AuthorID:  userID1.String(),
		Body:      body,
	}

	// when
	result, err := server.CreateComment(context.Background(), commentRequest)

	// then
	assert.Nil(t, err)
	assert.Equal(t, commentID1.String(), result.ID)
}

func TestCanFindCommentsByArticleID(t *testing.T) {
	commentID1 := uuid.NewV4()
	articleID1, _ := uuid.FromString("5dfbf327-1fd0-4729-a189-18eae8b4664d")
	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")
	body := "ini Comment"
	createdAt := time.Now()

	articles := []domain.Comment{
		domain.Comment{
			ID:        commentID1,
			ArticleID: articleID1,
			AuthorID:  userID1,
			Body:      body,
			CreatedAt: createdAt,
		},
	}

	service := MockCommentService{}
	service.On("FindCommentsByArticleID", articleID1.String()).Return(articles, nil)

	server := NewCommentServer(service)

	commentRequest := &pb.CommentRequest{
		ArticleID: articleID1.String(),
	}

	_, err := server.FindCommentsByArticleID(context.Background(), commentRequest)

	assert.Nil(t, err)
}

func TestCanGetCommentCountByArticleID(t *testing.T) {
	articleID1, _ := uuid.FromString("5dfbf327-1fd0-4729-a189-18eae8b4664d")

	var commentsCountArticle1 int32
	commentsCountArticle1 = 2

	service := MockCommentService{}
	service.On("GetCommentCountByArticleID", articleID1.String()).Return(commentsCountArticle1, nil)

	server := NewCommentServer(service)

	commentCountRequest := &pb.CommentRequest{
		ArticleID: articleID1.String(),
	}

	result, err := server.GetCommentCountByArticleID(context.Background(), commentCountRequest)

	assert.Nil(t, err)
	assert.Equal(t, commentsCountArticle1, result.Count)
}
