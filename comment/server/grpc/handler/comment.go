package handler

import (
	"context"

	"gitlab.com/fannyhasbi/kumpalite/comment/domain"
	pb "gitlab.com/fannyhasbi/kumpalite/comment/proto"
)

// CommentServerService is wrap comment server service
type CommentServerService interface {
	CreateComment(body, articleID, userID string) (domain.Comment, error)
	FindCommentsByArticleID(articleID string) ([]domain.Comment, error)
	GetCommentCountByArticleID(articleID string) (int32, error)
}

// CommentServer is wrap comment server
type CommentServer struct {
	CommentService CommentServerService
}

// NewCommentServer is create new instance of comment server
func NewCommentServer(service CommentServerService) pb.CommentServiceServer {
	return &CommentServer{
		CommentService: service,
	}
}

// CreateComment is implement CreateComment method
func (cs CommentServer) CreateComment(ctx context.Context, params *pb.CommentCreateRequest) (*pb.Comment, error) {
	result, err := cs.CommentService.CreateComment(params.Body, params.ArticleID, params.AuthorID)

	if err != nil {
		return &pb.Comment{}, err
	}

	response := ParseCommentToProto(result)

	return response, nil
}

// FindCommentsByArticleID is implement FindCommentsByArticleID method
func (cs CommentServer) FindCommentsByArticleID(ctx context.Context, params *pb.CommentRequest) (*pb.CommentList, error) {
	result, err := cs.CommentService.FindCommentsByArticleID(params.ArticleID)

	if err != nil {
		return &pb.CommentList{}, err
	}

	var response pb.CommentList
	var listComment []*pb.Comment
	for _, comment := range result {
		commentTemp := ParseCommentToProto(comment)

		listComment = append(listComment, commentTemp)
	}
	response.Comments = listComment

	return &response, nil
}

// GetCommentCountByArticleID is implement GetCommentCountByArticleID method
func (cs CommentServer) GetCommentCountByArticleID(ctx context.Context, params *pb.CommentRequest) (*pb.CommentCountResponse, error) {
	result, err := cs.CommentService.GetCommentCountByArticleID(params.ArticleID)

	if err != nil {
		return &pb.CommentCountResponse{}, err
	}

	return &pb.CommentCountResponse{
		Count: result,
	}, nil
}
