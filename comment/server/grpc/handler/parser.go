package handler

import (
	"github.com/golang/protobuf/ptypes"
	"gitlab.com/fannyhasbi/kumpalite/comment/domain"
	pb "gitlab.com/fannyhasbi/kumpalite/comment/proto"
)

// ParseCommentToProto parse comment domain struct into comment proto struct
func ParseCommentToProto(param domain.Comment) *pb.Comment {
	createdAt, _ := ptypes.TimestampProto(param.CreatedAt)

	var response pb.Comment
	response.ID = param.ID.String()
	response.Body = param.Body
	response.CreatedAt = createdAt
	response.ArticleID = param.ArticleID.String()
	response.AuthorID = param.AuthorID.String()

	return &response
}
