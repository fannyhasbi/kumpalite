package inmemory

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/fannyhasbi/kumpalite/comment/domain"
	"gitlab.com/fannyhasbi/kumpalite/comment/storage"
)

func TestQueryComment(t *testing.T) {
	t.Run("Can Find Comments By ArticleID", func(t *testing.T) {
		// GIVEN
		storage := storage.NewCommentStorage()
		query := NewCommentQueryInMemory(storage)

		// WHEN
		comments := query.FindCommentsByArticleID("85cf8ed1-63df-4381-834a-dcf9af5e96cc")

		// THEN
		assert.NoError(t, comments.Error)
		assert.Equal(t, "85cf8ed1-63df-4381-834a-dcf9af5e96cc", comments.Result.([]domain.Comment)[0].ArticleID.String())
	})

	t.Run("Can Get Comments Count By ArticleID", func(t *testing.T) {
		// GIVEN
		storage := storage.NewCommentStorage()
		query := NewCommentQueryInMemory(storage)
		var expectedCount int32
		expectedCount = int32(len(storage.CommentMap))

		// WHEN
		comments := query.GetCommentCountByArticleID("85cf8ed1-63df-4381-834a-dcf9af5e96cc")

		// THEN
		assert.NoError(t, comments.Error)
		assert.Equal(t, expectedCount, comments.Result.(int32))
	})
}
