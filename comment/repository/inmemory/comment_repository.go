package inmemory

import (
	"gitlab.com/fannyhasbi/kumpalite/comment/domain"
	"gitlab.com/fannyhasbi/kumpalite/comment/repository"
	"gitlab.com/fannyhasbi/kumpalite/comment/storage"
)

// CommentRepositoryInMemory is comment repository in memory
type CommentRepositoryInMemory struct {
	Storage *storage.CommentStorage
}

// NewCommentRepositoryInMemory is to create new instance of comment repositoru in memory
func NewCommentRepositoryInMemory(storage *storage.CommentStorage) repository.CommentRepository {
	return &CommentRepositoryInMemory{
		Storage: storage,
	}
}

// Save is implements Save interface
func (cr CommentRepositoryInMemory) Save(comment *domain.Comment) error {
	var result error
	cr.Storage.CommentMap = append(cr.Storage.CommentMap, *comment)

	result = nil
	return result
}
