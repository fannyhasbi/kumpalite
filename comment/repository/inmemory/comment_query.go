package inmemory

import (
	"gitlab.com/fannyhasbi/kumpalite/comment/domain"
	"gitlab.com/fannyhasbi/kumpalite/comment/repository"
	"gitlab.com/fannyhasbi/kumpalite/comment/storage"
)

// CommentQueryInMemory wrap implement of CommentQuery in memory
type CommentQueryInMemory struct {
	Storage *storage.CommentStorage
}

// NewCommentQueryInMemory create new instance of CommentQueryInMemory
func NewCommentQueryInMemory(storage *storage.CommentStorage) repository.CommentQuery {
	return CommentQueryInMemory{
		Storage: storage,
	}
}

// FindCommentsByArticleID implement interface of FindCommentsByArticleID
func (cq CommentQueryInMemory) FindCommentsByArticleID(articleID string) repository.QueryResult {
	var response repository.QueryResult

	var result []domain.Comment
	for _, comment := range cq.Storage.CommentMap {
		if comment.ArticleID.String() == articleID {
			result = append(result, comment)
		}
	}

	response.Result = result
	return response

}

// GetCommentCountByArticleID implement interface of GetCommentCountByArticleID
func (cq CommentQueryInMemory) GetCommentCountByArticleID(articleID string) repository.QueryResult {
	var response repository.QueryResult

	var result int32
	for _, comment := range cq.Storage.CommentMap {
		if comment.ArticleID.String() == articleID {
			result++
		}
	}

	response.Result = result
	return response
}
