package inmemory

import (
	"testing"
	"time"

	"gitlab.com/fannyhasbi/kumpalite/comment/storage"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/fannyhasbi/kumpalite/comment/domain"
)

func TestCanSaveCommentInRepository(t *testing.T) {
	t.Run("Can Save Comment in Repository", func(t *testing.T) {
		commentID1 := uuid.NewV4()
		articleID1, _ := uuid.FromString("5dfbf327-1fd0-4729-a189-18eae8b4664d")
		userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")

		comment := domain.Comment{
			ID:        commentID1,
			Body:      "New Comment",
			ArticleID: articleID1,
			AuthorID:  userID1,
			CreatedAt: time.Now(),
		}

		storage := storage.NewCommentStorage()
		repo := NewCommentRepositoryInMemory(storage)

		err := repo.Save(&comment)

		assert.NoError(t, err)
		assert.Equal(t, 3, len(storage.CommentMap))

	})
}
