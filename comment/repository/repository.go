package repository

import (
	"gitlab.com/fannyhasbi/kumpalite/comment/domain"
)

// CommentRepository is wrap comment repository
type CommentRepository interface {
	Save(comment *domain.Comment) error
}
