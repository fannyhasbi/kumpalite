package cockroach

import (
	"database/sql"

	"gitlab.com/fannyhasbi/kumpalite/comment/domain"
	"gitlab.com/fannyhasbi/kumpalite/comment/repository"
)

type CommentRepositoryCockroachDB struct {
	DB *sql.DB
}

func NewCommentRepositoryCockroachDB(DB *sql.DB) repository.CommentRepository {
	return &CommentRepositoryCockroachDB{
		DB: DB,
	}
}

func (cr CommentRepositoryCockroachDB) Save(comment *domain.Comment) error {
	_, err := cr.DB.Exec(`INSERT INTO comment (
		id, 
		body,
		created_at,
		article_id, 
		author_id) VALUES($1, $2, $3, $4, $5)`,
		comment.ID, comment.Body, comment.CreatedAt, comment.ArticleID, comment.AuthorID)

	if err != nil {
		return err
	}

	return nil
}
