package cockroach

import (
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/fannyhasbi/kumpalite/comment/domain"
)

func TestSaveComment(t *testing.T) {
	db, mock, err := sqlmock.New()

	author_id, _ := uuid.FromString("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")
	article_id, _ := uuid.FromString("343d8f6e-813f-46e9-b05f-216c699b24cb")

	comment := domain.Comment{
		ID:        uuid.NewV4(),
		Body:      "Ini Comment",
		CreatedAt: time.Now(),
		ArticleID: article_id,
		AuthorID:  author_id,
	}

	defer db.Close()
	repo := NewCommentRepositoryCockroachDB(db)

	mock.ExpectExec("INSERT INTO comment").
		WithArgs(comment.ID, comment.Body, comment.CreatedAt, comment.ArticleID, comment.AuthorID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	err = repo.Save(&comment)
	assert.NoError(t, err)
	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}
