package cockroach

import (
	"database/sql"

	"gitlab.com/fannyhasbi/kumpalite/comment/domain"

	"gitlab.com/fannyhasbi/kumpalite/comment/repository"
)

type CommentQueryCockroachDB struct {
	DB *sql.DB
}

func NewCommentQueryCockroach(DB *sql.DB) repository.CommentQuery {
	return &CommentQueryCockroachDB{
		DB: DB,
	}
}

func (cq CommentQueryCockroachDB) FindCommentsByArticleID(articleID string) repository.QueryResult {
	rows, err := cq.DB.Query(`SELECT id, body, created_at, article_id, author_id FROM comment WHERE article_id = $1`, articleID)

	comments := []domain.Comment{}
	result := repository.QueryResult{}

	if err != nil {
		result.Error = err
	} else {
		for rows.Next() {
			temp := domain.Comment{}
			rows.Scan(
				&temp.ID,
				&temp.Body,
				&temp.CreatedAt,
				&temp.ArticleID,
				&temp.AuthorID,
			)

			comments = append(comments, temp)
		}
		result.Result = comments
	}
	return result
}

func (cq CommentQueryCockroachDB) GetCommentCountByArticleID(articleID string) repository.QueryResult {
	var count int32
	row := cq.DB.QueryRow(`SELECT COUNT(*) FROM comment WHERE article_id = $1`, articleID)
	result := repository.QueryResult{}

	err := row.Scan(
		&count,
	)

	if err != nil {
		result.Error = err
	} else {
		result.Result = count
	}

	return result
}
