package cockroach

import (
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

func TestCanFindCommentsByArticleID(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()
	newID, _ := uuid.FromString("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")
	articleID, _ := uuid.FromString("343d8f6e-813f-46e9-b05f-216c699b24cb")
	authorID, _ := uuid.FromString("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")

	query := NewCommentQueryCockroach(db)

	rows := sqlmock.NewRows([]string{"id", "body", "created_at", "article_id", "author_id"}).
		AddRow(newID, "ini comment", time.Now(), articleID, authorID)

	mock.ExpectQuery(`^SELECT (.+) FROM comment WHERE article_id = ?`).
		WithArgs(articleID).
		WillReturnRows(rows)

	result := query.FindCommentsByArticleID("343d8f6e-813f-46e9-b05f-216c699b24cb")
	assert.NoError(t, result.Error)
	assert.NotEmpty(t, result.Result)
}

// func TestCanGetCommentCountByArticleID(t *testing.T) {
// 	db, mock, _ := sqlmock.New()
// 	defer db.Close()
// 	newID, _ := uuid.FromString("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")
// 	articleID, _ := uuid.FromString("343d8f6e-813f-46e9-b05f-216c699b24cb")
// 	authorID, _ := uuid.FromString("3cdd396b-c9cf-4b5f-946a-1f3e4a0aa691")

// 	query := NewCommentQueryCockroach(db)

// 	rows := sqlmock.NewRows([]string{"id", "body", "created_at", "article_id", "author_id"}).
// 		AddRow(newID, "ini comment", time.Now(), articleID, authorID)

// 	mock.ExpectQuery(`SELECT COUNT'('||*||')' FROM comment WHERE article_id = ?`).
// 		WithArgs(articleID).
// 		WillReturnRows(rows)

// 	result := query.GetCommentCountByArticleID("343d8f6e-813f-46e9-b05f-216c699b24cb")
// 	assert.NoError(t, result.Error)
// 	assert.NotEmpty(t, result.Result)
// }
