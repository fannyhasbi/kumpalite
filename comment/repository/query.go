package repository

// QueryResult wrap comment query result
type QueryResult struct {
	Result interface{}
	Error  error
}

// CommentQuery wrap comment query interfaces
type CommentQuery interface {
	FindCommentsByArticleID(articleID string) QueryResult
	GetCommentCountByArticleID(articleID string) QueryResult
}
