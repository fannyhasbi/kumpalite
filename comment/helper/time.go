package helper

import (
	"time"
)

// TimeZone for the local timezone
var TimeZone *time.Location

func init() {
	TimeZone, _ = time.LoadLocation("Asia/Jakarta")
}
