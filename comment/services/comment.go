package services

import (
	"time"

	"gitlab.com/fannyhasbi/kumpalite/comment/repository/cockroach"

	"gitlab.com/fannyhasbi/kumpalite/comment/config"
	"gitlab.com/fannyhasbi/kumpalite/comment/helper"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/fannyhasbi/kumpalite/comment/domain"
	"gitlab.com/fannyhasbi/kumpalite/comment/repository"
	"gitlab.com/fannyhasbi/kumpalite/comment/repository/inmemory"
	"gitlab.com/fannyhasbi/kumpalite/comment/storage"
)

// CommentServices is wrap comment services
type CommentServices struct {
	Query repository.CommentQuery
	Repo  repository.CommentRepository
}

// NewCommentService is create new instance of comment servicces
func NewCommentService() *CommentServices {
	var commentQuery repository.CommentQuery
	var commentRepo repository.CommentRepository

	driver := "cockroach"

	switch driver {
	case "cockroach":
		db := config.InitCockroachDB()
		commentQuery = cockroach.NewCommentQueryCockroach(db)
		commentRepo = cockroach.NewCommentRepositoryCockroachDB(db)
	default:
		db := storage.NewCommentStorage()
		commentQuery = inmemory.NewCommentQueryInMemory(db)
		commentRepo = inmemory.NewCommentRepositoryInMemory(db)
	}

	return &CommentServices{
		Query: commentQuery,
		Repo:  commentRepo,
	}
}

// CreateComment is implement create comment interface
func (cs CommentServices) CreateComment(body, articleID, userID string) (domain.Comment, error) {
	uuidArticle, _ := uuid.FromString(articleID)
	uuidUser, _ := uuid.FromString(userID)
	comment := domain.Comment{
		ID:        uuid.NewV4(),
		Body:      body,
		ArticleID: uuidArticle,
		AuthorID:  uuidUser,
		CreatedAt: time.Now().In(helper.TimeZone),
	}
	err := cs.Repo.Save(&comment)
	if err != nil {
		return domain.Comment{}, err
	}

	return comment, err
}

// FindCommentsByArticleID is implement find comments by article id
func (cs CommentServices) FindCommentsByArticleID(articleID string) ([]domain.Comment, error) {
	result := cs.Query.FindCommentsByArticleID(articleID)

	if result.Error != nil {
		return []domain.Comment{}, result.Error
	}

	return result.Result.([]domain.Comment), nil
}

// GetCommentCountByArticleID is implement get comments count by article id
func (cs CommentServices) GetCommentCountByArticleID(articleID string) (int32, error) {

	// log.Println("KKKKK: ", articleID)
	result := cs.Query.GetCommentCountByArticleID(articleID)

	if result.Error != nil {
		return 0, result.Error
	}

	return result.Result.(int32), nil
}
