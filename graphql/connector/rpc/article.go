package rpc

import (
	"context"
	"log"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/fannyhasbi/kumpalite/graphql/config"
	"gitlab.com/fannyhasbi/kumpalite/graphql/connector"
	pb "gitlab.com/fannyhasbi/kumpalite/graphql/proto"
	"gitlab.com/fannyhasbi/kumpalite/graphql/structures"
	"google.golang.org/grpc"
)

// ArticleRPCConnector wrap article rpc connection
type ArticleRPCConnector struct {
	Connection pb.ArticleServiceClient
}

// NewArticleRPCConnector create new article connection instance
func NewArticleRPCConnector() connector.ArticleConnector {
	port := config.GRPCArticleServerPort
	conn, err := grpc.Dial(port, grpc.WithInsecure())

	if err != nil {
		log.Fatal("could not connect to", port, err)
	}

	return &ArticleRPCConnector{
		Connection: pb.NewArticleServiceClient(conn),
	}
}

// CreateArticle create an article
func (article *ArticleRPCConnector) CreateArticle(id, name, body, slug, status, authorID, imageURL string, categoryIDs []string) (structures.Article, error) {
	createRequest := &pb.CreateRequest{
		Id:          id,
		Name:        name,
		Body:        body,
		Slug:        slug,
		Status:      status,
		AuthorID:    authorID,
		ImageURL:    imageURL,
		CategoryIDs: categoryIDs,
	}

	result, err := article.Connection.CreateArticle(context.Background(), createRequest)
	if err != nil {
		return structures.Article{}, err
	}

	articleParsed := ParseProtoToArticle(result)
	return articleParsed, nil
}

// GetPublishedArticles get published articles
func (article *ArticleRPCConnector) GetPublishedArticles() []structures.Article {
	result, _ := article.Connection.GetPublishedArticles(context.Background(), &empty.Empty{})

	articles := make([]structures.Article, len(result.Articles))
	for i := range result.Articles {
		articles[i] = ParseProtoToArticle(result.Articles[i])
	}

	return articles
}

// GetPublishedArticlesByName get published articles by name (search feature)
func (article *ArticleRPCConnector) GetPublishedArticlesByName(name string) []structures.Article {
	articleRequest := &pb.ArticleRequestByName{
		Name: name,
	}

	result, _ := article.Connection.GetArticlesByName(context.Background(), articleRequest)
	log.Println("WOO", result.Articles)
	if result == nil {
		return []structures.Article{}
	}

	articles := make([]structures.Article, len(result.Articles))
	for i := range result.Articles {
		articles[i] = ParseProtoToArticle(result.Articles[i])
	}

	return articles
}

// GetRelatedArticles get related articles by category
func (article *ArticleRPCConnector) GetRelatedArticles(category []string) []structures.Article {
	relatedRequest := &pb.RelatedArticleRequst{
		Category: category,
	}

	result, _ := article.Connection.GetRelatedArticles(context.Background(), relatedRequest)
	if result == nil {
		return []structures.Article{}
	}

	articles := make([]structures.Article, len(result.Articles))
	for i := range result.Articles {
		articles[i] = ParseProtoToArticle(result.Articles[i])
	}

	return articles
}

// GetPublishedArticleBySlug get published articles by slug
func (article *ArticleRPCConnector) GetPublishedArticleBySlug(slug string) structures.Article {
	articleRequestBySlug := &pb.ArticleRequestBySlug{
		Slug: slug,
	}

	result, _ := article.Connection.FindArticleBySlug(context.Background(), articleRequestBySlug)
	if result == nil {
		return structures.Article{}
	}

	var articleResponse structures.Article
	articleResponse = ParseProtoToArticle(result)

	return articleResponse
}

// GetPublishedArticlesByAuthorID get published articles by author id
func (article *ArticleRPCConnector) GetPublishedArticlesByAuthorID(id string) []structures.Article {
	articleRequestAuthorID := &pb.ArticleRequestByAuthorID{
		AuthorID: id,
	}

	result, _ := article.Connection.GetPublishedArticlesByAuthorID(context.Background(), articleRequestAuthorID)
	if result == nil {
		return []structures.Article{}
	}

	articles := make([]structures.Article, len(result.Articles))
	for i := range result.Articles {
		articles[i] = ParseProtoToArticle(result.Articles[i])
	}

	return articles
}

// GetArticlesByCategoryAndAuthorID is get articles that belong to specific author by specific category
func (article *ArticleRPCConnector) GetArticlesByCategoryAndAuthorID(category, authorID string) []structures.Article {
	articleRequestByCategoryAuthorID := &pb.ArticleRequestByCategoryAndAuthorID{
		Category: category,
		AuthorID: authorID,
	}

	result, _ := article.Connection.GetArticlesByCategoryAndAuthorID(context.Background(), articleRequestByCategoryAuthorID)
	if result == nil {
		return []structures.Article{}
	}

	articles := make([]structures.Article, len(result.Articles))
	for i := range result.Articles {
		articles[i] = ParseProtoToArticle(result.Articles[i])
	}

	return articles

}

// GetArticlesByCategory is get articles by category
func (article *ArticleRPCConnector) GetArticlesByCategory(category string) []structures.Article {
	articleRequestByCategory := &pb.ArticleRequestByCategorySlug{
		Slug: category,
	}

	result, _ := article.Connection.GetPublishedArticlesByCategorySlug(context.Background(), articleRequestByCategory)
	if result == nil {
		return []structures.Article{}
	}

	articles := make([]structures.Article, len(result.Articles))
	for i := range result.Articles {
		articles[i] = ParseProtoToArticle(result.Articles[i])
	}

	return articles
}

// GetArticlesByAuthorIDAndName search feature for author public profile
func (article *ArticleRPCConnector) GetArticlesByAuthorIDAndName(authorID, name string) []structures.Article {
	articleRequest := &pb.ArticleRequestByAuthorIDAndName{
		AuthorID: authorID,
		Name:     name,
	}

	result, _ := article.Connection.GetArticlesByAuthorIDAndName(context.Background(), articleRequest)
	if result == nil {
		return []structures.Article{}
	}

	articles := make([]structures.Article, len(result.Articles))
	for i := range result.Articles {
		articles[i] = ParseProtoToArticle(result.Articles[i])
	}

	return articles
}

// GetArticlesByAuthorID get articles by author id
func (article *ArticleRPCConnector) GetArticlesByAuthorID(authorID string) []structures.Article {
	articleRequestByAuthorID := &pb.ArticleRequestByAuthorID{
		AuthorID: authorID,
	}

	result, _ := article.Connection.GetArticlesByAuthorID(context.Background(), articleRequestByAuthorID)
	if result == nil {
		return []structures.Article{}
	}

	articles := make([]structures.Article, len(result.Articles))
	for i := range result.Articles {
		articles[i] = ParseProtoToArticle(result.Articles[i])
	}

	return articles
}
