package rpc

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

func TestCanGetPublishedArticle(t *testing.T) {
	// given
	client := NewArticleRPCConnector()

	// when
	result := client.GetPublishedArticles()

	// then
	assert.NotEmpty(t, result)
}

func TestGetPublishedArticleByName(t *testing.T) {
	client := NewArticleRPCConnector()

	t.Run("Correct", func(t *testing.T) {
		// given
		name := "Artikel"

		// when
		result := client.GetPublishedArticlesByName(name)

		// then
		assert.NotEmpty(t, result)
		assert.Contains(t, result[0].Name, name)
	})

	t.Run("No result", func(t *testing.T) {
		// given
		name := "hahahahahahahahahahaha"

		// when
		result := client.GetPublishedArticlesByName(name)

		// then
		assert.Empty(t, result)
	})

	t.Run("Symbols", func(t *testing.T) {
		// given
		name := ";'/.,-=)"

		// when
		result := client.GetPublishedArticlesByName(name)

		// then
		assert.Empty(t, result)
	})
}

func TestRelatedArticles(t *testing.T) {
	client := NewArticleRPCConnector()

	t.Run("Correct", func(t *testing.T) {
		// given
		searchedCategory := []string{"AUTOMOTIVE"}

		// when
		result := client.GetRelatedArticles(searchedCategory)

		// then
		assert.Equal(t, result[0].Category, searchedCategory)
	})
}

func TestGetPublishedArticleBySlug(t *testing.T) {
	t.Run("Correct", func(t *testing.T) {
		// given
		client := NewArticleRPCConnector()
		slug := "artikel-lama"

		// when
		result := client.GetPublishedArticleBySlug(slug)

		// then
		assert.NotEmpty(t, result)
	})
}

func TestGetArticlesByCategoryAndAuthorID(t *testing.T) {
	t.Run("Correct", func(t *testing.T) {
		// given
		client := NewArticleRPCConnector()
		automotive := "AUTOMOTIVE"
		authorID := "815b95be-4741-4167-8224-975de6645de9"

		// when
		result := client.GetArticlesByCategoryAndAuthorID(automotive, authorID)

		// then
		assert.Equal(t, 1, len(result))
	})
}

func TestGetArticlesByCategory(t *testing.T) {
	t.Run("Correct", func(t *testing.T) {
		// given
		client := NewArticleRPCConnector()
		newsCategory := "NEWS"

		// when
		result := client.GetArticlesByCategory(newsCategory)

		// then
		assert.Equal(t, 2, len(result))
	})
}

func TestGetArticlesByAuthorID(t *testing.T) {
	t.Run("Correct", func(t *testing.T) {
		// given
		client := NewArticleRPCConnector()
		userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")

		// when
		result := client.GetArticlesByAuthorID(userID1.String())

		// then
		assert.Equal(t, 2, len(result))
	})
}
