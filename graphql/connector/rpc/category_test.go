package rpc

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCanCreateCategory(t *testing.T) {
	// given
	name := "FASHION"
	authorID := "815b95be-4741-4167-8224-975de6645de9"

	client := NewCategoryRPCConnector()

	// when
	result, err := client.CreateCategory(name, authorID)

	// then
	assert.NotEmpty(t, result)
	assert.NoError(t, err)
}

func TestCanFindCategoryByID(t *testing.T) {
	// given
	categoryID := "85cf8ed1-63df-4381-834a-dcf9af5e96cc"

	client := NewCategoryRPCConnector()

	// when
	result, err := client.FindCategoryByID(categoryID)

	// then
	assert.NotEmpty(t, result)
	assert.NoError(t, err)
}

func TestCanGetCategoriesByAuthorID(t *testing.T) {
	// given
	authorID := "42217852-9bb2-44f8-baea-fd5dbdced2b7"
	client := NewCategoryRPCConnector()

	// when
	result, err := client.GetCategoriesByAuthorID(authorID)
	t.Log("INI", result)
	// then
	assert.NotEmpty(t, result)
	assert.NoError(t, err)
}
