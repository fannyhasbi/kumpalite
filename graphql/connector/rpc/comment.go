package rpc

import (
	"context"
	"log"

	"gitlab.com/fannyhasbi/kumpalite/graphql/config"
	"gitlab.com/fannyhasbi/kumpalite/graphql/connector"
	pb "gitlab.com/fannyhasbi/kumpalite/graphql/proto"
	"gitlab.com/fannyhasbi/kumpalite/graphql/structures"
	"google.golang.org/grpc"
)

// CommentRPCConnector wrap comment rpc connection
type CommentRPCConnector struct {
	Connection pb.CommentServiceClient
}

// NewCommentRPCConnector create new comment connection RPC instance
func NewCommentRPCConnector() connector.CommentConnector {
	port := config.GRPCCommentServerPort
	conn, err := grpc.Dial(port, grpc.WithInsecure())
	if err != nil {
		log.Fatal("could not connect to", port, err)
	}

	return &CommentRPCConnector{
		Connection: pb.NewCommentServiceClient(conn),
	}
}

// GetCommentsByArticleID get comments by article id
func (comment *CommentRPCConnector) GetCommentsByArticleID(articleID string) ([]structures.Comment, error) {
	commentRequest := &pb.CommentRequest{
		ArticleID: articleID,
	}

	result, err := comment.Connection.FindCommentsByArticleID(context.Background(), commentRequest)
	if err != nil {
		return []structures.Comment{}, err
	}

	comments := make([]structures.Comment, len(result.Comments))
	for i := range result.Comments {
		comments[i] = ParseProtoToComment(result.Comments[i])
	}

	return comments, nil
}

// CreateComment create a new comment
func (comment *CommentRPCConnector) CreateComment(body, articleID, authorID string) (structures.Comment, error) {
	commentCreateRequest := &pb.CommentCreateRequest{
		Body:      body,
		ArticleID: articleID,
		AuthorID:  authorID,
	}

	result, err := comment.Connection.CreateComment(context.Background(), commentCreateRequest)
	if err != nil {
		return structures.Comment{}, err
	}

	commentParsed := ParseProtoToComment(result)

	return commentParsed, nil

}
