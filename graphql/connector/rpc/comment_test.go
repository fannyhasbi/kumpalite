package rpc

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCanGetComments(t *testing.T) {
	// given
	articleID := "85cf8ed1-63df-4381-834a-dcf9af5e96cc"

	client := NewCommentRPCConnector()

	// when
	result, err := client.GetCommentsByArticleID(articleID)

	// then
	assert.NotEmpty(t, result)
	assert.NoError(t, err)
}

func TestCanCreateComments(t *testing.T) {
	// given
	articleID := "85cf8ed1-63df-4381-834a-dcf9af5e96cc"
	authorID := "42217852-9bb2-44f8-baea-fd5dbdced2b7"
	client := NewCommentRPCConnector()

	// when
	result, err := client.CreateComment("Ini Komen", articleID, authorID)

	// then
	assert.NotEmpty(t, result)
	assert.NoError(t, err)
}
