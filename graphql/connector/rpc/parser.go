package rpc

import (
	"github.com/golang/protobuf/ptypes"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/fannyhasbi/kumpalite/graphql/helper"
	pb "gitlab.com/fannyhasbi/kumpalite/graphql/proto"
	"gitlab.com/fannyhasbi/kumpalite/graphql/structures"
)

// ParseProtoToArticle parse article proto struct into article structures struct
func ParseProtoToArticle(param *pb.Article) structures.Article {
	id, _ := uuid.FromString(param.ID)
	userID, _ := uuid.FromString(param.AuthorID)

	createdAt, _ := ptypes.Timestamp(param.CreatedAt)
	updatedAt, _ := ptypes.Timestamp(param.UpdatedAt)

	return structures.Article{
		ID:            id,
		Slug:          param.Slug,
		Name:          param.Name,
		Body:          param.Body,
		Status:        param.Status,
		Category:      param.Category,
		CreatedAt:     createdAt.In(helper.TimeZone),
		UpdatedAt:     updatedAt.In(helper.TimeZone),
		AuthorID:      userID,
		CommentsCount: param.CommentsCount,
		ImageURL:      param.ImageURL,
	}
}

// ParseProtoToAuthor parse author proto struct into author structures struct
func ParseProtoToAuthor(param *pb.AuthorResponse) structures.Author {
	id, _ := uuid.FromString(param.ID)

	return structures.Author{
		ID:       id,
		Name:     param.Name,
		Email:    param.Email,
		Username: param.Username,
	}
}

// ParseProtoToComment parse comment proto struct into comment structures struct
func ParseProtoToComment(param *pb.Comment) structures.Comment {
	id, _ := uuid.FromString(param.ID)
	articleID, _ := uuid.FromString(param.ArticleID)
	authorID, _ := uuid.FromString(param.AuthorID)

	createdAt, _ := ptypes.Timestamp(param.CreatedAt)

	return structures.Comment{
		ID:        id,
		Body:      param.Body,
		CreatedAt: createdAt.In(helper.TimeZone),
		ArticleID: articleID,
		AuthorID:  authorID,
	}
}

// ParseProtoToCategory parse author proto struct into author structures struct
func ParseProtoToCategory(param *pb.Category) structures.Category {
	id, _ := uuid.FromString(param.ID)

	return structures.Category{
		ID:   id,
		Name: param.Name,
	}
}
