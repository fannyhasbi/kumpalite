package rpc

import (
	"context"
	"log"

	"gitlab.com/fannyhasbi/kumpalite/graphql/config"
	"gitlab.com/fannyhasbi/kumpalite/graphql/connector"
	pb "gitlab.com/fannyhasbi/kumpalite/graphql/proto"
	"gitlab.com/fannyhasbi/kumpalite/graphql/structures"
	"google.golang.org/grpc"
)

type CategoryRPCConnector struct {
	Connection pb.CategoryServiceClient
}

func NewCategoryRPCConnector() connector.CategoryConnector {
	port := config.GRPCCategoryServerPort
	conn, err := grpc.Dial(port, grpc.WithInsecure())
	if err != nil {
		log.Fatal("could not connect to", port, err)
	}

	return &CategoryRPCConnector{
		Connection: pb.NewCategoryServiceClient(conn),
	}
}

func (category *CategoryRPCConnector) CreateCategory(name, slug, authorID string) (structures.Category, error) {
	createCategoryRequest := &pb.CreateCategoryRequest{
		Name:     name,
		AuthorID: authorID,
		Slug:     slug,
	}

	result, err := category.Connection.CreateCategory(context.Background(), createCategoryRequest)
	if err != nil {
		return structures.Category{}, err
	}

	categoryParsed := ParseProtoToCategory(result)

	return categoryParsed, nil
}

func (category *CategoryRPCConnector) FindCategoryByID(id string) (structures.Category, error) {
	queryCategoryByID := &pb.CategoryQueryByID{
		ID: id,
	}

	result, err := category.Connection.FindCategoryByID(context.Background(), queryCategoryByID)
	if err != nil {
		return structures.Category{}, err
	}

	categoryParsed := ParseProtoToCategory(result)

	return categoryParsed, nil
}

// GetCategoriesByAuthorID get categories by author ID
func (category *CategoryRPCConnector) GetCategoriesByAuthorID(authorID string) ([]structures.Category, error) {
	queryGetCategoriesByAuthorID := &pb.CategoryQueryByAuthorID{
		AuthorID: authorID,
	}
	result, err := category.Connection.GetCategoriesByAuthorID(context.Background(), queryGetCategoriesByAuthorID)
	if err != nil {
		return []structures.Category{}, err
	}

	articles := make([]structures.Category, len(result.Categories))
	for i := range result.Categories {
		articles[i] = ParseProtoToCategory(result.Categories[i])
	}

	return articles, nil
}

func (category *CategoryRPCConnector) GetCategoriesByAuthorIDAndArticleID(authorID, articleID string) ([]structures.Category, error) {
	queryGetCategoriesByAuthorIDAndArticleID := &pb.CategoryQueryByAuthorIDAndArticleID{
		AuthorID:  authorID,
		ArticleID: articleID,
	}

	result, err := category.Connection.GetCategoriesByAuthorIDAndArticleID(context.Background(), queryGetCategoriesByAuthorIDAndArticleID)
	if err != nil {
		return []structures.Category{}, err
	}

	categories := make([]structures.Category, len(result.Categories))
	for i := range result.Categories {
		categories[i] = ParseProtoToCategory(result.Categories[i])
	}

	return categories, nil
}
