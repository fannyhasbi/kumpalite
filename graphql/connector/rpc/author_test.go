package rpc

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCanFindAuthorByID(t *testing.T) {
	// given
	authorID := "815b95be-4741-4167-8224-975de6645de9"

	client := NewAuthorRPCConnector()

	// when
	result, err := client.FindAuthorByID(authorID)

	t.Log(result)
	t.Log(err)

	// then
	assert.NotEmpty(t, result)
	assert.NoError(t, err)
}

func TestCanFindAuthorByUsername(t *testing.T) {
	// given
	username := "budi13"

	client := NewAuthorRPCConnector()

	// when
	result, err := client.FindAuthorByUsername(username)

	// then
	assert.NotEmpty(t, result)
	assert.NoError(t, err)
}

func TestCreateAuthor(t *testing.T) {
	// given
	username := "fajarganteng"
	name := "Fajar"
	email := "fajarganteng@example.com"
	password := "fajar12345"

	client := NewAuthorRPCConnector()

	// when
	result, err := client.CreateAuthor(username, name, email, password)

	// then
	assert.NotEmpty(t, result)
	assert.NoError(t, err)
}
