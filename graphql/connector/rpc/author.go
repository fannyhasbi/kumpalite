package rpc

import (
	"context"
	"log"

	"gitlab.com/fannyhasbi/kumpalite/graphql/config"
	"gitlab.com/fannyhasbi/kumpalite/graphql/connector"
	pb "gitlab.com/fannyhasbi/kumpalite/graphql/proto"
	"gitlab.com/fannyhasbi/kumpalite/graphql/structures"
	"google.golang.org/grpc"
)

// AuthorRPCConnector wrap author rpc connection
type AuthorRPCConnector struct {
	Connection pb.AuthorServiceClient
}

// NewAuthorRPCConnector create new author connection RPC instance
func NewAuthorRPCConnector() connector.AuthorConnector {
	port := config.GRPCAuthorServerPort
	conn, err := grpc.Dial(port, grpc.WithInsecure())
	if err != nil {
		log.Fatal("could not connect to", port, err)
	}

	return &AuthorRPCConnector{
		Connection: pb.NewAuthorServiceClient(conn),
	}
}

// FindAuthorByID find an a
func (author *AuthorRPCConnector) FindAuthorByID(id string) (structures.Author, error) {
	authorRequestID := &pb.AuthorRequestByID{
		ID: id,
	}

	result, err := author.Connection.FindAuthorByID(context.Background(), authorRequestID)
	if err != nil {
		return structures.Author{}, err
	}

	authorParsed := ParseProtoToAuthor(result)

	return authorParsed, nil
}

// FindAuthorByUsername find author by username
func (author *AuthorRPCConnector) FindAuthorByUsername(username string) (structures.Author, error) {
	authorRequestUsername := &pb.AuthorRequestByUsername{
		Username: username,
	}

	result, err := author.Connection.FindAuthorByUsername(context.Background(), authorRequestUsername)
	if err != nil {
		return structures.Author{}, err
	}

	authorParsed := ParseProtoToAuthor(result)

	return authorParsed, nil
}

// CreateAuthor connection to register an author
func (author *AuthorRPCConnector) CreateAuthor(username, name, email, password string) (structures.Author, error) {
	registerRequest := &pb.AuthorCreateRequest{
		Username: username,
		Name:     name,
		Email:    email,
		Password: password,
	}

	result, err := author.Connection.CreateAuthor(context.Background(), registerRequest)
	if err != nil {
		return structures.Author{}, err
	}

	authorParsed := ParseProtoToAuthor(result)
	return authorParsed, nil
}
