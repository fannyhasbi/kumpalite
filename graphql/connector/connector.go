package connector

import (
	"gitlab.com/fannyhasbi/kumpalite/graphql/structures"
)

// ArticleConnector wrap article for connecting with GRPC
type ArticleConnector interface {
	GetPublishedArticles() []structures.Article
	GetPublishedArticlesByName(name string) []structures.Article
	GetRelatedArticles(category []string) []structures.Article
	GetPublishedArticleBySlug(slug string) structures.Article
	GetPublishedArticlesByAuthorID(id string) []structures.Article
	GetArticlesByCategoryAndAuthorID(category, authorID string) []structures.Article
	GetArticlesByCategory(category string) []structures.Article
	GetArticlesByAuthorID(authorID string) []structures.Article
	CreateArticle(id, name, body, slug, status, authorID, imageURL string, categoryIDs []string) (structures.Article, error)
	GetArticlesByAuthorIDAndName(authorID, name string) []structures.Article
}

// AuthorConnector wrap author for connecting with GRPC
type AuthorConnector interface {
	FindAuthorByUsername(username string) (structures.Author, error)
	FindAuthorByID(id string) (structures.Author, error)
	CreateAuthor(username, name, email, password string) (structures.Author, error)
}

// CommentConnector wrap comment for connecting with GRPC
type CommentConnector interface {
	GetCommentsByArticleID(articleID string) ([]structures.Comment, error)
	CreateComment(body, articleID, authorID string) (structures.Comment, error)
}

// CategoryConnector wrap category for connecting with GRPC
type CategoryConnector interface {
	CreateCategory(name, slug, authorID string) (structures.Category, error)
	FindCategoryByID(id string) (structures.Category, error)
	GetCategoriesByAuthorID(authorID string) ([]structures.Category, error)
	GetCategoriesByAuthorIDAndArticleID(authorID, articleID string) ([]structures.Category, error)
}
