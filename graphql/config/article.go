package config

import (
	"log"

	pb "gitlab.com/fannyhasbi/kumpalite/graphql/proto"
	"google.golang.org/grpc"
)

// InitArticleGRPC init the author GRPC client
func InitArticleGRPC() pb.ArticleServiceClient {
	host := "localhost:9002"

	conn, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect to %s: %v", host, err)
	}

	return pb.NewArticleServiceClient(conn)
}
