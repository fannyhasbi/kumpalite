package config

import (
	"log"

	pb "gitlab.com/fannyhasbi/kumpalite/graphql/proto"
	"google.golang.org/grpc"
)

// InitCommentGRPC init the comment GRPC client
func InitCommentGRPC() pb.CommentServiceClient {
	host := GRPCCommentServerPort

	conn, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect to %s: %v", host, err)
	}

	return pb.NewCommentServiceClient(conn)
}
