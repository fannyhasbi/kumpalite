package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

var (
	// ENV store environment variable (development|production)
	ENV string
	// GRPCArticleServerPort is port used to connect article grpc server
	GRPCArticleServerPort = ":9002"
	// GRPCAuthorServerPort is port used to connect author grpc server
	GRPCAuthorServerPort = ":9001"
	// GRPCCommentServerPort is port used to connect comment grpc server
	GRPCCommentServerPort = ":9003"
	// GRPCCategoryServerPort is port used to connect  author grpc server
	GRPCCategoryServerPort = ":9004"
)

func init() {
	godotenv.Load()

	// Check the environment variable
	env := os.Getenv("ENV")
	log.Println("ENV", env)

	if env != "production" {
		ENV = "development"
	} else {
		GRPCArticleServerPort = "article:9002"
		GRPCAuthorServerPort = "author:9001"
		GRPCCommentServerPort = "comment:9003"
		GRPCCategoryServerPort = "category:9004"
		ENV = "production"
	}

	log.Println("Article PORT", GRPCArticleServerPort)
	log.Println("Author PORT", GRPCAuthorServerPort)
	log.Println("Comment PORT", GRPCCommentServerPort)
	log.Println("Category PORT", GRPCCategoryServerPort)
}
