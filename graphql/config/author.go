package config

import (
	"log"

	pb "gitlab.com/fannyhasbi/kumpalite/graphql/proto"
	"google.golang.org/grpc"
)

// InitAuthorGRPC init the author GRPC client
func InitAuthorGRPC() pb.AuthorServiceClient {
	host := GRPCAuthorServerPort

	conn, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect to %s: %v", host, err)
	}

	return pb.NewAuthorServiceClient(conn)
}
