package helper

import (
	"time"

	"github.com/dgrijalva/jwt-go"
)

// StaticPath define the static folder for uploaded files
const StaticPath = "/static"

// TokenExpireTime set the token expire time
const TokenExpireTime = 72

// JWTSecretKey for JWT generator
var JWTSecretKey = []byte("fadlikerenbangeteuy")

// JWTPayload store JWT payload to generate
type JWTPayload struct {
	Username string
	Name     string
	AuthorID string
}

// GenerateToken generate a JWT token
func GenerateToken(payload JWTPayload) (string, error) {
	// token generator
	token := jwt.New(jwt.SigningMethodHS256)

	// set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["username"] = payload.Username
	claims["name"] = payload.Name
	claims["author_id"] = payload.AuthorID

	claims["exp"] = time.Now().Add(time.Hour * TokenExpireTime).Unix()

	t, err := token.SignedString(JWTSecretKey)
	if err != nil {
		return "", err
	}

	return t, nil
}
