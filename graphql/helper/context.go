package helper

import (
	"context"
	"errors"
	"log"

	"gitlab.com/fannyhasbi/kumpalite/graphql/config"
	pb "gitlab.com/fannyhasbi/kumpalite/graphql/proto"
)

// ParseContext parse context value
func ParseContext(c interface{}) (map[string]string, error) {
	log.Println("SAMPAI PARSE")
	r := make(map[string]string)
	r = c.(map[string]string)

	authorClient := config.InitAuthorGRPC()

	authorID, exist := r["author_id"]
	if !exist {
		log.Println("[ParseContext][ERR]", r)
		return r, errors.New("Authorization failed")
	}

	authorRequest := &pb.AuthorRequestByID{
		ID: authorID,
	}

	author, err := authorClient.FindAuthorByID(context.Background(), authorRequest)
	if err != nil {
		log.Println("[ParseContext][ERR]", err)
		return r, errors.New("Authorization failed")
	}

	log.Println("[ParseContext][Accessing User]", author)

	return r, nil
}
