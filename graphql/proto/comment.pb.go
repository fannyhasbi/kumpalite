// Code generated by protoc-gen-go. DO NOT EDIT.
// source: comment.proto

package proto

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type Comment struct {
	ID                   string               `protobuf:"bytes,1,opt,name=ID,proto3" json:"ID,omitempty"`
	Body                 string               `protobuf:"bytes,2,opt,name=Body,proto3" json:"Body,omitempty"`
	CreatedAt            *timestamp.Timestamp `protobuf:"bytes,3,opt,name=CreatedAt,proto3" json:"CreatedAt,omitempty"`
	ArticleID            string               `protobuf:"bytes,4,opt,name=ArticleID,proto3" json:"ArticleID,omitempty"`
	AuthorID             string               `protobuf:"bytes,5,opt,name=AuthorID,proto3" json:"AuthorID,omitempty"`
	XXX_NoUnkeyedLiteral struct{}             `json:"-"`
	XXX_unrecognized     []byte               `json:"-"`
	XXX_sizecache        int32                `json:"-"`
}

func (m *Comment) Reset()         { *m = Comment{} }
func (m *Comment) String() string { return proto.CompactTextString(m) }
func (*Comment) ProtoMessage()    {}
func (*Comment) Descriptor() ([]byte, []int) {
	return fileDescriptor_749aee09ea917828, []int{0}
}

func (m *Comment) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Comment.Unmarshal(m, b)
}
func (m *Comment) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Comment.Marshal(b, m, deterministic)
}
func (m *Comment) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Comment.Merge(m, src)
}
func (m *Comment) XXX_Size() int {
	return xxx_messageInfo_Comment.Size(m)
}
func (m *Comment) XXX_DiscardUnknown() {
	xxx_messageInfo_Comment.DiscardUnknown(m)
}

var xxx_messageInfo_Comment proto.InternalMessageInfo

func (m *Comment) GetID() string {
	if m != nil {
		return m.ID
	}
	return ""
}

func (m *Comment) GetBody() string {
	if m != nil {
		return m.Body
	}
	return ""
}

func (m *Comment) GetCreatedAt() *timestamp.Timestamp {
	if m != nil {
		return m.CreatedAt
	}
	return nil
}

func (m *Comment) GetArticleID() string {
	if m != nil {
		return m.ArticleID
	}
	return ""
}

func (m *Comment) GetAuthorID() string {
	if m != nil {
		return m.AuthorID
	}
	return ""
}

type CommentList struct {
	Comments             []*Comment `protobuf:"bytes,1,rep,name=Comments,proto3" json:"Comments,omitempty"`
	XXX_NoUnkeyedLiteral struct{}   `json:"-"`
	XXX_unrecognized     []byte     `json:"-"`
	XXX_sizecache        int32      `json:"-"`
}

func (m *CommentList) Reset()         { *m = CommentList{} }
func (m *CommentList) String() string { return proto.CompactTextString(m) }
func (*CommentList) ProtoMessage()    {}
func (*CommentList) Descriptor() ([]byte, []int) {
	return fileDescriptor_749aee09ea917828, []int{1}
}

func (m *CommentList) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CommentList.Unmarshal(m, b)
}
func (m *CommentList) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CommentList.Marshal(b, m, deterministic)
}
func (m *CommentList) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CommentList.Merge(m, src)
}
func (m *CommentList) XXX_Size() int {
	return xxx_messageInfo_CommentList.Size(m)
}
func (m *CommentList) XXX_DiscardUnknown() {
	xxx_messageInfo_CommentList.DiscardUnknown(m)
}

var xxx_messageInfo_CommentList proto.InternalMessageInfo

func (m *CommentList) GetComments() []*Comment {
	if m != nil {
		return m.Comments
	}
	return nil
}

type CommentRequest struct {
	ArticleID            string   `protobuf:"bytes,4,opt,name=ArticleID,proto3" json:"ArticleID,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CommentRequest) Reset()         { *m = CommentRequest{} }
func (m *CommentRequest) String() string { return proto.CompactTextString(m) }
func (*CommentRequest) ProtoMessage()    {}
func (*CommentRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_749aee09ea917828, []int{2}
}

func (m *CommentRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CommentRequest.Unmarshal(m, b)
}
func (m *CommentRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CommentRequest.Marshal(b, m, deterministic)
}
func (m *CommentRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CommentRequest.Merge(m, src)
}
func (m *CommentRequest) XXX_Size() int {
	return xxx_messageInfo_CommentRequest.Size(m)
}
func (m *CommentRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_CommentRequest.DiscardUnknown(m)
}

var xxx_messageInfo_CommentRequest proto.InternalMessageInfo

func (m *CommentRequest) GetArticleID() string {
	if m != nil {
		return m.ArticleID
	}
	return ""
}

type CommentCreateRequest struct {
	Body                 string   `protobuf:"bytes,1,opt,name=Body,proto3" json:"Body,omitempty"`
	ArticleID            string   `protobuf:"bytes,2,opt,name=ArticleID,proto3" json:"ArticleID,omitempty"`
	AuthorID             string   `protobuf:"bytes,3,opt,name=AuthorID,proto3" json:"AuthorID,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CommentCreateRequest) Reset()         { *m = CommentCreateRequest{} }
func (m *CommentCreateRequest) String() string { return proto.CompactTextString(m) }
func (*CommentCreateRequest) ProtoMessage()    {}
func (*CommentCreateRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_749aee09ea917828, []int{3}
}

func (m *CommentCreateRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CommentCreateRequest.Unmarshal(m, b)
}
func (m *CommentCreateRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CommentCreateRequest.Marshal(b, m, deterministic)
}
func (m *CommentCreateRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CommentCreateRequest.Merge(m, src)
}
func (m *CommentCreateRequest) XXX_Size() int {
	return xxx_messageInfo_CommentCreateRequest.Size(m)
}
func (m *CommentCreateRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_CommentCreateRequest.DiscardUnknown(m)
}

var xxx_messageInfo_CommentCreateRequest proto.InternalMessageInfo

func (m *CommentCreateRequest) GetBody() string {
	if m != nil {
		return m.Body
	}
	return ""
}

func (m *CommentCreateRequest) GetArticleID() string {
	if m != nil {
		return m.ArticleID
	}
	return ""
}

func (m *CommentCreateRequest) GetAuthorID() string {
	if m != nil {
		return m.AuthorID
	}
	return ""
}

type CommentCountResponse struct {
	Count                int32    `protobuf:"varint,1,opt,name=Count,proto3" json:"Count,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CommentCountResponse) Reset()         { *m = CommentCountResponse{} }
func (m *CommentCountResponse) String() string { return proto.CompactTextString(m) }
func (*CommentCountResponse) ProtoMessage()    {}
func (*CommentCountResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_749aee09ea917828, []int{4}
}

func (m *CommentCountResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CommentCountResponse.Unmarshal(m, b)
}
func (m *CommentCountResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CommentCountResponse.Marshal(b, m, deterministic)
}
func (m *CommentCountResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CommentCountResponse.Merge(m, src)
}
func (m *CommentCountResponse) XXX_Size() int {
	return xxx_messageInfo_CommentCountResponse.Size(m)
}
func (m *CommentCountResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_CommentCountResponse.DiscardUnknown(m)
}

var xxx_messageInfo_CommentCountResponse proto.InternalMessageInfo

func (m *CommentCountResponse) GetCount() int32 {
	if m != nil {
		return m.Count
	}
	return 0
}

func init() {
	proto.RegisterType((*Comment)(nil), "proto.Comment")
	proto.RegisterType((*CommentList)(nil), "proto.CommentList")
	proto.RegisterType((*CommentRequest)(nil), "proto.CommentRequest")
	proto.RegisterType((*CommentCreateRequest)(nil), "proto.CommentCreateRequest")
	proto.RegisterType((*CommentCountResponse)(nil), "proto.CommentCountResponse")
}

func init() { proto.RegisterFile("comment.proto", fileDescriptor_749aee09ea917828) }

var fileDescriptor_749aee09ea917828 = []byte{
	// 339 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x8c, 0x51, 0xcd, 0x4e, 0xc2, 0x40,
	0x10, 0xce, 0x16, 0xaa, 0x30, 0x04, 0x0e, 0x13, 0x8c, 0x4d, 0x35, 0x91, 0xf4, 0x44, 0x8c, 0x29,
	0x09, 0x5e, 0x34, 0xf1, 0x02, 0x34, 0x9a, 0x26, 0x1e, 0x4c, 0xf5, 0x05, 0x80, 0x8e, 0xd8, 0x84,
	0xb2, 0xd8, 0xdd, 0x9a, 0xf0, 0x38, 0xbe, 0x9c, 0xcf, 0x61, 0xdc, 0xed, 0x52, 0xda, 0x83, 0x7a,
	0x6a, 0xe7, 0x9b, 0xd9, 0xf9, 0x7e, 0x06, 0xba, 0x4b, 0x9e, 0xa6, 0xb4, 0x91, 0xfe, 0x36, 0xe3,
	0x92, 0xa3, 0xad, 0x3e, 0xee, 0xc5, 0x8a, 0xf3, 0xd5, 0x9a, 0x46, 0xaa, 0x5a, 0xe4, 0xaf, 0x23,
	0x99, 0xa4, 0x24, 0xe4, 0x3c, 0xdd, 0xea, 0x39, 0xef, 0x93, 0xc1, 0xf1, 0x4c, 0xbf, 0xc4, 0x1e,
	0x58, 0x61, 0xe0, 0xb0, 0x01, 0x1b, 0xb6, 0x23, 0x2b, 0x0c, 0x10, 0xa1, 0x39, 0xe5, 0xf1, 0xce,
	0xb1, 0x14, 0xa2, 0xfe, 0xf1, 0x06, 0xda, 0xb3, 0x8c, 0xe6, 0x92, 0xe2, 0x89, 0x74, 0x1a, 0x03,
	0x36, 0xec, 0x8c, 0x5d, 0x5f, 0x93, 0xf8, 0x86, 0xc4, 0x7f, 0x31, 0x24, 0x51, 0x39, 0x8c, 0xe7,
	0xd0, 0x9e, 0x64, 0x32, 0x59, 0xae, 0x29, 0x0c, 0x9c, 0xa6, 0x5a, 0x59, 0x02, 0xe8, 0x42, 0x6b,
	0x92, 0xcb, 0x37, 0x9e, 0x85, 0x81, 0x63, 0xab, 0xe6, 0xbe, 0xf6, 0x6e, 0xa1, 0x53, 0x48, 0x7c,
	0x4c, 0x84, 0xc4, 0x4b, 0x68, 0x15, 0xa5, 0x70, 0xd8, 0xa0, 0x31, 0xec, 0x8c, 0x7b, 0x9a, 0xda,
	0x2f, 0xe0, 0x68, 0xdf, 0xf7, 0x7c, 0xe8, 0x19, 0x90, 0xde, 0x73, 0x12, 0x7f, 0xc8, 0xf0, 0x62,
	0xe8, 0x17, 0xf3, 0x5a, 0xb8, 0x79, 0x65, 0xa2, 0x60, 0x07, 0x51, 0x54, 0x36, 0x59, 0xbf, 0x19,
	0x6a, 0xd4, 0x0c, 0x5d, 0x95, 0x2c, 0x3c, 0xff, 0x91, 0x26, 0xb6, 0x7c, 0x23, 0x08, 0xfb, 0x60,
	0x2b, 0x40, 0xd1, 0xd8, 0x91, 0x2e, 0xc6, 0x5f, 0x6c, 0x6f, 0xe2, 0x99, 0xb2, 0x8f, 0x64, 0x49,
	0x78, 0x07, 0x5d, 0xad, 0xcf, 0x9c, 0xee, 0xac, 0x9a, 0x40, 0x45, 0xbc, 0x5b, 0x8b, 0x07, 0x03,
	0x38, 0xbd, 0x4f, 0x36, 0xb1, 0x09, 0x69, 0xba, 0x2b, 0x55, 0x9f, 0xd4, 0x92, 0x2c, 0x36, 0x60,
	0x15, 0x56, 0x67, 0x78, 0x02, 0xf7, 0x81, 0xe4, 0xa1, 0x8f, 0x7f, 0x2c, 0xaa, 0xeb, 0x3c, 0xb4,
	0xbf, 0x38, 0x52, 0xbd, 0xeb, 0xef, 0x00, 0x00, 0x00, 0xff, 0xff, 0x98, 0xaf, 0xa6, 0xdf, 0xcb,
	0x02, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// CommentServiceClient is the client API for CommentService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type CommentServiceClient interface {
	CreateComment(ctx context.Context, in *CommentCreateRequest, opts ...grpc.CallOption) (*Comment, error)
	FindCommentsByArticleID(ctx context.Context, in *CommentRequest, opts ...grpc.CallOption) (*CommentList, error)
	GetCommentCountByArticleID(ctx context.Context, in *CommentRequest, opts ...grpc.CallOption) (*CommentCountResponse, error)
}

type commentServiceClient struct {
	cc *grpc.ClientConn
}

func NewCommentServiceClient(cc *grpc.ClientConn) CommentServiceClient {
	return &commentServiceClient{cc}
}

func (c *commentServiceClient) CreateComment(ctx context.Context, in *CommentCreateRequest, opts ...grpc.CallOption) (*Comment, error) {
	out := new(Comment)
	err := c.cc.Invoke(ctx, "/proto.CommentService/CreateComment", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *commentServiceClient) FindCommentsByArticleID(ctx context.Context, in *CommentRequest, opts ...grpc.CallOption) (*CommentList, error) {
	out := new(CommentList)
	err := c.cc.Invoke(ctx, "/proto.CommentService/FindCommentsByArticleID", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *commentServiceClient) GetCommentCountByArticleID(ctx context.Context, in *CommentRequest, opts ...grpc.CallOption) (*CommentCountResponse, error) {
	out := new(CommentCountResponse)
	err := c.cc.Invoke(ctx, "/proto.CommentService/GetCommentCountByArticleID", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// CommentServiceServer is the server API for CommentService service.
type CommentServiceServer interface {
	CreateComment(context.Context, *CommentCreateRequest) (*Comment, error)
	FindCommentsByArticleID(context.Context, *CommentRequest) (*CommentList, error)
	GetCommentCountByArticleID(context.Context, *CommentRequest) (*CommentCountResponse, error)
}

// UnimplementedCommentServiceServer can be embedded to have forward compatible implementations.
type UnimplementedCommentServiceServer struct {
}

func (*UnimplementedCommentServiceServer) CreateComment(ctx context.Context, req *CommentCreateRequest) (*Comment, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateComment not implemented")
}
func (*UnimplementedCommentServiceServer) FindCommentsByArticleID(ctx context.Context, req *CommentRequest) (*CommentList, error) {
	return nil, status.Errorf(codes.Unimplemented, "method FindCommentsByArticleID not implemented")
}
func (*UnimplementedCommentServiceServer) GetCommentCountByArticleID(ctx context.Context, req *CommentRequest) (*CommentCountResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetCommentCountByArticleID not implemented")
}

func RegisterCommentServiceServer(s *grpc.Server, srv CommentServiceServer) {
	s.RegisterService(&_CommentService_serviceDesc, srv)
}

func _CommentService_CreateComment_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CommentCreateRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CommentServiceServer).CreateComment(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.CommentService/CreateComment",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CommentServiceServer).CreateComment(ctx, req.(*CommentCreateRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CommentService_FindCommentsByArticleID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CommentRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CommentServiceServer).FindCommentsByArticleID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.CommentService/FindCommentsByArticleID",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CommentServiceServer).FindCommentsByArticleID(ctx, req.(*CommentRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CommentService_GetCommentCountByArticleID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CommentRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CommentServiceServer).GetCommentCountByArticleID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.CommentService/GetCommentCountByArticleID",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CommentServiceServer).GetCommentCountByArticleID(ctx, req.(*CommentRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _CommentService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "proto.CommentService",
	HandlerType: (*CommentServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateComment",
			Handler:    _CommentService_CreateComment_Handler,
		},
		{
			MethodName: "FindCommentsByArticleID",
			Handler:    _CommentService_FindCommentsByArticleID_Handler,
		},
		{
			MethodName: "GetCommentCountByArticleID",
			Handler:    _CommentService_GetCommentCountByArticleID_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "comment.proto",
}
