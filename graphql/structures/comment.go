package structures

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

// Comment wrap comment object
type Comment struct {
	ID        uuid.UUID `json:"id"`
	Body      string    `json:"body"`
	CreatedAt time.Time `json:"created_at"`
	ArticleID uuid.UUID `json:"article_id"`
	AuthorID  uuid.UUID `json:"user_id"`
}
