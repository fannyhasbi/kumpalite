package structures

import uuid "github.com/satori/go.uuid"

// Category store a single category
type Category struct {
	ID   uuid.UUID `json:"id"`
	Name string    `json:"name"`
}
