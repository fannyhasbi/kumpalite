package structures

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

// Article store a single article
type Article struct {
	ID            uuid.UUID `json:"id"`
	Slug          string    `json:"slug"`
	Name          string    `json:"name"`
	Body          string    `json:"body"`
	Status        string    `json:"status"`
	Category      []string  `json:"category"`
	CreatedAt     time.Time `json:"created_at"`
	UpdatedAt     time.Time `json:"updated_at"`
	AuthorID      uuid.UUID `json:"author_id"`
	CommentsCount int32     `json:"comments_count"`
	ImageURL      string    `json:"image_url"`
}
