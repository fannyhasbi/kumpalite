package handler

import (
	"log"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/fannyhasbi/kumpalite/graphql/config"
	"gitlab.com/fannyhasbi/kumpalite/graphql/helper"
	pb "gitlab.com/fannyhasbi/kumpalite/graphql/proto"
)

// LoginHandler handle login from request
func LoginHandler(c echo.Context) error {
	m := echo.Map{}
	if err := c.Bind(&m); err != nil {
		return err
	}

	username := m["username"].(string)
	password := m["password"].(string)

	authorService := config.InitAuthorGRPC()
	authorRequest := &pb.AuthorRequest{
		Username: username,
		Password: password,
	}

	result, err := authorService.FindAuthorByUsernamePass(c.Request().Context(), authorRequest)
	if err != nil {
		log.Println(err)
		// user not found
		return c.JSON(http.StatusUnauthorized, map[string]string{
			"message": "Username or password not found",
		})
	}

	jwtPayload := helper.JWTPayload{
		Username: result.Username,
		Name:     result.Name,
		AuthorID: result.ID,
	}

	t, err := helper.GenerateToken(jwtPayload)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]string{
		"token":     t,
		"username":  result.Username,
		"name":      result.Name,
		"author_id": result.ID,
	})
}
