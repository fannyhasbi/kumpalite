package handler

import (
	"net/http"

	"github.com/labstack/echo"
)

// IndexHandler handle index HTTP
func IndexHandler(c echo.Context) error {
	return c.String(http.StatusOK, "Hello world")
}
