package handler

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"gitlab.com/fannyhasbi/kumpalite/graphql/helper"
)

// Upload handle file upload
func Upload(c echo.Context) error {
	// Check user token
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)

	authorID := claims["author_id"].(string)
	log.Println("[UPLOAD][ACCESS]", authorID, time.Now().String())

	file, err := c.FormFile("file")
	if err != nil {
		log.Println("[UPLOAD][READ_FILE]", err)
		return err
	}

	src, err := file.Open()
	if err != nil {
		log.Println("[UPLOAD][OPEN_SOURCE_FILE", err)
		return err
	}
	defer src.Close()

	dst, err := os.Create(fmt.Sprintf("static/%s", file.Filename))
	if err != nil {
		log.Println("[UPLOAD][OPEN_DEST_FILE]", err)
		return err
	}
	defer dst.Close()

	if _, err := io.Copy(dst, src); err != nil {
		return err
	}

	log.Println("[UPLOAD][SUCCESS]", file.Filename)
	return c.JSON(http.StatusOK, map[string]string{
		"path": fmt.Sprintf("%s/%s", helper.StaticPath, file.Filename),
	})
}
