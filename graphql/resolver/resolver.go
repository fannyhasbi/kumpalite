package resolver

import (
	"gitlab.com/fannyhasbi/kumpalite/graphql/connector"
)

const (
	PUBLISHED = "PUBLISHED"
	DRAFT     = "DRAFT"
)

// Resolver wrap all resolvers
type Resolver struct {
	AuthorService   connector.AuthorConnector
	ArticleService  connector.ArticleConnector
	CommentService  connector.CommentConnector
	CategoryService connector.CategoryConnector
}
