package resolver

import (
	"gitlab.com/fannyhasbi/kumpalite/graphql/structures"
)

type AuthorResolver struct {
	a structures.Author
}

type AuthorRegisterResolver struct {
	a structures.Author
}

func (ar *AuthorResolver) ID() *string {
	result := ar.a.ID.String()
	return &result
}

func (ar *AuthorResolver) Name() *string {
	return &ar.a.Name
}

func (ar *AuthorResolver) Email() *string {
	return &ar.a.Email
}

func (ar *AuthorResolver) Username() *string {
	return &ar.a.Username
}

// ID for Register author response
// -----
//
func (ar *AuthorRegisterResolver) ID() *string {
	result := ar.a.ID.String()
	return &result
}

func (ar *AuthorRegisterResolver) Name() *string {
	return &ar.a.Name
}

func (ar *AuthorRegisterResolver) Email() *string {
	return &ar.a.Email
}

func (ar *AuthorRegisterResolver) Username() *string {
	return &ar.a.Username
}

func (ar *AuthorRegisterResolver) Token() *string {
	return &ar.a.Token
}
