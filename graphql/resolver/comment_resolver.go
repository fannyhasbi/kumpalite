package resolver

import (
	"github.com/satori/go.uuid"
	"gitlab.com/fannyhasbi/kumpalite/graphql/structures"
)

// CommentResolver resolve the comment type schema
type CommentResolver struct {
	c structures.Comment
	a AuthorResolver
}

func (cmt *CommentResolver) ID() *string {
	result := cmt.c.ID.String()
	return &result
}

func (cmt *CommentResolver) Body() *string {
	result := cmt.c.Body
	return &result
}

func (cmt *CommentResolver) CreatedAt() *string {
	result := cmt.c.CreatedAt.Format("2006-01-02 15:04:05")
	return &result
}

func (cmt *CommentResolver) Author() *AuthorResolver {
	id, _ := uuid.FromString(*cmt.a.ID())

	authorStructure := structures.Author{
		ID:       id,
		Name:     *cmt.a.Name(),
		Email:    *cmt.a.Email(),
		Username: *cmt.a.Username(),
	}

	result := &AuthorResolver{
		a: authorStructure,
	}
	return result
}
