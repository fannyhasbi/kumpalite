package resolver

import (
	"context"
	"errors"
	"log"
)

// GetPublishedArticles get publihsed articles
func (r *Resolver) GetPublishedArticles(ctx context.Context) *[]*ArticleResolver {
	var result []*ArticleResolver

	articles := r.ArticleService.GetPublishedArticles()

	result = make([]*ArticleResolver, len(articles))
	for i := range articles {
		author, err := r.AuthorService.FindAuthorByID(articles[i].AuthorID.String())
		if err != nil {
			log.Println(err)
		}

		categories, err := r.GetCategoriesByAuthorIDAndArticleID(ctx, struct{ AuthorID, ArticleID string }{AuthorID: author.ID.String(), ArticleID: articles[i].ID.String()})
		if err != nil {
			log.Println(err)
			return &result
		}

		result[i] = &ArticleResolver{
			meta:         articles[i],
			authorMeta:   author,
			categoryMeta: categories,
		}
	}

	return &result
}

// GetPublishedArticlesByName search an article by name
func (r *Resolver) GetPublishedArticlesByName(ctx context.Context, args struct{ Name string }) *[]*ArticleResolver {
	articles := r.ArticleService.GetPublishedArticlesByName(args.Name)

	result := make([]*ArticleResolver, len(articles))
	for i := range articles {
		author, err := r.AuthorService.FindAuthorByID(articles[i].AuthorID.String())
		if err != nil {
			log.Println(err)
		}

		categories, err := r.GetCategoriesByAuthorIDAndArticleID(ctx, struct{ AuthorID, ArticleID string }{AuthorID: author.ID.String(), ArticleID: articles[i].ID.String()})
		if err != nil {
			log.Println(err)
			return &result
		}

		result[i] = &ArticleResolver{
			meta:         articles[i],
			authorMeta:   author,
			categoryMeta: categories,
		}
	}

	return &result
}

// GetRelatedArticles get related articles by category
func (r *Resolver) GetRelatedArticles(ctx context.Context, args struct{ Category []*string }) *[]*ArticleResolver {
	var categories []string
	for i := range args.Category {
		categories = append(categories, *args.Category[i])
	}

	articles := r.ArticleService.GetRelatedArticles(categories)

	result := make([]*ArticleResolver, len(articles))
	for i := range articles {
		author, err := r.AuthorService.FindAuthorByID(articles[i].AuthorID.String())
		if err != nil {
			log.Println(err)
		}

		categories, err := r.GetCategoriesByAuthorIDAndArticleID(ctx, struct{ AuthorID, ArticleID string }{AuthorID: author.ID.String(), ArticleID: articles[i].ID.String()})
		if err != nil {
			log.Println(err)
			return &result
		}

		result[i] = &ArticleResolver{
			meta:         articles[i],
			authorMeta:   author,
			categoryMeta: categories,
		}
	}

	return &result
}

// GetPublishedArticleBySlug search an article by slug
func (r *Resolver) GetPublishedArticleBySlug(ctx context.Context, args struct{ Slug string }) *ArticleResolver {
	article := r.ArticleService.GetPublishedArticleBySlug(args.Slug)

	var result *ArticleResolver
	author, err := r.AuthorService.FindAuthorByID(article.AuthorID.String())
	if err != nil {
		log.Println(err)
	}

	categories, err := r.GetCategoriesByAuthorIDAndArticleID(ctx, struct{ AuthorID, ArticleID string }{AuthorID: author.ID.String(), ArticleID: article.ID.String()})
	if err != nil {
		log.Println(err)
		return &ArticleResolver{}
	}

	result = &ArticleResolver{
		meta:         article,
		authorMeta:   author,
		categoryMeta: categories,
	}

	return result
}

// GetPublishedArticlesByUsername get published articles by author username
func (r *Resolver) GetPublishedArticlesByUsername(ctx context.Context, args struct{ Username string }) (*[]*ArticleResolver, error) {
	var result []*ArticleResolver

	author, err := r.AuthorService.FindAuthorByUsername(args.Username)
	if err != nil {
		log.Println(err)
		return &result, errors.New("Author not found")
	}

	articles := r.ArticleService.GetPublishedArticlesByAuthorID(author.ID.String())

	result = make([]*ArticleResolver, len(articles))
	for i := range articles {
		author, err := r.AuthorService.FindAuthorByID(articles[i].AuthorID.String())
		if err != nil {
			log.Println(err)
		}

		categories, err := r.GetCategoriesByAuthorIDAndArticleID(ctx, struct{ AuthorID, ArticleID string }{AuthorID: author.ID.String(), ArticleID: articles[i].ID.String()})
		if err != nil {
			log.Println(err)
			return &result, err
		}

		result[i] = &ArticleResolver{
			meta:         articles[i],
			authorMeta:   author,
			categoryMeta: categories,
		}
	}

	return &result, nil
}

// GetArticlesByCategoryAndAuthorID get articles catagory and authorID
func (r *Resolver) GetArticlesByCategoryAndAuthorID(ctx context.Context, args struct {
	Category string
	AuthorID string
}) (*[]*ArticleResolver, error) {
	var result []*ArticleResolver

	articles := r.ArticleService.GetArticlesByCategoryAndAuthorID(args.Category, args.AuthorID)
	result = make([]*ArticleResolver, len(articles))

	for i := range articles {
		author, err := r.AuthorService.FindAuthorByID(articles[i].AuthorID.String())
		if err != nil {
			log.Println(err)
		}

		categories, err := r.GetCategoriesByAuthorIDAndArticleID(ctx, struct{ AuthorID, ArticleID string }{AuthorID: author.ID.String(), ArticleID: articles[i].ID.String()})
		if err != nil {
			log.Println(err)
			return &result, err
		}

		result[i] = &ArticleResolver{
			meta:         articles[i],
			authorMeta:   author,
			categoryMeta: categories,
		}
	}

	return &result, nil
}

// GetArticlesByCategory is get articles by category
func (r *Resolver) GetArticlesByCategory(ctx context.Context, args struct{ Category string }) (*[]*ArticleResolver, error) {
	var result []*ArticleResolver

	articles := r.ArticleService.GetArticlesByCategory(args.Category)
	result = make([]*ArticleResolver, len(articles))

	for i := range articles {
		author, err := r.AuthorService.FindAuthorByID(articles[i].AuthorID.String())
		if err != nil {
			log.Println(err)
		}

		categories, err := r.GetCategoriesByAuthorIDAndArticleID(ctx, struct{ AuthorID, ArticleID string }{AuthorID: author.ID.String(), ArticleID: articles[i].ID.String()})
		if err != nil {
			log.Println(err)
			return &result, err
		}

		result[i] = &ArticleResolver{
			meta:         articles[i],
			authorMeta:   author,
			categoryMeta: categories,
		}
	}

	return &result, nil
}

// GetArticlesByUsernameAndName search articles by username and title name
func (r *Resolver) GetArticlesByUsernameAndName(ctx context.Context, args struct{ Username, Name string }) (*[]*ArticleResolver, error) {
	var result []*ArticleResolver

	author, err := r.AuthorService.FindAuthorByUsername(args.Username)
	if err != nil {
		log.Println(err)
		return &result, errors.New("Author not found")
	}

	articles := r.ArticleService.GetArticlesByAuthorIDAndName(author.ID.String(), args.Name)

	result = make([]*ArticleResolver, len(articles))
	for i := range articles {
		author, err := r.AuthorService.FindAuthorByID(articles[i].AuthorID.String())
		if err != nil {
			log.Println(err)
		}

		categories, err := r.GetCategoriesByAuthorIDAndArticleID(ctx, struct{ AuthorID, ArticleID string }{AuthorID: author.ID.String(), ArticleID: articles[i].ID.String()})
		if err != nil {
			log.Println(err)
			return &result, err
		}

		result[i] = &ArticleResolver{
			meta:         articles[i],
			authorMeta:   author,
			categoryMeta: categories,
		}
	}

	return &result, nil
}

// GetArticlesByAuthorID is get articles by authorID
func (r *Resolver) GetArticlesByAuthorID(ctx context.Context, args struct{ AuthorID string }) (*[]*ArticleResolver, error) {
	var result []*ArticleResolver

	articles := r.ArticleService.GetArticlesByAuthorID(args.AuthorID)
	result = make([]*ArticleResolver, len(articles))
	for i := range articles {
		author, err := r.AuthorService.FindAuthorByID(articles[i].AuthorID.String())
		if err != nil {
			log.Println(err)
		}

		categories, err := r.GetCategoriesByAuthorIDAndArticleID(ctx, struct{ AuthorID, ArticleID string }{AuthorID: author.ID.String(), ArticleID: articles[i].ID.String()})
		if err != nil {
			log.Println(err)
			return &result, err
		}

		result[i] = &ArticleResolver{
			meta:         articles[i],
			authorMeta:   author,
			categoryMeta: categories,
		}
	}

	return &result, nil
}
