package resolver

import (
	"context"

	"gitlab.com/fannyhasbi/kumpalite/graphql/structures"
)

// ArticleResolver resolve the Article type
type ArticleResolver struct {
	meta         structures.Article
	authorMeta   structures.Author
	categoryMeta *[]*CategoryResolver
}

func (r *ArticleResolver) ID(ctx context.Context) *string {
	result := r.meta.ID.String()
	return &result
}

func (r *ArticleResolver) Slug(ctx context.Context) *string {
	result := r.meta.Slug
	return &result
}

func (r *ArticleResolver) Name(ctx context.Context) *string {
	result := r.meta.Name
	return &result
}

func (r *ArticleResolver) Body(ctx context.Context) *string {
	result := r.meta.Body
	return &result
}

func (r *ArticleResolver) Status(ctx context.Context) *string {
	result := r.meta.Status
	return &result
}

// func (r *ArticleResolver) Category(ctx context.Context) *[]*string {
// 	var result []*string
// 	for _, v := range r.meta.Category {
// 		ctg := v
// 		result = append(result, &ctg)
// 	}

// 	return &result
// }

func (r *ArticleResolver) CreatedAt(ctx context.Context) *string {
	result := r.meta.CreatedAt.Format("2006-01-02 15:04:05")
	return &result
}

func (r *ArticleResolver) UpdatedAt(ctx context.Context) *string {
	result := r.meta.UpdatedAt.Format("2006-01-02 15:04:05")
	return &result
}

func (r *ArticleResolver) CommentsCount(ctx context.Context) *int32 {
	result := r.meta.CommentsCount
	return &result
}

func (r *ArticleResolver) ImageURL(ctx context.Context) *string {
	result := r.meta.ImageURL
	return &result
}

func (r *ArticleResolver) Author(ctx context.Context) *AuthorResolver {
	result := &AuthorResolver{
		a: r.authorMeta,
	}
	return result
}

func (r *ArticleResolver) Category(ctx context.Context) *[]*CategoryResolver {
	return r.categoryMeta
}
