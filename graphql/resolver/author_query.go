package resolver

import (
	"context"
	"errors"
	"log"
)

// GetAuthorByUsername find an author by username
func (r *Resolver) GetAuthorByUsername(ctx context.Context, args struct{ Username string }) (*AuthorResolver, error) {
	author, err := r.AuthorService.FindAuthorByUsername(args.Username)
	if err != nil {
		log.Println(err)
		return &AuthorResolver{}, errors.New("Author not found")
	}

	result := &AuthorResolver{
		a: author,
	}

	return result, nil
}

// GetAuthorByID get author by its id
func (r *Resolver) GetAuthorByID(ctx context.Context, args struct{ AuthorID string }) (*AuthorResolver, error) {
	author, err := r.AuthorService.FindAuthorByID(args.AuthorID)
	if err != nil {
		log.Println(err)
		return &AuthorResolver{}, errors.New("Author not found")
	}

	result := &AuthorResolver{
		a: author,
	}

	return result, nil
}
