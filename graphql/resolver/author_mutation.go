package resolver

import (
	"context"

	"gitlab.com/fannyhasbi/kumpalite/graphql/helper"
)

// RegisterAuthorInput Input for author registration
type RegisterAuthorInput struct {
	Username string
	Name     string
	Email    string
	Password string
}

// RegisterAuthor resolver for author registration
func (r *Resolver) RegisterAuthor(ctx context.Context, args struct{ Input *RegisterAuthorInput }) (*AuthorRegisterResolver, error) {
	var result AuthorRegisterResolver

	author, err := r.AuthorService.CreateAuthor(args.Input.Username, args.Input.Name, args.Input.Email, args.Input.Password)
	if err != nil {
		return &result, err
	}

	jwtPayload := helper.JWTPayload{
		Username: author.Username,
		Name:     author.Name,
		AuthorID: author.ID.String(),
	}

	t, err := helper.GenerateToken(jwtPayload)
	if err != nil {
		return &result, err
	}

	// attach generated JWT to Token field
	author.Token = t

	result = AuthorRegisterResolver{
		a: author,
	}

	return &result, nil
}
