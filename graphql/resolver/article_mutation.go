package resolver

import (
	"context"
	"errors"
	"log"

	"gitlab.com/fannyhasbi/kumpalite/graphql/helper"
)

// CreateArticleInput input for article creation
type CreateArticleInput struct {
	Id          *string
	Name        string
	Body        string
	Slug        string
	Status      string
	CategoryIDs *[]*string
	ImageURL    string
}

// CreateArticle resolver for article creation
func (r *Resolver) CreateArticle(ctx context.Context, args struct{ Input *CreateArticleInput }) (*ArticleResolver, error) {
	var result ArticleResolver
	var categories []string
	var id string

	if args.Input.Id == nil {
		id = ""
	} else {
		id = *args.Input.Id
	}

	if args.Input.CategoryIDs != nil {
		for _, v := range *args.Input.CategoryIDs {
			categories = append(categories, *v)
		}
	}

	contextData, err := helper.ParseContext(ctx.Value("data"))
	if err != nil {
		return &result, err
	}

	if contextData["author_id"] == "" {
		log.Println("[CreateArticle][ERR] Unauthorized", contextData)
		return &result, errors.New("Unauthorized")
	}

	article, err := r.ArticleService.CreateArticle(id, args.Input.Name, args.Input.Body, args.Input.Slug, args.Input.Status, contextData["author_id"], args.Input.ImageURL, categories)
	if err != nil {
		log.Println("[CreateArticle][ERR]", err)
		return &result, err
	}

	responseCategories, err := r.GetCategoriesByAuthorIDAndArticleID(ctx, struct{ AuthorID, ArticleID string }{AuthorID: contextData["author_id"], ArticleID: article.ID.String()})
	if err != nil {
		log.Println(err)
	}

	result = ArticleResolver{
		meta:         article,
		categoryMeta: responseCategories,
	}

	return &result, nil
}
