package resolver

import (
	"context"
	"log"
)

// GetArticleComments get article comments by article id
func (r *Resolver) GetArticleComments(ctx context.Context, args struct{ ArticleID string }) (*[]*CommentResolver, error) {
	var result []*CommentResolver

	comments, err := r.CommentService.GetCommentsByArticleID(args.ArticleID)
	if err != nil {
		log.Println(err)
		return &result, err
	}

	result = make([]*CommentResolver, len(comments))
	for i := range comments {
		author, err := r.GetAuthorByID(ctx, struct{ AuthorID string }{AuthorID: comments[i].AuthorID.String()})
		if err != nil {
			log.Println(err)
			return &result, err
		}

		result[i] = &CommentResolver{
			c: comments[i],
			a: *author,
		}
	}

	return &result, nil
}
