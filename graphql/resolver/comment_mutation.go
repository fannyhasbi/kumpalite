package resolver

import (
	"context"
	"errors"
	"log"

	"gitlab.com/fannyhasbi/kumpalite/graphql/helper"
)

// CreateCommentInput input for comment creation
type CreateCommentInput struct {
	Body      string
	ArticleID string
}

// CreateComment resolver to create comment with mutation
func (r *Resolver) CreateComment(ctx context.Context, args CreateCommentInput) (*CommentResolver, error) {
	var result CommentResolver

	contextData, err := helper.ParseContext(ctx.Value("data"))
	if err != nil {
		return &result, err
	}
	if contextData["author_id"] == "" {
		log.Println("[CreateComment][ERR} Unauthorized", contextData)
		return &result, errors.New("Unauthorized")
	}

	comment, err := r.CommentService.CreateComment(args.Body, args.ArticleID, contextData["author_id"])
	author, err := r.GetAuthorByID(ctx, struct{ AuthorID string }{AuthorID: comment.AuthorID.String()})
	if err != nil {
		return &result, err
	}

	result = CommentResolver{
		c: comment,
		a: *author,
	}

	return &result, nil
}
