package resolver

import (
	"context"
	"log"
)

// FindCategoryByID find category by id
func (r *Resolver) FindCategoryByID(ctx context.Context, args struct{ ID string }) (*CategoryResolver, error) {
	var result CategoryResolver

	category, err := r.CategoryService.FindCategoryByID(args.ID)

	if err != nil {
		return &result, err
	}

	result = CategoryResolver{
		meta: category,
	}

	return &result, nil
}

// GetCategoriesByAuthorID get categories by authorID
func (r *Resolver) GetCategoriesByAuthorID(ctx context.Context, args struct{ AuthorID string }) (*[]*CategoryResolver, error) {
	categories, _ := r.CategoryService.GetCategoriesByAuthorID(args.AuthorID)

	result := make([]*CategoryResolver, len(categories))
	for i := range categories {
		result[i] = &CategoryResolver{
			meta: categories[i],
		}
	}

	return &result, nil
}

// GetCategoriesByAuthorIDAndArticleID get categories
func (r *Resolver) GetCategoriesByAuthorIDAndArticleID(ctx context.Context, args struct{ AuthorID, ArticleID string }) (*[]*CategoryResolver, error) {
	var result []*CategoryResolver

	categories, err := r.CategoryService.GetCategoriesByAuthorIDAndArticleID(args.AuthorID, args.ArticleID)
	if err != nil {
		log.Println("[GetCategoriesByAuthorIDAndArticleID][ERR]", err)
		return &result, err
	}

	result = make([]*CategoryResolver, len(categories))
	for i := range categories {
		result[i] = &CategoryResolver{
			meta: categories[i],
		}
	}

	return &result, nil
}

// func (r *Resolver) GetCategoriesByID(ctx context.Context, args struct{ ID string }) (*CategoryResolver, error) {

// }
