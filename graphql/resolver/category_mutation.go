package resolver

import (
	"context"
	"errors"

	"gitlab.com/fannyhasbi/kumpalite/graphql/helper"
)

// CreateCategoryInput input for create new category
type CreateCategoryInput struct {
	Name string
	Slug string
}

// CreateCategory create new category
func (r *Resolver) CreateCategory(ctx context.Context, args CreateCategoryInput) (*CategoryResolver, error) {
	var result CategoryResolver

	contextData, err := helper.ParseContext(ctx.Value("data"))
	if err != nil {
		return &result, err
	}
	if contextData["author_id"] == "" {
		return &result, errors.New("Unauthorized")
	}

	category, err := r.CategoryService.CreateCategory(args.Name, args.Slug, contextData["author_id"])

	if err != nil {
		return &result, err
	}

	result = CategoryResolver{
		meta: category,
	}

	return &result, nil
}
