package resolver

import (
	"context"

	"gitlab.com/fannyhasbi/kumpalite/graphql/structures"
)

type CategoryResolver struct {
	meta structures.Category
}

func (c *CategoryResolver) ID(ctx context.Context) *string {
	result := c.meta.ID.String()
	return &result
}

func (c *CategoryResolver) Name(ctx context.Context) *string {
	result := c.meta.Name
	return &result
}

func (c *CategoryResolver) Slug(ctx context.Context) *string {
	result := c.meta.Name
	return &result
}
