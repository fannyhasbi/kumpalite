package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	graphql "github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/fannyhasbi/kumpalite/graphql/connector/rpc"
	"gitlab.com/fannyhasbi/kumpalite/graphql/handler"
	"gitlab.com/fannyhasbi/kumpalite/graphql/helper"
	"gitlab.com/fannyhasbi/kumpalite/graphql/resolver"
)

const (
	// PORT running port for server
	PORT = 9000
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}

func main() {
	s, err := getSchema("./schema/schema.graphql")
	if err != nil {
		panic(err)
	}

	authorService := rpc.NewAuthorRPCConnector()
	articleService := rpc.NewArticleRPCConnector()
	commentService := rpc.NewCommentRPCConnector()
	categoryService := rpc.NewCategoryRPCConnector()

	resolver := &resolver.Resolver{
		AuthorService:   authorService,
		ArticleService:  articleService,
		CommentService:  commentService,
		CategoryService: categoryService,
	}
	schema := graphql.MustParseSchema(s, resolver)

	// instantiate echo framework
	e := echo.New()

	e.Use(middleware.Recover())
	e.Use(middleware.RequestID())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
	}))

	e.Static(helper.StaticPath, "static")

	e.GET("/", handler.IndexHandler)
	e.POST("/login", handler.LoginHandler)
	e.POST("/graphql", wrapHandler(&relay.Handler{Schema: schema}))

	u := e.Group("/upload")
	u.Use(middleware.JWT(helper.JWTSecretKey))

	u.POST("", handler.Upload)

	e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", PORT)))
}

func getSchema(path string) (string, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}

	return string(b), nil
}

func wrapHandler(h http.Handler) echo.HandlerFunc {
	return func(c echo.Context) error {
		originalRequest := c.Request()
		originalContext := originalRequest.Context()

		var username string
		var authorID string

		authorizationHeader := c.Request().Header.Get("Authorization")
		if strings.Contains(authorizationHeader, "Bearer") {
			tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)

			token, _ := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
				if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("Signing method invalid")
				} else if method != jwt.SigningMethodHS256 {
					return nil, fmt.Errorf("Signing method invalid")
				}

				return helper.JWTSecretKey, nil
			})

			claims, _ := token.Claims.(jwt.MapClaims)
			if _, ok := claims["username"]; ok {
				username = claims["username"].(string)
			}

			if _, ok := claims["author_id"]; ok {
				authorID = claims["author_id"].(string)
			}
		}

		data := map[string]string{
			"username":  username,
			"author_id": authorID,
		}

		req := originalRequest.WithContext(
			context.WithValue(originalContext, "data", data),
		)

		h.ServeHTTP(c.Response(), req)
		return nil
	}
}
