package main

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
)

var envFiles = []string{
	"./graphql/.env",
	"./article/.env",
	"./author/.env",
	"./category/.env",
	"./comment/.env",
}

func main() {
	var env map[string]string
	var err error

	command := os.Args[1]

	if command == "production" {
		for _, v := range envFiles {
			env, _ = godotenv.Unmarshal("ENV=production")
			err = godotenv.Write(env, v)
			if err != nil {
				fmt.Println("[ENV_GENERATOR][ERR]", err)
			}
		}
	} else {
		for _, v := range envFiles {
			env, _ = godotenv.Unmarshal("ENV=development")
			err = godotenv.Write(env, v)
			if err != nil {
				fmt.Println("[ENV_GENERATOR][ERR]", err)
			}
		}
	}

	fmt.Println("[ENV_GENERATOR][SUCCESS] =>", env["ENV"])
}
